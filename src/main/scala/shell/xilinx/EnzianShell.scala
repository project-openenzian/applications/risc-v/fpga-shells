// See LICENSE for license details.
package sifive.fpgashells.shell.xilinx

import chisel3._
import chisel3.experimental.{attach, Analog, IO}
import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.util.SyncResetSynchronizerShiftReg
import sifive.fpgashells.clocks._
import sifive.fpgashells.shell._
import sifive.fpgashells.ip.xilinx._
import sifive.blocks.devices.chiplink._
import sifive.fpgashells.devices.xilinx.xilinxenzianmig._
import sifive.fpgashells.devices.xilinx.xdma._
import sifive.fpgashells.ip.xilinx.xxv_ethernet._
import sifive.fpgashells.ip.xilinx.enzianmig.ECISignals

class SysClockEnzianPlacedOverlay(val shell: EnzianShellBasicOverlays, name: String, val designInput: ClockInputDesignInput, val shellInput: ClockInputShellInput)
  extends LVDSClockInputXilinxPlacedOverlay(name, designInput, shellInput)
{
  val node = shell { ClockSourceNode(freqMHz = 300, jitterPS = 50)(ValName(name)) }

  shell { InModuleBody {
    shell.xdc.addPackagePin(io.p, "AW28")
    shell.xdc.addPackagePin(io.n, "AY28")
    shell.xdc.addIOStandard(io.p, "DIFF_SSTL12")
    shell.xdc.addIOStandard(io.n, "DIFF_SSTL12")
    shell.xdc.addDirection(io.p, "IN")
    shell.xdc.addDirection(io.n, "IN")
  } }
}
class SysClockEnzianShellPlacer(shell: EnzianShellBasicOverlays, val shellInput: ClockInputShellInput)(implicit val valName: ValName)
  extends ClockInputShellPlacer[EnzianShellBasicOverlays]
{
    def place(designInput: ClockInputDesignInput) = new SysClockEnzianPlacedOverlay(shell, valName.name, designInput, shellInput)
}

class RefClockEnzianPlacedOverlay(val shell: EnzianShellBasicOverlays, name: String, val designInput: ClockInputDesignInput, val shellInput: ClockInputShellInput)
  extends LVDSClockInputXilinxPlacedOverlay(name, designInput, shellInput) {
  val node = shell { ClockSourceNode(freqMHz = 100, jitterPS = 50)(ValName(name)) }

  shell { InModuleBody {
    shell.xdc.addPackagePin(io.p, "AW28")
    shell.xdc.addPackagePin(io.n, "AY28")
    shell.xdc.addIOStandard(io.p, "DIFF_SSTL12")
    shell.xdc.addIOStandard(io.n, "DIFF_SSTL12")
    shell.xdc.setODT(io.p, "RTT_NONE")
    shell.xdc.setODT(io.n, "RTT_NONE")
  } }
}

class RefClockEnzianShellPlacer(shell: EnzianShellBasicOverlays, val shellInput: ClockInputShellInput)(implicit val valName: ValName)
  extends ClockInputShellPlacer[EnzianShellBasicOverlays] {
  def place(designInput: ClockInputDesignInput) = new RefClockEnzianPlacedOverlay(shell, valName.name, designInput, shellInput)
}

class UARTEnzianPlacedOverlay(val shell: EnzianShellBasicOverlays, name: String, val designInput: UARTDesignInput, val shellInput: UARTShellInput)
  extends UARTXilinxPlacedOverlay(name, designInput, shellInput, true)
{
  shell { InModuleBody {
    val packagePinsWithPackageIOsIn = Seq(("BB25", IOPin(io.ctsn.get)),
                                          ("BB27", IOPin(io.txd)))

    val packagePinsWithPackageIOsOut = Seq(("BA25", IOPin(io.rtsn.get), 2, "SLOW"),
                                           ("BB26", IOPin(io.rxd),      2, "SLOW"))

    packagePinsWithPackageIOsIn foreach { case (pin, io) => {
        shell.xdc.addPackagePin(io, pin)
        shell.xdc.addIOStandard(io, "LVCMOS12")
        shell.xdc.addDirection(io, "IN")
      }
    }

    packagePinsWithPackageIOsOut foreach { case (pin, io, drive, slew) => {
        shell.xdc.addPackagePin(io, pin)
        shell.xdc.addIOStandard(io, "LVCMOS12")
        shell.xdc.addDirection(io, "OUT")
        shell.xdc.addDriveStrength(io, drive.toString())
        shell.xdc.addSlew(io, slew)
      }
    }
  } }
}

class UARTEnzianShellPlacer(shell: EnzianShellBasicOverlays, val shellInput: UARTShellInput)(implicit val valName: ValName)
  extends UARTShellPlacer[EnzianShellBasicOverlays] {
  def place(designInput: UARTDesignInput) = new UARTEnzianPlacedOverlay(shell, valName.name, designInput, shellInput)
}

case object EnzianDDRSize extends Field[BigInt](0x400000000L) // 16GB
case object EnableECI extends Field[Boolean](false)
class DDREnzianPlacedOverlay(val shell: EnzianShellBasicOverlays, name: String, val designInput: DDRDesignInput, val shellInput: DDRShellInput)
  extends DDRPlacedOverlay[XilinxEnzianMIGPads](name, designInput, shellInput)
{
  val size = p(EnzianDDRSize)
  val eci  = p(EnableECI)

  val migParams = XilinxEnzianMIGParams(address = AddressSet.misaligned(di.baseAddress, size), eci)
  val mig = LazyModule(new XilinxEnzianMIG(migParams))
  val ioNode = BundleBridgeSource(() => mig.module.io.cloneType)
  val topIONode = shell { ioNode.makeSink() }
  val ddrUI     = shell { ClockSourceNode(freqMHz = 200) }
  val areset    = shell { ClockSinkNode(Seq(ClockSinkParameters())) }
  areset := designInput.wrangler := ddrUI

  def overlayOutput = DDROverlayOutput(ddr = mig.node)
  def ioFactory = if (eci) { new XilinxEnzianMIGPads(size) with ECISignals } else { new XilinxEnzianMIGPads(size) }

  InModuleBody { ioNode.bundle <> mig.module.io }

  shell { InModuleBody {
    require (shell.sys_clock.get.isDefined, "Use of DDREnzianOverlay depends on SysClockEnzianOverlay")
    val (sys, _) = shell.sys_clock.get.get.overlayOutput.node.out(0)
    val (ui, _) = ddrUI.out(0)
    val (ar, _) = areset.in(0)
    val port = topIONode.bundle.port
    io <> port
    ui.clock := port.c0_ddr4_ui_clk
    ui.reset := /*!port.mmcm_locked ||*/ port.c0_ddr4_ui_clk_sync_rst
    //port.c0_sys_clk_i := sys.clock.asUInt
    port.sys_rst := sys.reset // pllReset
    port.c0_ddr4_aresetn := !ar.reset

    val allddrpins = Seq(  "G32", "K32", "K33", "K34", "L32", "L34", "L33",
      "M32", "M34", "N33", "H31", "N34", "P34", "K30", "G30", "H32", "E31", 
      "P33", "N32", "E32", "G31", "R30", "P30", "E33", "F33", "R32", "F30", 
      "J30", "U30", "V31", "U31", "T30", "T33", "U32", "R33", "T32", "D35", 
      "E35", "A35", "B35", "A37", "A38", "E36", "D36", "H34", "G34", "F34", 
      "F35", "J36", "J35", "F37", "G37", "D28", "E28", "E27", "F27", "G29", 
      "H29", "G26", "G27", "C32", "B32", "D31", "C31", "D34", "D33", "C34", 
      "C33", "B30", "A30", "A29", "B29", "D30", "E30", "D29", "C29", "H27", 
      "H28", "J28", "J29", "L28", "K28", "M27", "L27", "N28", "N26", "P26", 
      "P28", "R26", "R27", "T26", "T27", "D38", "C39", "C38", "E38", "A40", 
      "B40", "D39", "E39", "V33", "T34", "B37", "C37", "G36", "H38", "F29", 
      "H26", "A33", "A34", "A28", "B27", "K27", "L29", "N29", "R28", "A39", 
      "D40", "V32", "U34", "B36", "C36", "H36", "H37", "F28", "J26", "A32", 
      "B34", "A27", "C27", "K26", "M29", "P29", "T28", "B39", "E40", "F32",
      "N31", "P31")

    (IOPin.of(io) zip allddrpins) foreach { case (io, pin) => shell.xdc.addPackagePin(io, pin) }
  } }

  shell.sdc.addGroup(pins = Seq(mig.island.module.blackbox.io.c0_ddr4_ui_clk))
}
class DDREnzianShellPlacer(shell: EnzianShellBasicOverlays, val shellInput: DDRShellInput)(implicit val valName: ValName)
  extends DDRShellPlacer[EnzianShellBasicOverlays] {
  def place(designInput: DDRDesignInput) = new DDREnzianPlacedOverlay(shell, valName.name, designInput, shellInput)
}

abstract class EnzianShellBasicOverlays()(implicit p: Parameters) extends UltraScaleShell {
  // PLL reset causes
  val pllReset = InModuleBody { Wire(Bool()) }

  val sys_clock = Overlay(ClockInputOverlayKey, new SysClockEnzianShellPlacer(this, ClockInputShellInput()))
  //val ref_clock = Overlay(ClockInputOverlayKey, new RefClockEnzianShellPlacer(this, ClockInputShellInput()))
  val ddr       = Overlay(DDROverlayKey, new DDREnzianShellPlacer(this, DDRShellInput()))
}

class EnzianShell()(implicit p: Parameters) extends EnzianShellBasicOverlays
{
  // Order matters; ddr depends on sys_clock
  val uart      = Overlay(UARTOverlayKey, new UARTEnzianShellPlacer(this, UARTShellInput()))

  val topDesign = LazyModule(p(DesignKey)(designParameters))

  // Place the sys_clock at the Shell if the user didn't ask for it
  designParameters(ClockInputOverlayKey).foreach { unused =>
    val source = unused.place(ClockInputDesignInput()).overlayOutput.node
    val sink = ClockSinkNode(Seq(ClockSinkParameters()))
    sink := source
  }

  override lazy val module = new LazyRawModuleImp(this) {

    val sysclk: Clock = sys_clock.get() match {
      case Some(x: SysClockEnzianPlacedOverlay) => x.clock
    }

    val powerOnReset: Bool = PowerOnResetFPGAOnly(sysclk)
    sdc.addAsyncPath(Seq(powerOnReset))

    pllReset := powerOnReset // (reset_ibuf.io.O || powerOnReset) // || ereset)
  }
}
