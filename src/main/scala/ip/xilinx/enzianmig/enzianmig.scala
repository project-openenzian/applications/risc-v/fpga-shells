package sifive.fpgashells.ip.xilinx.enzianmig

import Chisel._
import chisel3.experimental.{Analog,attach}
import freechips.rocketchip.util.{ElaborationArtefacts}
import freechips.rocketchip.util.GenericParameterizedBundle
import freechips.rocketchip.config._

class EnzianMIGIODDR(depth : BigInt) extends GenericParameterizedBundle(depth) {
  require((depth<=0x400000000L),"enzianmig supports upto 16GB depth configuraton")
  val c0_ddr4_adr           = Bits(OUTPUT,17) // Check
  val c0_ddr4_bg            = Bits(OUTPUT,2) // Check
  val c0_ddr4_ba            = Bits(OUTPUT,2) // Check
  val c0_ddr4_reset_n       = Bool(OUTPUT) // Check
  val c0_ddr4_act_n         = Bool(OUTPUT) // Check
  val c0_ddr4_ck_c          = Bits(OUTPUT,1) // Check
  val c0_ddr4_ck_t          = Bits(OUTPUT,1) // Check
  val c0_ddr4_cke           = Bits(OUTPUT,1) // Check
  val c0_ddr4_cs_n          = Bits(OUTPUT,1) // Check
  val c0_ddr4_odt           = Bits(OUTPUT,1) // Check

  val c0_ddr4_dq            = Analog(72.W) // Check
  val c0_ddr4_dqs_c         = Analog(18.W) // Check
  val c0_ddr4_dqs_t         = Analog(18.W) // Check
  val c0_ddr4_parity        = Bool(OUTPUT) // Check

  val c0_sys_clk_n          = Bool(INPUT)
  val c0_sys_clk_p          = Bool(INPUT)
}

//reused directly in io bundle for sifive.blocks.devices.xilinxvcu118mig
trait EnzianMIGIOClocksReset extends Bundle {
  //inputs
  //"NO_BUFFER" clock source (must be connected to IBUF outside of IP)
  // val c0_sys_clk_i              = Bool(INPUT)
  //user interface signals
  val c0_ddr4_ui_clk            = Clock(OUTPUT) // Check
  val c0_ddr4_ui_clk_sync_rst   = Bool(OUTPUT) // Check
  val c0_ddr4_aresetn           = Bool(INPUT) // Check
  //misc
  val c0_init_calib_complete    = Bool(OUTPUT) // Check
  val sys_rst                   = Bool(INPUT) // Check
}

trait MIGAXISignals extends Bundle {
  //slave interface write address ports
  val c0_ddr4_s_axi_awid            = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_awaddr          = Bits(INPUT,34) // Check
  val c0_ddr4_s_axi_awlen           = Bits(INPUT,8) // Check
  val c0_ddr4_s_axi_awsize          = Bits(INPUT,3) // Check
  val c0_ddr4_s_axi_awburst         = Bits(INPUT,2) // Check
  val c0_ddr4_s_axi_awlock          = Bits(INPUT,1) // Check
  val c0_ddr4_s_axi_awcache         = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_awprot          = Bits(INPUT,3) // Check
  val c0_ddr4_s_axi_awqos           = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_awvalid         = Bool(INPUT) // Check
  val c0_ddr4_s_axi_awready         = Bool(OUTPUT) // Check
  //slave interface write data ports
  val c0_ddr4_s_axi_wdata           = Bits(INPUT,64) // Check
  val c0_ddr4_s_axi_wstrb           = Bits(INPUT,8) // Check
  val c0_ddr4_s_axi_wlast           = Bool(INPUT) // Check
  val c0_ddr4_s_axi_wvalid          = Bool(INPUT) // Check
  val c0_ddr4_s_axi_wready          = Bool(OUTPUT) // Check
  //slave interface write response ports
  val c0_ddr4_s_axi_bready          = Bool(INPUT) // Check
  val c0_ddr4_s_axi_bid             = Bits(OUTPUT,4) // Check
  val c0_ddr4_s_axi_bresp           = Bits(OUTPUT,2) // Check
  val c0_ddr4_s_axi_bvalid          = Bool(OUTPUT) // Check
  //slave interface read address ports
  val c0_ddr4_s_axi_arid            = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_araddr          = Bits(INPUT,34) // Check
  val c0_ddr4_s_axi_arlen           = Bits(INPUT,8) // Check
  val c0_ddr4_s_axi_arsize          = Bits(INPUT,3) // Check
  val c0_ddr4_s_axi_arburst         = Bits(INPUT,2) // Check
  val c0_ddr4_s_axi_arlock          = Bits(INPUT,1) // Check
  val c0_ddr4_s_axi_arcache         = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_arprot          = Bits(INPUT,3) // Check
  val c0_ddr4_s_axi_arqos           = Bits(INPUT,4) // Check
  val c0_ddr4_s_axi_arvalid         = Bool(INPUT) // Check
  val c0_ddr4_s_axi_arready         = Bool(OUTPUT) // Check
  //slave interface read data ports
  val c0_ddr4_s_axi_rready          = Bool(INPUT) // Check
  val c0_ddr4_s_axi_rid             = Bits(OUTPUT,4) // Check
  val c0_ddr4_s_axi_rdata           = Bits(OUTPUT,64) // Check
  val c0_ddr4_s_axi_rresp           = Bits(OUTPUT,2) // Check
  val c0_ddr4_s_axi_rlast           = Bool(OUTPUT) // Check
  val c0_ddr4_s_axi_rvalid          = Bool(OUTPUT) // Check
}

trait ECISignals extends Bundle {
  val eci_gt_clk_link1_clk_n          = Bits(INPUT,3)
  val eci_gt_clk_link1_clk_p          = Bits(INPUT,3)
  val eci_gt_clk_link2_clk_n          = Bits(INPUT,3)
  val eci_gt_clk_link2_clk_p          = Bits(INPUT,3)
  val eci_gt_link1_grx_n              = Bits(INPUT,12)
  val eci_gt_link1_grx_p              = Bits(INPUT,12)
  val eci_gt_link1_gtx_n              = Bits(OUTPUT,12)
  val eci_gt_link1_gtx_p              = Bits(OUTPUT,12)
  val eci_gt_link2_grx_n              = Bits(INPUT,12)
  val eci_gt_link2_grx_p              = Bits(INPUT,12)
  val eci_gt_link2_gtx_n              = Bits(OUTPUT,12)
  val eci_gt_link2_gtx_p              = Bits(OUTPUT,12)
}

abstract class AbstractEnzianMIG(depth: BigInt)(implicit val p: Parameters) extends BlackBox {
  require((depth<=0x400000000L),"enzianmig supports upto 16GB depth configuraton")

  val io = new EnzianMIGIODDR(depth) with EnzianMIGIOClocksReset with MIGAXISignals
}

//scalastyle:off
//turn off linter: blackbox name must match verilog module
class enzianmig(depth : BigInt)(override implicit val p:Parameters) extends AbstractEnzianMIG(depth) { 

  ElaborationArtefacts.add(
    "enzianmig.vivado.tcl",
    """ 
    create_ip -vendor xilinx.com -library ip -version 2.2 -name ddr4 -module_name enzianmig -dir $ipdir -force
    set_property -dict [ list \
      CONFIG.No_Controller {1} \
      CONFIG.C0.DDR4_AxiSelection {true} \
      CONFIG.C0.DDR4_AxiAddressWidth {34} \
      CONFIG.C0.DDR4_AxiDataWidth {64} \
      CONFIG.C0.DDR4_AxiIDWidth {4} \
      CONFIG.C0.DDR4_CLKOUT0_DIVIDE {5} \
      CONFIG.C0.DDR4_CasLatency {17} \
      CONFIG.C0.DDR4_CasWriteLatency {12} \
      CONFIG.C0.DDR4_DataMask {NONE} \
      CONFIG.C0.DDR4_DataWidth {72} \
      CONFIG.C0.DDR4_EN_PARITY {true} \
      CONFIG.C0.DDR4_Ecc {true} \
      CONFIG.C0.DDR4_InputClockPeriod {9995} \
      CONFIG.C0.DDR4_MemoryPart {MTA18ASF2G72PZ-2G3} \
      CONFIG.C0.DDR4_MemoryType {RDIMMs} \
      CONFIG.C0.DDR4_TimePeriod {833} \
    ] [get_ips enzianmig]"""
  )
   
  // CONFIG.System_Clock {No_Buffer} \
}
//scalastyle:on

//scalastyle:off
//turn off linter: blackbox name must match verilog module
class enzianmigeci(depth : BigInt)(override implicit val p:Parameters) extends AbstractEnzianMIG(depth) { 

  override val io = new EnzianMIGIODDR(depth) with EnzianMIGIOClocksReset with MIGAXISignals with ECISignals

  ElaborationArtefacts.add(
    "enzianmigeci.vivado.tcl",
    """ 
    variable design_name
    set design_name enzianmigeci

    # If you do not already have an existing IP Integrator design open,
    # you can create a design using the following command:
    #    create_bd_design $design_name

    # Creating design if needed
    set errMsg ""
    set nRet 0

    set cur_design [current_bd_design -quiet]
    set list_cells [get_bd_cells -quiet]

    create_bd_design $design_name
    current_bd_design $design_name

    set parentCell [get_bd_cells /]

    # Get object for parentCell
    set parentObj [get_bd_cells $parentCell]
    if { $parentObj == "" } {
      catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
      return
    }

    # Make sure parentObj is hier blk
    set parentType [get_property TYPE $parentObj]
    if { $parentType ne "hier" } {
      catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
      return
    }

    # Save current instance; Restore later
    set oldCurInst [current_bd_instance .]

    # Set parent object as current
    current_bd_instance $parentObj

    # Create interface ports
    set c0_ddr4_s_axi [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 c0_ddr4_s_axi ]
    set_property -dict [ list \
    CONFIG.ADDR_WIDTH {34} \
    CONFIG.ARUSER_WIDTH {0} \
    CONFIG.AWUSER_WIDTH {0} \
    CONFIG.BUSER_WIDTH {0} \
    CONFIG.DATA_WIDTH {64} \
    CONFIG.FREQ_HZ {300000000} \
    CONFIG.HAS_BRESP {1} \
    CONFIG.HAS_BURST {1} \
    CONFIG.HAS_CACHE {1} \
    CONFIG.HAS_LOCK {1} \
    CONFIG.HAS_PROT {1} \
    CONFIG.HAS_QOS {1} \
    CONFIG.HAS_REGION {1} \
    CONFIG.HAS_RRESP {1} \
    CONFIG.HAS_WSTRB {1} \
    CONFIG.ID_WIDTH {4} \
    CONFIG.MAX_BURST_LENGTH {256} \
    CONFIG.NUM_READ_OUTSTANDING {1} \
    CONFIG.NUM_READ_THREADS {1} \
    CONFIG.NUM_WRITE_OUTSTANDING {1} \
    CONFIG.NUM_WRITE_THREADS {1} \
    CONFIG.PROTOCOL {AXI4} \
    CONFIG.READ_WRITE_MODE {READ_WRITE} \
    CONFIG.RUSER_BITS_PER_BYTE {0} \
    CONFIG.RUSER_WIDTH {0} \
    CONFIG.SUPPORTS_NARROW_BURST {1} \
    CONFIG.WUSER_BITS_PER_BYTE {0} \
    CONFIG.WUSER_WIDTH {0} \
    ] $c0_ddr4_s_axi

    set c0_sys [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 c0_sys ]
    set_property -dict [ list \
    CONFIG.FREQ_HZ {100000000} \
    ] $c0_sys

    set eci_gt_clk_link1 [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 eci_gt_clk_link1 ]
    set_property -dict [ list \
    CONFIG.FREQ_HZ {156250000} \
    ] $eci_gt_clk_link1

    set eci_gt_clk_link2 [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 eci_gt_clk_link2 ]
    set_property -dict [ list \
    CONFIG.FREQ_HZ {156250000} \
    ] $eci_gt_clk_link2

    set eci_gt_link1 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gt_rtl:1.0 eci_gt_link1 ]

    set eci_gt_link2 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gt_rtl:1.0 eci_gt_link2 ]

    # Create ports
    set c0_ddr4_act_n [ create_bd_port -dir O c0_ddr4_act_n ]
    set c0_ddr4_adr [ create_bd_port -dir O -from 16 -to 0 c0_ddr4_adr ]
    set c0_ddr4_aresetn [ create_bd_port -dir I -type rst c0_ddr4_aresetn ]
    set c0_ddr4_ba [ create_bd_port -dir O -from 1 -to 0 c0_ddr4_ba ]
    set c0_ddr4_bg [ create_bd_port -dir O -from 1 -to 0 c0_ddr4_bg ]
    set c0_ddr4_ck_c [ create_bd_port -dir O -from 0 -to 0 -type clk c0_ddr4_ck_c ]
    set c0_ddr4_ck_t [ create_bd_port -dir O -from 0 -to 0 -type clk c0_ddr4_ck_t ]
    set c0_ddr4_cke [ create_bd_port -dir O -from 0 -to 0 c0_ddr4_cke ]
    set c0_ddr4_cs_n [ create_bd_port -dir O -from 0 -to 0 c0_ddr4_cs_n ]
    set c0_ddr4_dq [ create_bd_port -dir IO -from 71 -to 0 c0_ddr4_dq ]
    set c0_ddr4_dqs_c [ create_bd_port -dir IO -from 17 -to 0 c0_ddr4_dqs_c ]
    set c0_ddr4_dqs_t [ create_bd_port -dir IO -from 17 -to 0 c0_ddr4_dqs_t ]
    set c0_ddr4_odt [ create_bd_port -dir O -from 0 -to 0 c0_ddr4_odt ]
    set c0_ddr4_parity [ create_bd_port -dir O c0_ddr4_parity ]
    set c0_ddr4_reset_n [ create_bd_port -dir O c0_ddr4_reset_n ]
    set c0_ddr4_ui_clk [ create_bd_port -dir O -type clk c0_ddr4_ui_clk ]
    set c0_ddr4_ui_clk_sync_rst [ create_bd_port -dir O -type rst c0_ddr4_ui_clk_sync_rst ]
    set c0_init_calib_complete [ create_bd_port -dir O c0_init_calib_complete ]
    set sys_rst [ create_bd_port -dir I -type rst sys_rst ]
    set_property -dict [ list \
    CONFIG.POLARITY {ACTIVE_HIGH} \
    ] $sys_rst

    # Create instance: axi_clock_converter_0, and set properties
    set axi_clock_converter_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_clock_converter:2.1 axi_clock_converter_0 ]

    # Create instance: clk_wiz, and set properties
    set clk_wiz [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz ]

    # Create instance: ddr4_0, and set properties
    set ddr4_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:ddr4:2.2 ddr4_0 ]
    set_property -dict [ list \
    CONFIG.C0.DDR4_AxiAddressWidth {34} \
    CONFIG.C0.DDR4_AxiDataWidth {64} \
    CONFIG.C0.DDR4_AxiIDWidth {4} \
    CONFIG.C0.DDR4_AxiSelection {true} \
    CONFIG.C0.DDR4_CLKOUT0_DIVIDE {5} \
    CONFIG.C0.DDR4_CasLatency {17} \
    CONFIG.C0.DDR4_CasWriteLatency {12} \
    CONFIG.C0.DDR4_DataMask {NONE} \
    CONFIG.C0.DDR4_DataWidth {72} \
    CONFIG.C0.DDR4_EN_PARITY {true} \
    CONFIG.C0.DDR4_Ecc {true} \
    CONFIG.C0.DDR4_InputClockPeriod {9995} \
    CONFIG.C0.DDR4_MemoryPart {MTA18ASF2G72PZ-2G3} \
    CONFIG.C0.DDR4_MemoryType {RDIMMs} \
    CONFIG.C0.DDR4_TimePeriod {833} \
    CONFIG.No_Controller {1} \
    ] $ddr4_0

    # Create instance: eci_module_0, and set properties
    set eci_module_0 [ create_bd_cell -type ip -vlnv ethz.ch:ECI:eci_module:1.0 eci_module_0 ]

    # Create instance: proc_sys_reset_0, and set properties
    set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0 ]

    # Create instance: proc_sys_reset_1, and set properties
    set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]

    # Create instance: smartconnect_0, and set properties
    set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
    set_property -dict [ list \
    CONFIG.ADVANCED_PROPERTIES { __view__ { }} \
    CONFIG.NUM_CLKS {2} \
    CONFIG.NUM_MI {2} \
    ] $smartconnect_0

    # Create interface connections
    connect_bd_intf_net -intf_net S01_AXI_0_1 [get_bd_intf_ports c0_ddr4_s_axi] [get_bd_intf_pins axi_clock_converter_0/S_AXI]
    connect_bd_intf_net -intf_net axi_clock_converter_0_M_AXI [get_bd_intf_pins axi_clock_converter_0/M_AXI] [get_bd_intf_pins smartconnect_0/S01_AXI]
    connect_bd_intf_net -intf_net c0_sys_clk [get_bd_intf_ports c0_sys] [get_bd_intf_pins ddr4_0/C0_SYS_CLK]
    connect_bd_intf_net -intf_net eci_gt_clk_link1_1_1 [get_bd_intf_ports eci_gt_clk_link1] [get_bd_intf_pins eci_module_0/eci_gt_clk_link1]
    connect_bd_intf_net -intf_net eci_gt_clk_link2_1_1 [get_bd_intf_ports eci_gt_clk_link2] [get_bd_intf_pins eci_module_0/eci_gt_clk_link2]
    connect_bd_intf_net -intf_net eci_module_0_eci_gt_link1 [get_bd_intf_ports eci_gt_link1] [get_bd_intf_pins eci_module_0/eci_gt_link1]
    connect_bd_intf_net -intf_net eci_module_0_eci_gt_link2 [get_bd_intf_ports eci_gt_link2] [get_bd_intf_pins eci_module_0/eci_gt_link2]
    connect_bd_intf_net -intf_net eci_module_0_m_io_axil [get_bd_intf_pins eci_module_0/m_io_axil] [get_bd_intf_pins smartconnect_0/S00_AXI]
    connect_bd_intf_net -intf_net smartconnect_0_M00_AXI [get_bd_intf_pins ddr4_0/C0_DDR4_S_AXI] [get_bd_intf_pins smartconnect_0/M00_AXI]
    connect_bd_intf_net -intf_net smartconnect_0_M01_AXI [get_bd_intf_pins ddr4_0/C0_DDR4_S_AXI_CTRL] [get_bd_intf_pins smartconnect_0/M01_AXI]

    # Create port connections
    connect_bd_net -net Net [get_bd_ports c0_ddr4_dqs_t] [get_bd_pins ddr4_0/c0_ddr4_dqs_t]
    connect_bd_net -net Net1 [get_bd_ports c0_ddr4_dqs_c] [get_bd_pins ddr4_0/c0_ddr4_dqs_c]
    connect_bd_net -net Net2 [get_bd_ports c0_ddr4_dq] [get_bd_pins ddr4_0/c0_ddr4_dq]
    connect_bd_net -net clk_wiz_clk_out1 [get_bd_pins clk_wiz/clk_out1] [get_bd_pins eci_module_0/clk_io]
    connect_bd_net -net ddr4_0_c0_ddr4_act_n [get_bd_ports c0_ddr4_act_n] [get_bd_pins ddr4_0/c0_ddr4_act_n]
    connect_bd_net -net ddr4_0_c0_ddr4_adr [get_bd_ports c0_ddr4_adr] [get_bd_pins ddr4_0/c0_ddr4_adr]
    connect_bd_net -net ddr4_0_c0_ddr4_ba [get_bd_ports c0_ddr4_ba] [get_bd_pins ddr4_0/c0_ddr4_ba]
    connect_bd_net -net ddr4_0_c0_ddr4_bg [get_bd_ports c0_ddr4_bg] [get_bd_pins ddr4_0/c0_ddr4_bg]
    connect_bd_net -net ddr4_0_c0_ddr4_ck_c [get_bd_ports c0_ddr4_ck_c] [get_bd_pins ddr4_0/c0_ddr4_ck_c]
    connect_bd_net -net ddr4_0_c0_ddr4_ck_t [get_bd_ports c0_ddr4_ck_t] [get_bd_pins ddr4_0/c0_ddr4_ck_t]
    connect_bd_net -net ddr4_0_c0_ddr4_cke [get_bd_ports c0_ddr4_cke] [get_bd_pins ddr4_0/c0_ddr4_cke]
    connect_bd_net -net ddr4_0_c0_ddr4_cs_n [get_bd_ports c0_ddr4_cs_n] [get_bd_pins ddr4_0/c0_ddr4_cs_n]
    connect_bd_net -net ddr4_0_c0_ddr4_odt [get_bd_ports c0_ddr4_odt] [get_bd_pins ddr4_0/c0_ddr4_odt]
    connect_bd_net -net ddr4_0_c0_ddr4_parity [get_bd_ports c0_ddr4_parity] [get_bd_pins ddr4_0/c0_ddr4_parity]
    connect_bd_net -net ddr4_0_c0_ddr4_reset_n [get_bd_ports c0_ddr4_reset_n] [get_bd_pins ddr4_0/c0_ddr4_reset_n]
    connect_bd_net -net ddr4_0_c0_ddr4_ui_clk [get_bd_ports c0_ddr4_ui_clk] [get_bd_pins axi_clock_converter_0/s_axi_aclk] [get_bd_pins clk_wiz/clk_in1] [get_bd_pins ddr4_0/c0_ddr4_ui_clk] [get_bd_pins proc_sys_reset_0/slowest_sync_clk] [get_bd_pins smartconnect_0/aclk1]
    connect_bd_net -net ddr4_0_c0_ddr4_ui_clk_sync_rst [get_bd_ports c0_ddr4_ui_clk_sync_rst] [get_bd_pins ddr4_0/c0_ddr4_ui_clk_sync_rst]
    connect_bd_net -net ddr4_0_c0_init_calib_complete [get_bd_ports c0_init_calib_complete] [get_bd_pins ddr4_0/c0_init_calib_complete]
    connect_bd_net -net eci_module_0_clk_sys [get_bd_pins axi_clock_converter_0/m_axi_aclk] [get_bd_pins eci_module_0/clk_sys] [get_bd_pins proc_sys_reset_1/slowest_sync_clk] [get_bd_pins smartconnect_0/aclk]
    connect_bd_net -net proc_sys_reset_0_peripheral_aresetn [get_bd_pins axi_clock_converter_0/s_axi_aresetn] [get_bd_pins ddr4_0/c0_ddr4_aresetn] [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins smartconnect_0/aresetn]
    connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins axi_clock_converter_0/m_axi_aresetn] [get_bd_pins proc_sys_reset_1/peripheral_aresetn]
    connect_bd_net -net rst_ddr4_0_300M_peripheral_aresetn [get_bd_ports c0_ddr4_aresetn] [get_bd_pins proc_sys_reset_0/ext_reset_in] [get_bd_pins proc_sys_reset_1/ext_reset_in]
    connect_bd_net -net sys_rst_0_1 [get_bd_ports sys_rst] [get_bd_pins clk_wiz/reset] [get_bd_pins ddr4_0/sys_rst]

    # Create address segments
    assign_bd_address -offset 0x00000000 -range 0x000400000000 -target_address_space [get_bd_addr_spaces eci_module_0/m_io_axil] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP/C0_DDR4_ADDRESS_BLOCK] -force
    assign_bd_address -offset 0x00000000 -range 0x000400000000 -target_address_space [get_bd_addr_spaces c0_ddr4_s_axi] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP/C0_DDR4_ADDRESS_BLOCK] -force

    # Exclude Address Segments
    exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces c0_ddr4_s_axi] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP_CTRL/C0_REG]
    exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces eci_module_0/m_io_axil] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP_CTRL/C0_REG]

    # Restore current instance
    current_bd_instance $oldCurInst

    validate_bd_design
    save_bd_design

    # Generate design
    set_property synth_checkpoint_mode None [get_files enzianmigeci.bd]
    generate_target all [get_files -all enzianmigeci.bd]
    read_bd [get_files -all enzianmigeci.bd]
    """
  )
}
