// See LICENSE for license details.
package sifive.fpgashells.devices.xilinx.xilinxenzianmig

import Chisel._
import chisel3.experimental.{Analog,attach}
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.config.Parameters
import freechips.rocketchip.subsystem._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._
import freechips.rocketchip.interrupts._
import sifive.fpgashells.ip.xilinx.enzianmig._

case class XilinxEnzianMIGParams(
  address : Seq[AddressSet],
  eci: Boolean
)

class XilinxEnzianMIGPads(depth : BigInt) extends EnzianMIGIODDR(depth) {
  def this(c : XilinxEnzianMIGParams) {
    this(AddressRange.fromSets(c.address).head.size)
  }
}

class XilinxEnzianMIGIO(depth : BigInt) extends EnzianMIGIODDR(depth) with EnzianMIGIOClocksReset
class XilinxEnzianMIGECIIO(depth : BigInt) extends EnzianMIGIODDR(depth) with EnzianMIGIOClocksReset with ECISignals

class XilinxEnzianMIGIsland(c : XilinxEnzianMIGParams)(implicit p: Parameters) extends LazyModule with CrossesToOnlyOneClockDomain {
  val ranges = AddressRange.fromSets(c.address)
  require (ranges.size == 1, "DDR range must be contiguous")
  val offset = ranges.head.base
  val depth = ranges.head.size
  val crossing = AsynchronousCrossing(8)
  require((depth<=0x400000000L),"enzianmig supports upto 16GB depth configuraton")
  
  val device = new MemoryDevice
  val node = AXI4SlaveNode(Seq(AXI4SlavePortParameters(
      slaves = Seq(AXI4SlaveParameters(
      address       = c.address,
      resources     = device.reg,
      regionType    = RegionType.UNCACHED,
      executable    = true,
      supportsWrite = TransferSizes(1, 256*8),
      supportsRead  = TransferSizes(1, 256*8))),
    beatBytes = 8)))

  lazy val module = new Impl
  class Impl extends LazyModuleImp(this) {
    val io = IO(new Bundle {
      var port = if (c.eci) {
        new XilinxEnzianMIGECIIO(depth)
      } else {
        new XilinxEnzianMIGIO(depth)
      }
    })

    //MIG black box instantiation
    val blackbox = if (c.eci) {
        Module(new enzianmigeci(depth))
      } else {
        Module(new enzianmig(depth))
      }
    val (axi_async, _) = node.in(0)

    //pins to top level

    //inouts
    attach(io.port.c0_ddr4_dq,blackbox.io.c0_ddr4_dq)
    attach(io.port.c0_ddr4_dqs_c,blackbox.io.c0_ddr4_dqs_c)
    attach(io.port.c0_ddr4_dqs_t,blackbox.io.c0_ddr4_dqs_t)

    //outputs
    io.port.c0_ddr4_adr         := blackbox.io.c0_ddr4_adr
    io.port.c0_ddr4_bg          := blackbox.io.c0_ddr4_bg
    io.port.c0_ddr4_ba          := blackbox.io.c0_ddr4_ba
    io.port.c0_ddr4_reset_n     := blackbox.io.c0_ddr4_reset_n
    io.port.c0_ddr4_act_n       := blackbox.io.c0_ddr4_act_n
    io.port.c0_ddr4_ck_c        := blackbox.io.c0_ddr4_ck_c
    io.port.c0_ddr4_ck_t        := blackbox.io.c0_ddr4_ck_t
    io.port.c0_ddr4_cke         := blackbox.io.c0_ddr4_cke
    io.port.c0_ddr4_cs_n        := blackbox.io.c0_ddr4_cs_n
    io.port.c0_ddr4_odt         := blackbox.io.c0_ddr4_odt
    io.port.c0_ddr4_parity      := blackbox.io.c0_ddr4_parity

    //inputs
    //clock
    blackbox.io.c0_sys_clk_n    := io.port.c0_sys_clk_n
    blackbox.io.c0_sys_clk_p    := io.port.c0_sys_clk_p

    io.port.c0_ddr4_ui_clk      := blackbox.io.c0_ddr4_ui_clk
    io.port.c0_ddr4_ui_clk_sync_rst := blackbox.io.c0_ddr4_ui_clk_sync_rst
    blackbox.io.c0_ddr4_aresetn := io.port.c0_ddr4_aresetn

    val awaddr = axi_async.aw.bits.addr - UInt(offset)
    val araddr = axi_async.ar.bits.addr - UInt(offset)

    //slave AXI interface write address ports
    blackbox.io.c0_ddr4_s_axi_awid    := axi_async.aw.bits.id
    blackbox.io.c0_ddr4_s_axi_awaddr  := awaddr //truncated
    blackbox.io.c0_ddr4_s_axi_awlen   := axi_async.aw.bits.len
    blackbox.io.c0_ddr4_s_axi_awsize  := axi_async.aw.bits.size
    blackbox.io.c0_ddr4_s_axi_awburst := axi_async.aw.bits.burst
    blackbox.io.c0_ddr4_s_axi_awlock  := axi_async.aw.bits.lock
    blackbox.io.c0_ddr4_s_axi_awcache := UInt("b0011")
    blackbox.io.c0_ddr4_s_axi_awprot  := axi_async.aw.bits.prot
    blackbox.io.c0_ddr4_s_axi_awqos   := axi_async.aw.bits.qos
    blackbox.io.c0_ddr4_s_axi_awvalid := axi_async.aw.valid
    axi_async.aw.ready        := blackbox.io.c0_ddr4_s_axi_awready

    //slave interface write data ports
    blackbox.io.c0_ddr4_s_axi_wdata   := axi_async.w.bits.data
    blackbox.io.c0_ddr4_s_axi_wstrb   := axi_async.w.bits.strb
    blackbox.io.c0_ddr4_s_axi_wlast   := axi_async.w.bits.last
    blackbox.io.c0_ddr4_s_axi_wvalid  := axi_async.w.valid
    axi_async.w.ready         := blackbox.io.c0_ddr4_s_axi_wready

    //slave interface write response
    blackbox.io.c0_ddr4_s_axi_bready  := axi_async.b.ready
    axi_async.b.bits.id       := blackbox.io.c0_ddr4_s_axi_bid
    axi_async.b.bits.resp     := blackbox.io.c0_ddr4_s_axi_bresp
    axi_async.b.valid         := blackbox.io.c0_ddr4_s_axi_bvalid

    //slave AXI interface read address ports
    blackbox.io.c0_ddr4_s_axi_arid    := axi_async.ar.bits.id
    blackbox.io.c0_ddr4_s_axi_araddr  := araddr // truncated
    blackbox.io.c0_ddr4_s_axi_arlen   := axi_async.ar.bits.len
    blackbox.io.c0_ddr4_s_axi_arsize  := axi_async.ar.bits.size
    blackbox.io.c0_ddr4_s_axi_arburst := axi_async.ar.bits.burst
    blackbox.io.c0_ddr4_s_axi_arlock  := axi_async.ar.bits.lock
    blackbox.io.c0_ddr4_s_axi_arcache := UInt("b0011")
    blackbox.io.c0_ddr4_s_axi_arprot  := axi_async.ar.bits.prot
    blackbox.io.c0_ddr4_s_axi_arqos   := axi_async.ar.bits.qos
    blackbox.io.c0_ddr4_s_axi_arvalid := axi_async.ar.valid
    axi_async.ar.ready        := blackbox.io.c0_ddr4_s_axi_arready

    //slace AXI interface read data ports
    blackbox.io.c0_ddr4_s_axi_rready  := axi_async.r.ready
    axi_async.r.bits.id       := blackbox.io.c0_ddr4_s_axi_rid
    axi_async.r.bits.data     := blackbox.io.c0_ddr4_s_axi_rdata
    axi_async.r.bits.resp     := blackbox.io.c0_ddr4_s_axi_rresp
    axi_async.r.bits.last     := blackbox.io.c0_ddr4_s_axi_rlast
    axi_async.r.valid         := blackbox.io.c0_ddr4_s_axi_rvalid

    //misc
    io.port.c0_init_calib_complete := blackbox.io.c0_init_calib_complete
    blackbox.io.sys_rst            := io.port.sys_rst

    //eci signals
    if (c.eci) {
      val io_port = io.port.asInstanceOf[XilinxEnzianMIGECIIO]
      val blackbox_io = blackbox.io.asInstanceOf[ECISignals]
      
      io_port.eci_gt_link1_gtx_n          := blackbox_io.eci_gt_link1_gtx_n
      io_port.eci_gt_link1_gtx_p          := blackbox_io.eci_gt_link1_gtx_p
      io_port.eci_gt_link2_gtx_n          := blackbox_io.eci_gt_link2_gtx_n
      io_port.eci_gt_link2_gtx_p          := blackbox_io.eci_gt_link2_gtx_p

      blackbox_io.eci_gt_clk_link1_clk_n  := io_port.eci_gt_clk_link1_clk_n
      blackbox_io.eci_gt_clk_link1_clk_p  := io_port.eci_gt_clk_link1_clk_p
      blackbox_io.eci_gt_clk_link2_clk_n  := io_port.eci_gt_clk_link2_clk_n
      blackbox_io.eci_gt_clk_link2_clk_p  := io_port.eci_gt_clk_link2_clk_p
      blackbox_io.eci_gt_link1_grx_n      := io_port.eci_gt_link1_grx_n
      blackbox_io.eci_gt_link1_grx_p      := io_port.eci_gt_link1_grx_p
      blackbox_io.eci_gt_link2_grx_n      := io_port.eci_gt_link2_grx_n
      blackbox_io.eci_gt_link2_grx_p      := io_port.eci_gt_link2_grx_p
    }
  }
}

class XilinxEnzianMIG(c : XilinxEnzianMIGParams)(implicit p: Parameters) extends LazyModule {
  val ranges = AddressRange.fromSets(c.address)
  val depth = ranges.head.size

  val buffer  = LazyModule(new TLBuffer)
  val toaxi4  = LazyModule(new TLToAXI4(adapterName = Some("mem")))
  val indexer = LazyModule(new AXI4IdIndexer(idBits = 4))
  val deint   = LazyModule(new AXI4Deinterleaver(p(CacheBlockBytes)))
  val yank    = LazyModule(new AXI4UserYanker)
  val island  = LazyModule(new XilinxEnzianMIGIsland(c))

  val node: TLInwardNode =
    island.crossAXI4In(island.node) := yank.node := deint.node := indexer.node := toaxi4.node := buffer.node

  lazy val module = new Impl
  class Impl extends LazyModuleImp(this) {
    val io = IO(new Bundle {
      val port = if (c.eci) {
        new XilinxEnzianMIGIO(depth) with ECISignals
      } else {
        new XilinxEnzianMIGIO(depth)
      }
    })

    io.port <> island.module.io.port

    // Shove the island
    island.module.clock := io.port.c0_ddr4_ui_clk
    island.module.reset := io.port.c0_ddr4_ui_clk_sync_rst
  }
}