# Enzian ECI module IP

Enzian ECI module IP provides an AXI Lite bus accessible from the CPU. It converts incoming ECI I/O transactions (IOBLD/IOBST) to AXI Lite transactions. The I/O space starts from 0x900000000000 ("9" + 11 zeros) 

Add the the module directory to the Vivado IP repositories and you can instantiate "eci_module".

Port description:
* clk_io - free-running 100MHz clock input used to initialize the GTs
* clk_sys - 322MHz clock output
* link_up - state of the link
* reset_sys - power-on-reset output
* m_io_axil - AXI Lite master
* eci_gt_clk_link1 - ECI link 1 GT reference clock input
* eci_gt_clk_link2 - ECI link 2 GT reference clock input
* eci_gt_link1 - ECI link 1 RX/TX interface
* eci_gt_link2 - ECI link 2 RX/TX interface
