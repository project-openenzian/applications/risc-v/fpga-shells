# ECI link 1
set_property PACKAGE_PIN AT11 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[0\].ref_buf_link1/I}]]]
set_property PACKAGE_PIN AT10 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[0\].ref_buf_link1/IB}]]]
set_property PACKAGE_PIN AM11 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[1\].ref_buf_link1/I}]]]
set_property PACKAGE_PIN AM10 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[1\].ref_buf_link1/IB}]]]
set_property PACKAGE_PIN AH11 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[2\].ref_buf_link1/I}]]]
set_property PACKAGE_PIN AH10 [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[2\].ref_buf_link1/IB}]]]

create_clock -period 6.400 -name {gt_eci_clk_p_link1[0]} -waveform {0.000 3.200} [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[0\].ref_buf_link1/I}]]]
create_clock -period 6.400 -name {gt_eci_clk_p_link1[1]} -waveform {0.000 3.200} [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[1\].ref_buf_link1/I}]]]
create_clock -period 6.400 -name {gt_eci_clk_p_link1[2]} -waveform {0.000 3.200} [get_ports -of_objects [get_nets -of [get_pins -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/eci_refclks\[2\].ref_buf_link1/I}]]]

# Disable delay alignment
set_property RXSYNC_SKIP_DA 1'b1 [get_cells -hierarchical -regexp -filter {NAME =~ .*i_eci_layer/xcvr1/inst/gen_gtwizard_gtye4_top.xcvr_link.*GTYE4_CHANNEL_PRIM_INST}]
