/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2020-12-18
 * Project : Enzian
 *
 */

`ifndef GEN_VC_ECIH_ROUTER_SV
`define GEN_VC_ECIH_ROUTER_SV

/*
 * Module Description:
 *  Generic ECI header router 
 *  * Deserializes the incoming VC packet into ECI header requests 
 *    and sends it to the output handler 
 *  * Module to be used only on VCs that do not have any data payload 
 *    only header 
 *  * Supports only 1 output handler, that is why it is generic  
 *
 * Input Output Description:
 *  Input - Data from req/resp WITHOUT data VC + vc number 
 *  Output - ECI packet without data + vc number 
 *
 * Architecture Description:
 *  Uses simple deserializer to get the next header from the VC packet 
 *  and enables output valid ready signal for the handler 
 *
 * Modifiable Parameters:
 *  VC_NUM - the VC number to which this module is attached to, mandatory 
 *
 * Modules Used:
 *  simple_deserializer
 *
 * Notes:
 * * Only header no data 
 *
 */

import eci_cmd_defs::*;

module gen_vc_ecih_router #
  (
   // Mandatory parameter 
   parameter VC_NUM = 11,
   parameter VC_SIZE = 7,
   parameter VC_SIZE_WIDTH = $clog2(VC_SIZE) + ( (VC_SIZE % 2 ) == 0 )
   )
   (
    input logic 			      clk,
    input logic 			      reset,
    // Input req/resp without data from VVC
    input logic [VC_SIZE-1:0][ECI_WORD_WIDTH-1:0] vc_pkt_i,
    input logic [VC_SIZE-1:0] 	  vc_pkt_word_enable_i,
    input logic 			      vc_pkt_valid_i,
    output logic 			      vc_pkt_ready_o,
    // Output ECI packet without data (only header)
    output logic [ECI_WORD_WIDTH-1:0] 	      eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0]  eci_pkt_size_o,
    output logic 			      eci_pkt_valid_o,
    input logic 			      eci_pkt_ready_i,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0]  eci_pkt_vc_o
    );
   // Register to latch VC number and size
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      eci_pkt_size_reg = '0;
   logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 	      eci_pkt_vc_reg = '0;

   // Assign registers to outputs
   assign eci_pkt_size_o = eci_pkt_size_reg;
   assign eci_pkt_vc_o = eci_pkt_vc_reg;

   // Pop 1 ECI header without data from VC stream
   // simple_deserializer module used as there are no payloads
    vc_word_extractor vc_simp_des1
    (
        .clk    (clk),
        .input_words     (vc_pkt_i),
        .input_valid     (vc_pkt_valid_i),
        .input_word_enable   (vc_pkt_word_enable_i),
        .input_ready     (vc_pkt_ready_o),
        .output_word     (eci_hdr_o),
        .output_valid    (eci_pkt_valid_o),
        .output_ready    (eci_pkt_ready_i)
    );

   always_ff @(posedge clk) begin : REG_ASSIGN
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 eci_pkt_size_reg <= '0;
	 eci_pkt_vc_reg <= '0;
      end else begin
	 // Size is always 1 (only header)
	 eci_pkt_size_reg <= 'd1;
	 // VC number is always a constant 
	 eci_pkt_vc_reg <= ECI_TOT_NUM_VCS_WIDTH'(VC_NUM);
      end
   end : REG_ASSIGN
endmodule // gen_vc_ecih_router
`endif
