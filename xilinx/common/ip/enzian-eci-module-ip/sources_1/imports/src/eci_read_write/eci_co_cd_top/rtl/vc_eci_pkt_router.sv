/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2021-01-05
 * Project : Enzian
 *
 */

`ifndef VC_ECI_PKT_ROUTER_SV
`define VC_ECI_PKT_ROUTER_SV

/*
 * Module Description:
 *  Gets CPU initiated events from VCs 11-2, generates ECI packet and
 *  routes to appropriate handler interface 
 * 
 *  For each VC, there is a generic ECI interface to which the ECI packet 
 *  contained in the VC gets routed to 
 *  There are special events in VC 6,7 namely GSYNC, GINV which are not
 *  routed to the generic interface 
 *   GSYNC gets routed to a GSYNC handler interface 
 *   GINV is ignored 
 *   Any other ECI event from VC6,7 gets routed to generic ECI interface 
 * 
 * Input Output Description:
 *
 *
 * Architecture Description:
 * gen_vc_ecih_router
 *  VC 11-8 contains events with no payload and only header 
 *  each ECI header is extracted and routed by the gen_vc_ecih_router 
 * 
 * gen_vc_ecip_router 
 *  VC 5-2 contains events with header + payload 
 *  the ECI packet is extracted and routed by the gen_vc_ecip_router 
 * 
 * c_route_co_vc6_7
 *  VC 7,6 contains special events GSYNC, GINV
 *  these events are differentiated from other events in VC 6,7 and routed 
 *  to appropriate handlers by this module 
 *
 * Modifiable Parameters:
 *  None
 *
 * Non-modifiable Parameters:
 *  VC_SIZE - the number of data words in a VC 
 *  VC_SIZE_WIDTH - number of bits to indicate number of valid words in a VC
 * 
 * Modules Used:
 *  gen_vc_ecih_router
 *  gen_vc_ecip_router
 *  c_route_co_vc6_7
 *
 * Notes:
 *
 *
 */


//////////////////////////////////////////////////////////////////////////////////////////////
//                            +--------------------+					    //
//                            |                    |					    //
//          VC Packet  +7*64+>+ gen_vc_ecih_router ++1*64+-> ECI header			    //
//                            |                    |					    //
//                            +--------------------+					    //
// VC 11 to 8                                                   ECI header from VCs 11 to 8 //
// (no payload)                     ........			    ----->		    //
//  ---->                     +--------------------+					    //
//                            |                    |					    //
//         VC Packet  ++7*64+>+ gen_vc_ecih_router ++1*64+-> ECI header			    //
//                            |                    |					    //
//                            +--------------------+		ECI header from VCs 7, 6    //
// 								    ----->  		    //
//                            +--------------------+					    //
//         VC Packet  ++7*64+>+                    ++1*64+-> GSYNC header		    //
//                            | c_route_co_vc67    |					    //
//                            |                    ++1*64+-> ECI header (non GSYNC/GINV)    //
// VC 7, 6                    +--------------------+					    //
// (no payload)										    //
// ---->                      +--------------------+					    //
//         VC Packet  ++7*64+>+                    ++1*64+-> GSYNC header		    //
//                            | c_route_co_vc67    |					    //
//                            |                    ++1*64+-> ECI header (non GSYNC/GINV)    //
//                            +--------------------+					    //
// 											    //
//                            +--------------------+					    //
//                            |                    |					    //
//         VC Packet  ++7*64+>+ gen_vc_ecip_router ++17*64-> ECI packet			    //
//                            |                    |					    //
//  VC 5 to 2                 +--------------------+					    //
//  (hdr + payload)                  .........                  ECI packet from VCs 5 to 2  //
//  ---->  							    ----->   		    //
//                            +--------------------+					    //
//                            |                    |					    //
//         VC Packet  ++7*64+>+ gen_vc_ecip_router ++17*64-> ECI packet			    //
//                            |                    |					    //
//                            +--------------------+					    //
//////////////////////////////////////////////////////////////////////////////////////////////

import eci_cmd_defs::*;

// Module takes data from VCs, deserializes it
// and generates ECI packets corresponding to events in the VCs 
module vc_eci_pkt_router #
  (
   parameter VC_SIZE = 7,
   parameter VC_SIZE_WIDTH = $clog2(VC_SIZE) + ( (VC_SIZE % 2 ) == 0 )
   )
   (
    input logic 					   clk,
    input logic 					   reset,
    // Incoming CPU to FPGA events (MIB VCs) c_ -> CPU initated events
    // VC11 to VC2 (VC1, VC0) are IO VCs not handled here
    // Incoming response without data VC 11,10
    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c11_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c11_vc_pkt_word_enable_i,
    input logic 					   c11_vc_pkt_valid_i,
    output logic 					   c11_vc_pkt_ready_o,

    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c10_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c10_vc_pkt_word_enable_i,
    input logic 					   c10_vc_pkt_valid_i,
    output logic 					   c10_vc_pkt_ready_o,
    // Incoming Forwards VC 9,8
    // Forwards are command only no data, currently not connected
    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c9_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c9_vc_pkt_word_enable_i,
    input logic 					   c9_vc_pkt_valid_i,
    output logic 					   c9_vc_pkt_ready_o,

    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c8_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c8_vc_pkt_word_enable_i,
    input logic 					   c8_vc_pkt_valid_i,
    output logic 					   c8_vc_pkt_ready_o,
    // Incoming requests without data VC 7,6
    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c7_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c7_vc_pkt_word_enable_i,
    input logic 					   c7_vc_pkt_valid_i,
    output logic 					   c7_vc_pkt_ready_o,

    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c6_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c6_vc_pkt_word_enable_i,
    input logic 					   c6_vc_pkt_valid_i,
    output logic 					   c6_vc_pkt_ready_o,
    // Incoming resoponse with data VC 5,4
    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c5_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c5_vc_pkt_word_enable_i,
    input logic 					   c5_vc_pkt_valid_i,
    output logic 					   c5_vc_pkt_ready_o,

    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c4_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c4_vc_pkt_word_enable_i,
    input logic 					   c4_vc_pkt_valid_i,
    output logic 					   c4_vc_pkt_ready_o,
    // Incoming request with data VC 3,2
    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c3_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c3_vc_pkt_word_enable_i,
    input logic 					   c3_vc_pkt_valid_i,
    output logic 					   c3_vc_pkt_ready_o,

    input logic [VC_SIZE*ECI_WORD_WIDTH-1:0] 		   c2_vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   c2_vc_pkt_word_enable_i,
    input logic 					   c2_vc_pkt_valid_i,
    output logic 					   c2_vc_pkt_ready_o,

    // ECI packets corresponding to the VCs holding CPU initiated events 
    // ECI pakcets for VC 11 - 2 (VC1,0 are IO VCs not handled here)
    // ECI packet for CPU initiated response without data VC 11,10
    // No payload only header 
    output logic [ECI_WORD_WIDTH-1:0] 			   c11_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c11_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c11_eci_pkt_vc_o,
    output logic 					   c11_eci_pkt_valid_o,
    input logic 					   c11_eci_pkt_ready_i,

    output logic [ECI_WORD_WIDTH-1:0] 			   c10_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c10_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c10_eci_pkt_vc_o,
    output logic 					   c10_eci_pkt_valid_o,
    input logic 					   c10_eci_pkt_ready_i,
    // ECI packet for CPU initiated forwards VC 9,8
    // No payload only header
    output logic [ECI_WORD_WIDTH-1:0] 			   c9_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c9_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c9_eci_pkt_vc_o,
    output logic 					   c9_eci_pkt_valid_o,
    input logic 					   c9_eci_pkt_ready_i,

    output logic [ECI_WORD_WIDTH-1:0] 			   c8_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c8_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c8_eci_pkt_vc_o,
    output logic 					   c8_eci_pkt_valid_o,
    input logic 					   c8_eci_pkt_ready_i,
    // ECI packet for CPU initiated request without data VC 7,6
    // No payload only header
    output logic [ECI_WORD_WIDTH-1:0] 			   c7_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c7_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c7_eci_pkt_vc_o,
    output logic 					   c7_eci_pkt_valid_o,
    input logic 					   c7_eci_pkt_ready_i,

    output logic [ECI_WORD_WIDTH-1:0] 			   c6_eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c6_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c6_eci_pkt_vc_o,
    output logic 					   c6_eci_pkt_valid_o,
    input logic 					   c6_eci_pkt_ready_i,
    // ECI packet for CPU initiated response with data VC 5,4
    // Header + payload
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] c5_eci_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c5_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c5_eci_pkt_vc_o,
    output logic 					   c5_eci_pkt_valid_o,
    input logic 					   c5_eci_pkt_ready_i,
    
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] c4_eci_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c4_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c4_eci_pkt_vc_o,
    output logic 					   c4_eci_pkt_valid_o,
    input logic 					   c4_eci_pkt_ready_i,
    // ECI packet for CPU initiated requests with data VC 3,2
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] c3_eci_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c3_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c3_eci_pkt_vc_o,
    output logic 					   c3_eci_pkt_valid_o,
    input logic 					   c3_eci_pkt_ready_i,
    
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] c2_eci_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   c2_eci_pkt_size_o,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   c2_eci_pkt_vc_o,
    output logic 					   c2_eci_pkt_valid_o,
    input logic 					   c2_eci_pkt_ready_i,
    // VC 1,0 are IO VCs, not handled here
    // Special Handlers
    // GSYNC from VC7,6 (req without data) 
    output logic [ECI_WORD_WIDTH-1:0] 			   c7_gsync_hdr_o,
    output logic 					   c7_gsync_valid_o,
    input logic 					   c7_gsync_ready_i,

    output logic [ECI_WORD_WIDTH-1:0] 			   c6_gsync_hdr_o,
    output logic 					   c6_gsync_valid_o,
    input logic 					   c6_gsync_ready_i

    );

   // Converts the VC packet to ECI packet and routes to appropriate handler
   // ECI packets from all VCs have a generic handler
   // VC 7,6 (requests without data) have special requirements.
   //   GSYNC commands are handled by a GSYNC handler and not generic handler
   //   GINV commands are to be ignored
   //   Any other event is handled by generic handler
   // Arch note: 
   // For VCs with events without payload, gen_vc_ecih_router is used to generate the ECI packet
   // For VCs with events with payload, gen_vc_ecip_router is used to generate ECI packets
   // For VC 7,6 c_route_co_vc6_7 is used to generate and route eci packet

   
   // CPU initiated response without data VC 11,10
   // deserialize the header and send it to corresponding output
   gen_vc_ecih_router #
     (
      .VC_NUM(11),
      .VC_SIZE(VC_SIZE)
      )
   c11_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c11_vc_pkt_i),
      .vc_pkt_word_enable_i	(c11_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c11_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c11_vc_pkt_ready_o),
      // ECI packet output
      .eci_hdr_o	(c11_eci_hdr_o),
      .eci_pkt_size_o	(c11_eci_pkt_size_o),
      .eci_pkt_vc_o	(c11_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c11_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c11_eci_pkt_ready_i)
      );

   gen_vc_ecih_router #
     (
      .VC_NUM(10),
      .VC_SIZE(VC_SIZE)
      )
   c10_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c10_vc_pkt_i),
      .vc_pkt_word_enable_i	(c10_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c10_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c10_vc_pkt_ready_o),
      // ECI packet output
      .eci_hdr_o	(c10_eci_hdr_o),
      .eci_pkt_size_o	(c10_eci_pkt_size_o),
      .eci_pkt_vc_o	(c10_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c10_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c10_eci_pkt_ready_i)
      );

   // CPU initiated forwards (no payload) VC 9,8
   gen_vc_ecih_router #
     (
      .VC_NUM(9),
      .VC_SIZE(VC_SIZE)
      )
   c9_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c9_vc_pkt_i),
      .vc_pkt_word_enable_i	(c9_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c9_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c9_vc_pkt_ready_o),
      // ECI packet output
      .eci_hdr_o	(c9_eci_hdr_o),
      .eci_pkt_size_o	(c9_eci_pkt_size_o),
      .eci_pkt_vc_o	(c9_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c9_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c9_eci_pkt_ready_i)
      );

   gen_vc_ecih_router #
     (
      .VC_NUM(8),
      .VC_SIZE(VC_SIZE)
      )
   c8_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c8_vc_pkt_i),
      .vc_pkt_word_enable_i	(c8_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c8_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c8_vc_pkt_ready_o),
      // ECI packet output
      .eci_hdr_o	(c8_eci_hdr_o),
      .eci_pkt_size_o	(c8_eci_pkt_size_o),
      .eci_pkt_vc_o	(c8_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c8_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c8_eci_pkt_ready_i)
      );

   // CPU initiated requests without data VC 7,6
   // Note: c_route_co_vc6_7 module differentiates GSYNC, GINV
   // events from rest of events in VC 7,6
   // GSYNC is handled by a special handler and GINV is ignored
   // rest of the events are propoageted to generic eci interface
   c_route_co_vc6_7 #
     (
      .VC_NUM(7),
      .VC_SIZE(VC_SIZE)
      )
   c7_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input
      .vc_pkt_i		(c7_vc_pkt_i),
      .vc_pkt_word_enable_i	(c7_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c7_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c7_vc_pkt_ready_o),
      // GSYNC handler
      .gsync_hdr_o	(c7_gsync_hdr_o),
      .gsync_valid_o	(c7_gsync_valid_o),
      .gsync_ready_i	(c7_gsync_ready_i),
      // GINV has no handler, GINV is ignored
      // Generic handler
      .eci_hdr_o	(c7_eci_hdr_o),
      .eci_pkt_size_o	(c7_eci_pkt_size_o),
      .eci_pkt_vc_o	(c7_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c7_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c7_eci_pkt_ready_i)
      );

   c_route_co_vc6_7 #
     (
      .VC_NUM(6),
      .VC_SIZE(VC_SIZE)
      )
   c6_vc_ecih_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input
      .vc_pkt_i		(c6_vc_pkt_i),
      .vc_pkt_word_enable_i	(c6_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c6_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c6_vc_pkt_ready_o),
      // GSYNC handler
      .gsync_hdr_o	(c6_gsync_hdr_o),
      .gsync_valid_o	(c6_gsync_valid_o),
      .gsync_ready_i	(c6_gsync_ready_i),
      // GINV has no handler, is ignored 
      // Generic handler
      .eci_hdr_o	(c6_eci_hdr_o),
      .eci_pkt_size_o	(c6_eci_pkt_size_o),
      .eci_pkt_vc_o	(c6_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c6_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c6_eci_pkt_ready_i)
      );

   // CPU initiated resoponse with data VC 5,4
   // Has header + payload
   // using gen_vc_ecip_router
   gen_vc_ecip_router #
     (
      .VC_NUM(5),
      .VC_SIZE(VC_SIZE)
       )
   c5_vc_ecip_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c5_vc_pkt_i),
      .vc_pkt_word_enable_i	(c5_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c5_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c5_vc_pkt_ready_o),
      // ECI packet output
      .eci_pkt_o	(c5_eci_pkt_o),
      .eci_pkt_size_o	(c5_eci_pkt_size_o),
      .eci_pkt_vc_o	(c5_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c5_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c5_eci_pkt_ready_i)
      );

   gen_vc_ecip_router #
     (
      .VC_NUM(4),
      .VC_SIZE(VC_SIZE)
       )
   c4_vc_ecip_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c4_vc_pkt_i),
      .vc_pkt_word_enable_i	(c4_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c4_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c4_vc_pkt_ready_o),
      // ECI packet output
      .eci_pkt_o	(c4_eci_pkt_o),
      .eci_pkt_size_o	(c4_eci_pkt_size_o),
      .eci_pkt_vc_o	(c4_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c4_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c4_eci_pkt_ready_i)
      );

   // CPU initiated request with data VC 3,2
   // Has header + payload
   // using gen_vc_ecip_router
   gen_vc_ecip_router #
     (
      .VC_NUM(3),
      .VC_SIZE(VC_SIZE)
       )
   c3_vc_ecip_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c3_vc_pkt_i),
      .vc_pkt_word_enable_i	(c3_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c3_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c3_vc_pkt_ready_o),
      // ECI packet output
      .eci_pkt_o	(c3_eci_pkt_o),
      .eci_pkt_size_o	(c3_eci_pkt_size_o),
      .eci_pkt_vc_o	(c3_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c3_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c3_eci_pkt_ready_i)
      );

   gen_vc_ecip_router #
     (
      .VC_NUM(2),
      .VC_SIZE(VC_SIZE)
       )
   c2_vc_ecip_router
     (
      .clk(clk),
      .reset(reset),
      // VC packet input 
      .vc_pkt_i		(c2_vc_pkt_i),
      .vc_pkt_word_enable_i	(c2_vc_pkt_word_enable_i),
      .vc_pkt_valid_i	(c2_vc_pkt_valid_i),
      .vc_pkt_ready_o	(c2_vc_pkt_ready_o),
      // ECI packet output
      .eci_pkt_o	(c2_eci_pkt_o),
      .eci_pkt_size_o	(c2_eci_pkt_size_o),
      .eci_pkt_vc_o	(c2_eci_pkt_vc_o),
      .eci_pkt_valid_o	(c2_eci_pkt_valid_o),
      .eci_pkt_ready_i	(c2_eci_pkt_ready_i)
      );
endmodule // vc_eci_pkt_router
`endif
