/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2021-07-26
 * Project : Enzian
 *
 */

`ifndef VC_ECI_PACKETIZER_SV
 `define VC_ECI_PACKETIZER_SV

/*
 * Module Description:
 *  Module to read data from VC Fifos and create ECI packets.
 *  Data from VCs are fed into a circular buffer.
 *  
 *  whenever an ECI packet is available in the ciruclar buffer,
 *  it is latched onto the output stage. 
 *  
 *  Fully pipelined, II = 1.
 *
 * Input Output Description:
 *  Input: VC packet, size can vary between 0 to 7 words
 *  Output: ECI packet, size can vary betweeen 0 to 17 words.
 *
 * Architecture Description:
 *  Data available from the VC is written into a circular buffer and when ECI 
 *  packet is ready, gets read into the output stage.
 * 
 *  The circular buffer has two pointers, 
 *   write pointer points to first empty spot available to write. 
 *   read pointer points to the header of the ECI packet that is being aggregated.
 *  
 *  VC data can be written into circular buffer till buffer gets full.
 *  Data from circular buffer will be latched into output stage when 
 *   eci packet is available and flow control allows. 
 * 
 * Modules Used:
 *   eci_get_num_words_in_pkt - read ECI header to get the size 
 *   of the ECI packet being aggregated. 
 */

import eci_cmd_defs::*;

module vc_eci_packetizer #
  (
   parameter VC_MAX_SIZE = 7,
   parameter VC_MAX_SIZE_BITS = 3,
   parameter WORD_WIDTH = ECI_WORD_WIDTH,
   parameter MAX_PACKET_SIZE = ECI_PACKET_SIZE,
   parameter MAX_PACKET_SIZE_WIDTH = ECI_PACKET_SIZE_WIDTH
   ) 
   (
    input logic 				       clk,
    input logic 				       reset,

    // Input data stream from VC
    input logic [VC_MAX_SIZE-1:0][WORD_WIDTH-1:0]      vc_data_i,
    input logic [VC_MAX_SIZE-1:0] 		       vc_word_enable_i,
    input logic 				       vc_valid_i,
    output logic 				       vc_ready_o,

    // ECI packet stream output
    output logic [MAX_PACKET_SIZE-1:0][WORD_WIDTH-1:0] eci_pkt_o,
    output logic [MAX_PACKET_SIZE_WIDTH-1:0] 	       eci_pkt_size_o,
    output logic 				       eci_pkt_valid_o,
    input logic 				       eci_pkt_ready_i
    );

   localparam NREGS = 32;
   localparam NREGS_ADDR_WIDTH = $clog2(NREGS);
   // NREGS can vary from 0 to NREGS ( NREGS + 1  elements), additional bit might be needed
   localparam NREGS_COUNT_WIDTH = $clog2(NREGS) + ( (NREGS % 2 ) == 0 );
   
   // Output stage.
   logic [MAX_PACKET_SIZE-1:0][WORD_WIDTH-1:0] 	       eci_pkt_reg		= '0, eci_pkt_next;
   logic [MAX_PACKET_SIZE_WIDTH-1:0] 		       eci_pkt_size_reg		= '0, eci_pkt_size_next;
   logic 					       eci_pkt_valid_reg	= '0, eci_pkt_valid_next;

   // Circular buffer.
   logic [(NREGS - 1):0][(WORD_WIDTH - 1):0] 	       cbuf_reg = '0, cbuf_next;
   logic [(NREGS_ADDR_WIDTH - 1):0] 		       wr_addr_reg = '0, wr_addr_next;
   logic [(NREGS_ADDR_WIDTH - 1):0] 		       rd_addr_reg = '0, rd_addr_next;
   logic 					       cbuf_we;
   logic 					       cbuf_rd_valid;

   // Cbuf Control signals.
   logic [(NREGS_COUNT_WIDTH - 1):0] 		       num_words_in_cbuf;
   logic 					       cbuf_full;

   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		       eci_hdr_size;
   

   // Assign registers to outputs. 
   assign eci_pkt_o		= eci_pkt_reg;  
   assign eci_pkt_size_o	= eci_pkt_size_reg; 
   assign eci_pkt_valid_o	= eci_pkt_valid_reg;

   // wr_addr_reg points to first empty spot available to write.
   // rd_addr_reg points to first non-empty spot to read from (unless cbuf is empty).
   assign num_words_in_cbuf = (wr_addr_reg >= rd_addr_reg) ? 
			      (wr_addr_reg - rd_addr_reg) :
			      ((NREGS) - (rd_addr_reg - wr_addr_reg));

   assign cbuf_full = ((num_words_in_cbuf + VC_MAX_SIZE) >= NREGS ) ? 1 : 0;
   assign vc_ready_o = ~cbuf_full;
   assign cbuf_we = vc_valid_i & vc_ready_o;
   
   // circular buffer has aggregated a full ECI packet when the number of
   // words in the circular buffer exceeds the size of eci packet mentioned
   // in its header.
   assign cbuf_rd_valid = (num_words_in_cbuf >= eci_hdr_size) ? 1 : 0;

   // Input to circular buffer.
   always_comb begin : CBUF_IN
      wr_addr_next = wr_addr_reg;
      cbuf_next = cbuf_reg;      
      if(cbuf_we) begin
	 for(integer i = 0; i < VC_MAX_SIZE; i = i + 1) begin
	   if (vc_word_enable_i[i]) begin
	    cbuf_next[wr_addr_next] = vc_data_i[i];
	    wr_addr_next = wr_addr_next + 1;
	   end
	 end
      end
   end : CBUF_IN

   // Output from circular buffer.
   // Latch to output stage when current ECI packet is filled up and flow control allows.
   always_comb begin : CBUF_OUT
      rd_addr_next = rd_addr_reg;
      eci_pkt_next = eci_pkt_reg;
      eci_pkt_size_next = eci_pkt_size_reg;
      eci_pkt_valid_next = eci_pkt_valid_reg;

      // Latch data from circular buffer into output stage.
      if(~eci_pkt_valid_o | eci_pkt_ready_i) begin
	 for(integer i = 0; i < MAX_PACKET_SIZE; i = i + 1) begin
	    logic [(NREGS_ADDR_WIDTH - 1):0] tmp_idx;
	    tmp_idx = rd_addr_reg + i;
	    eci_pkt_next[i] = cbuf_reg[tmp_idx];
	 end
	 eci_pkt_size_next = eci_hdr_size;
	 eci_pkt_valid_next = cbuf_rd_valid;
	 if(cbuf_rd_valid) begin
	    // A valid ECI packet has been latched in output stage,
	    // point rd_addr_reg to next ECI header.
	    rd_addr_next = rd_addr_reg + eci_hdr_size;
	 end
      end
   end : CBUF_OUT

   // Decode header to get size of ECI packet.
   eci_get_num_words_in_pkt
     get_nw_in_pkt_1
       (
	.eci_command(cbuf_reg[rd_addr_reg]),
	.num_words_in_pkt(eci_hdr_size)
	);
   
   always_ff @(posedge clk) begin : REG_ASSIGN
      eci_pkt_reg	<= eci_pkt_next;
      eci_pkt_size_reg	<= eci_pkt_size_next;
      eci_pkt_valid_reg <= eci_pkt_valid_next;
      cbuf_reg		<= cbuf_next;
      wr_addr_reg	<= wr_addr_next;
      rd_addr_reg <= rd_addr_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 eci_pkt_reg		<= '0;
	 eci_pkt_size_reg	<= '0;
	 eci_pkt_valid_reg	<= '0;
	 cbuf_reg		<= '0;
	 wr_addr_reg		<= '0;
	 rd_addr_reg            <= '0;
      end
   end : REG_ASSIGN

endmodule //vc_eci_packetizer
`endif
