`ifndef C_ROUTE_CO_VC6_7_SV
`define C_ROUTE_CO_VC6_7_SV

import eci_cmd_defs::*;

/*
 * Module Description: 
 * * CPU issued command only requests from VC6/7 route to appropriate handlers 
 * * Module to route different types of request commands without data issued 
 *   by the CPU through VC6/VC7 to appropriate handlers based on the opcode of the command 
 * * VC6 and 7 will have the same set of handlers 
 * * Each request can be routed to only 1 handler 
 * 
 * Currently handlers are defined for the following commands
 * * Ginv - pop the request and ignore it
 * * Gsync - send the request to gsync handler
 * * Generic - for opcodes other than GINV, GSYNC 
 *
 * Input Ports
 * * VC6/7 stream with data words, size, valid, ready signals
 *   size indicates number of valid data words in its stream 
 * 
 * Output Ports
 * * gsync handler port
 *   Header, valid, ready 
 * * generic handler port 
 *   Header, valid, ready 
 * 
 * Since VC6/VC7 inputs from CPU are command only VC, the handler ports 
 * have only the request being forwarded to them 
 * 
 * Only one of the handlers will be active at a time, one request 
 * does not map to multiple handlers 
 *
 * Architecture Description:
 *  * The VC packet is connected to a simple_deserializer module to pop 1 request at a time
 *    simple_deserializer is enough as there is no payload in these VCs
 *  * The request is broadcasted to all handlers but the CONTROLLER ensures only one of the handlers
 *    have valid signal enabled.
 *  * See box below 
 *
 * Modifiable Parameters:
 *  WORD_WIDTH - the number of bits per word 
 *  VC_SIZE    - the number of words in input streams of VC6 and VC7 
 *
 * Non-modifiable Parameters:
 *  VC_SIZE_WIDTH - the number of bits required for "Size" input, automatically calculated 
 * 
 * Modules Used:
 * * simple_deserializer
 *
 * Notes:
 * * VC6/7 are command only VCs
 * * CPU requested commands are sent to the FPGA
 * * "c_" in module name means cpu issued 
 * * One request can have only 1 handler 
 *
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                          All outputs				      //
//                                      request word        are registered			      //
//                    +-----------+          1*64						      //
// VC6/7    7*64      |           +---+-----+/+-----------> handler 1 request word (broadcast)	      //
// stream ---+/+----->+           |   | |							      //
// size,   vc_pkt     | simple_   |   | |   +-------------> handler 1 valid (from controller)	      //
// valid,  size,      | deseriali |   | |   | +-----------+ handler 1 ready (to controller)	      //
//         valid      | zer       |   | |   | |							      //
//                    |           |   | |   | |                    ....				      //
//                    | pop 1     |   | |   | |  1*64						      //
// VC6/7    ready     | request   |   | +-------+/+-------> handler N request word (broadcast)	      //
// ready  <---+/+-----+           |   |     | |     +-----> handler N valid (from controller)	      //
//             1      +---+-------+   +     | |     | +---+ handler N ready (to controller)	      //
//                        ^         5 /     | |     | |						      //
//                        |   request +     | |     | |						      //
// Note: VC6/7 are CO     |   opcode  v     | v     | v	     NOTE: Only 1 handler		      //
// command only and       |          ++-----+-+-----+-----+	   has valid request		      //
// no data payload        |          |                    |	   at a time			      //
//                        +--+/+-----+   CONTROLLER       |					      //
//                            1      +--------------------+					      //
//                         ready	   Enable appropriate handler based on opcode		      //
////////////////////////////////////////////////////////////////////////////////////////////////////////

module c_route_co_vc6_7 #
  (
   // Mandatory parameter 
   parameter VC_NUM = 6,
   // Not mandatory 
   parameter VC_SIZE = 7,
   parameter VC_SIZE_WIDTH = $clog2(VC_SIZE) + ( (VC_SIZE % 2 ) == 0 )
   )
   (
    input logic 			      clk,
    input logic 			      reset,
    // Input can be Request without data CO VC6/7 from CPU
    input logic [VC_SIZE-1:0][ECI_WORD_WIDTH-1:0] vc_pkt_i,
    input logic [VC_SIZE-1:0] 	      vc_pkt_word_enable_i,
    input logic 			      vc_pkt_valid_i,
    output logic 			      vc_pkt_ready_o,
    // Gsync handler
    output logic [ECI_WORD_WIDTH-1:0] 	      gsync_hdr_o,
    output logic 			      gsync_valid_o,
    input logic 			      gsync_ready_i,
    // Generic handler
    // req without data
    output logic [ECI_WORD_WIDTH-1:0] 	      eci_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0]  eci_pkt_size_o,
    output logic 			      eci_pkt_valid_o,
    input logic 			      eci_pkt_ready_i,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0]  eci_pkt_vc_o
    );
   
   // Register the outputs
   logic [ECI_WORD_WIDTH-1:0] 		      gsync_hdr_reg	= '0, gsync_hdr_next;
   logic 				      gsync_valid_reg = '0, gsync_valid_next;
   logic [ECI_WORD_WIDTH-1:0] 		      eci_hdr_reg	= '0, eci_hdr_next;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      eci_pkt_size_reg	= '0;
   logic 				      eci_pkt_valid_reg = '0, eci_pkt_valid_next;
   logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 	      eci_pkt_vc_reg = '0;
   // Local signals from deserializer
   eci_word_t 		                      des_eci_hdr;
   logic 				      des_eci_valid;
   logic 				      des_eci_ready;
   // Local Controller signals
   logic 				      no_valid_ops;
   logic 				      any_op_hs;
   // CMD decoder signals
   logic 				      ginv_flag;
   logic 				      gsync_flag;
   logic 				      generic_flag;
   // Handshake signals
   logic 				      gsync_hs;
   logic 				      eci_hs;
   logic 				      des_hs;

   // Assign registers to outputs
   assign gsync_hdr_o = gsync_hdr_reg;
   assign gsync_valid_o = gsync_valid_reg;
   assign eci_hdr_o = eci_hdr_reg;
   assign eci_pkt_size_o = eci_pkt_size_reg;
   assign eci_pkt_valid_o = eci_pkt_valid_reg;
   assign eci_pkt_vc_o = eci_pkt_vc_reg;
   // Assign local signals
   assign no_valid_ops = ~gsync_valid_o & ~eci_pkt_valid_o;
   assign any_op_hs = gsync_hs | eci_hs;
   // Assign handshake signals
   assign gsync_hs = gsync_valid_o & gsync_ready_i;
   assign eci_hs = eci_pkt_valid_o & eci_pkt_ready_i;
   assign des_hs = des_eci_valid & des_eci_ready;

   // Pop 1 ECI header without data from VC stream
   // simple_deserializer module used as there are no payloads
    vc_word_extractor vc_simp_des1
    (
        .clk    (clk),
        .input_words     (vc_pkt_i),
        .input_valid     (vc_pkt_valid_i),
        .input_word_enable   (vc_pkt_word_enable_i),
        .input_ready     (vc_pkt_ready_o),
        .output_word     (des_eci_hdr.eci_word),
        .output_valid    (des_eci_valid),
        .output_ready    (des_eci_ready)
    );

   always_comb begin : CONTROLLER
      gsync_hdr_next = gsync_hdr_reg;
      gsync_valid_next = gsync_valid_reg;
      eci_hdr_next = eci_hdr_reg;
      eci_pkt_valid_next = eci_pkt_valid_reg;
      des_eci_ready = 1'b0;
      if(no_valid_ops) begin
	 // No handlers are enabled 
	 // ready to accept new data and route it to appropriate handler
	 des_eci_ready = 1'b1;
	 if(des_eci_valid) begin
	    // Handshake has happened this cycle,
	    // latch new data and route it to appropriate handler 
	    if(gsync_flag) begin
	       // Received GSYNC command, route to GSYNC handler
	       gsync_hdr_next = des_eci_hdr.eci_word;
	       // only gsync handler can be valid 
	       gsync_valid_next = 1'b1;
	       eci_pkt_valid_next = 1'b0;
	    end else if (ginv_flag) begin
	       // GINV command is ignored,
	       gsync_valid_next = 1'b0;
	       eci_pkt_valid_next = 1'b0;
	    end else begin
	       // Route to generic ECI handler
	       eci_hdr_next = des_eci_hdr.eci_word;
	       // only generic handler is valid 
	       eci_pkt_valid_next = 1'b1;
	       gsync_valid_next = 1'b0;
	    end
	 end
      end else if(any_op_hs) begin
	 // one of the output handlers is valid,
	 // Only one of the output handlers can have a handshake at a time
	 // no need to identify which one
	 gsync_valid_next = 1'b0;
	 eci_pkt_valid_next = 1'b0;
      end // else
      // if one of the handlers are valid, wait for handshake
      // else wait for deserailzer to get next valid data 
   end : CONTROLLER

   // Decode what command it is based on the opcode 
   always_comb begin : DECODE_ECI_CMD
      ginv_flag = 1'b0;
      gsync_flag = 1'b0;
      generic_flag = 1'b0;
      case(des_eci_hdr.generic_cmd.opcode)
	ECI_CMD_MREQ_GINV  : ginv_flag = 1'b1;
	ECI_CMD_MREQ_GSYNC : gsync_flag = 1'b1;
	default: generic_flag = 1'b1;
      endcase
   end : DECODE_ECI_CMD

   always_ff @(posedge clk) begin : REG_ASSIGN
      gsync_hdr_reg <= gsync_hdr_next;
      eci_hdr_reg <= eci_hdr_next;
      gsync_valid_reg <= gsync_valid_next;
      eci_pkt_size_reg <= 'd1;
      eci_pkt_valid_reg <= eci_pkt_valid_next;
      eci_pkt_vc_reg <= ECI_TOT_NUM_VCS_WIDTH'(VC_NUM);
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 //gsync_hdr_reg	<= '0;
	 gsync_valid_reg	<= '0;
	 //eci_hdr_reg		<= '0;
	 eci_pkt_size_reg	<= '0;
	 eci_pkt_valid_reg	<= '0;
	 eci_pkt_vc_reg         <= '0;
      end
   end : REG_ASSIGN
   
//ila_vc67_deser i_ila_vc67_deser
//(
//    .clk (clk),
//    .probe0 (vc_pkt_i),
//    .probe1 (vc_pkt_word_enable_i),
//    .probe2 (vc_pkt_valid_i),
//    .probe3 (vc_pkt_ready_o),
//    .probe4 (des_eci_hdr.eci_word),
//    .probe5 (des_eci_valid),
//    .probe6 (des_eci_ready),
//    .probe7 (gsync_flag),
//    .probe8 (ginv_flag)
//);

endmodule // c_route_co_vc6_7
`endif
