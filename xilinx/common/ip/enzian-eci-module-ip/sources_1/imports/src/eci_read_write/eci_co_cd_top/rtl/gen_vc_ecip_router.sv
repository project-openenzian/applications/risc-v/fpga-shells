/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2021-01-04
 * Project : Enzian
 *
 */

`ifndef GEN_VC_ECIP_ROUTER_SV
`define GEN_VC_ECIP_ROUTER_SV

/*
 * Module Description:
 *  Generic ECI packet router 
 *  * Deserializes the incoming VC packet into ECI packet  
 *    and sends it to the output handler 
 *  * Module supports req/resp with data VCs  
 *  * Supports only 1 output handler, that is why it is generic  
 *
 * Input Output Description:
 *  Input - Data from req/resp WITH data VC + vc number 
 *  Output - ECI packet WITH data + vc number 
 *
 * Architecture Description:
 *  Uses vc_eci_packetizer to get the next header + payload from the VC packet 
 *  and enables output valid ready signal for the handler 
 *
 * Modifiable Parameters:
 *  NONE
 *
 * Modules Used:
 *  vc_eci_packetizer
 *
 * Notes:
 * * for VCs with header+payload 
 *
 */

import eci_cmd_defs::*;

module gen_vc_ecip_router #
  (
   // Mandatory parameter
   parameter VC_NUM = 5,
   parameter VC_SIZE = 7,
   parameter VC_SIZE_WIDTH = $clog2(VC_SIZE) + ( (VC_SIZE % 2 ) == 0 )
   )
   (
    input logic 					   clk,
    input logic 					   reset,
    // Input req/resp without data from VVC
    input logic [VC_SIZE-1:0][ECI_WORD_WIDTH-1:0] 	   vc_pkt_i,
    input logic [VC_SIZE-1:0] 			   vc_pkt_word_enable_i,
    input logic 					   vc_pkt_valid_i,
    output logic 					   vc_pkt_ready_o,
    // Output ECI packet with data (header+payload data) 
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] eci_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0 ] 		   eci_pkt_size_o,
    output logic 					   eci_pkt_valid_o,
    input logic 					   eci_pkt_ready_i,
    output logic [ECI_TOT_NUM_VCS_WIDTH-1:0] 		   eci_pkt_vc_o
    );

   // Register to latch VC size
   logic [ECI_TOT_NUM_VCS_WIDTH-1:0] eci_pkt_vc_reg = '0;

   // Assign registers to outputs
   assign eci_pkt_vc_o = eci_pkt_vc_reg;

   // Deserialize the input VC packet into an ECI packet
   // Using vc_eci_packetizer because VC packet contains ECI header and payload 
   vc_eci_packetizer #
     (
      .VC_MAX_SIZE(VC_SIZE),
      .VC_MAX_SIZE_BITS(VC_SIZE_WIDTH),
      .MAX_PACKET_SIZE(ECI_PACKET_SIZE),
      .MAX_PACKET_SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH)
      )
   req_wd_pktizer_inst1
     (
      .clk(clk),
      .reset(reset),
      // Input from VCs
      .vc_data_i(vc_pkt_i),
      .vc_word_enable_i(vc_pkt_word_enable_i),
      .vc_valid_i(vc_pkt_valid_i),
      .vc_ready_o(vc_pkt_ready_o),
      // Output ECI packet
      .eci_pkt_o(eci_pkt_o),
      .eci_pkt_size_o(eci_pkt_size_o),
      .eci_pkt_valid_o(eci_pkt_valid_o),
      .eci_pkt_ready_i(eci_pkt_ready_i)
      );

   always_ff @(posedge clk) begin : REG_ASSIGN
      if( reset ) begin
	 eci_pkt_vc_reg <= '0;
      end else begin
	 eci_pkt_vc_reg <= ECI_TOT_NUM_VCS_WIDTH'(VC_NUM);
      end
   end : REG_ASSIGN

endmodule // gen_vc_ecip_router
`endif
