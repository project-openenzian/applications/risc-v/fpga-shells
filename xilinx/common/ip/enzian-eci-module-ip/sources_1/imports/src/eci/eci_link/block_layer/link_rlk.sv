
/*
    This is the Receiver channel of the ECI link. It consumes incoming blocks from the lower layer
    and decode the blocks, separate their contents into different VCs, and manage credits on the 
    different VCs.
*/

import block_types::*;
import eci_package::*;

module link_rlk (
	input   wire 										clk,    // Clock
	input   wire 										rst_n,  // Asynchronous reset active low

     //------------------ Input Block ----------------------//
    input   wire [BLOCK_WIDTH-1:0]                      blk_rx_data,
    input   wire                                        blk_rx_valid,
    input   wire                                        blk_crc_match,

    //---------------- VCs Data Words Received ------------//
    // VCs outputed to the MIB0 & MIB1 banks
    // VC 11 - 6
    output wire   [WORD_WIDTH-1:0]                      vc_co[NUM_CO_VCS-1:0][VC_CO_MAX_SIZE-1:0],              
    output wire                                         vc_co_valid[NUM_CO_VCS-1:0],
    output wire   [VC_CO_MAX_SIZE-1:0]                  vc_co_word_enable[NUM_CO_VCS-1:0],
    input  wire                                         vc_co_ready[NUM_CO_VCS-1:0],
    // VC 5 - 2
    output wire   [WORD_WIDTH-1:0]                      vc_cd[NUM_CD_VCS-1:0][VC_CD_MAX_SIZE-1:0],              
    output wire                                         vc_cd_valid[NUM_CD_VCS-1:0],
    output wire   [VC_CD_MAX_SIZE-1:0]                  vc_cd_word_enable[NUM_CD_VCS-1:0],
    input  wire                                         vc_cd_ready[NUM_CD_VCS-1:0],
    // VC 1 - 0
    output wire   [WORD_WIDTH-1:0]                      vc_io[NUM_IO_VCS-1:0][VC_IO_MAX_SIZE-1:0],              
    output wire                                         vc_io_valid[NUM_IO_VCS-1:0],
    output wire   [VC_IO_MAX_SIZE-1:0]                  vc_io_word_enable[NUM_IO_VCS-1:0],
    input  wire                                         vc_io_ready[NUM_IO_VCS-1:0],
    // MCD VC13
    output wire   [WORD_WIDTH-1:0]                      vc_mcd,              
    output wire                                         vc_mcd_valid,
    input  wire                                         vc_mcd_ready,
    // MIC VC12
    output wire   [WORD_WIDTH-1:0]                      vc_mic[VC_MXC_MAX_SIZE-1:0],              
    output wire                                         vc_mic_valid,
    output wire   [VC_MXC_MAX_SIZE-1:0]                 vc_mic_word_enable,
    input  wire                                         vc_mic_ready,

    //------------------ Synch Block Detected -------------//
    /* Output from RLK to TLK */
    output  SyncBlock_t                                 rx_sync_block,              // Sync Block from RLK
    output  wire                                        rx_sync_block_valid,
    output  wire                                        rx_blk_received,          // A valid data block received
    output  wire                                        rx_blk_error,                // CRC error detected

    //--------- Returned Credit from Thunderx -------------//
    // Returned credit from partner to FPGA: passed to TLK Arbiter
    output  wire  [7:0]                                 credits,    
    output  wire                                        hi_credits, 
    output  wire                                        credits_valid,             // a credits block received
    
    //-------- Credit to Return to Thunderx ---------------//
    // Return credit from FPGA to partner: each set bit represents 8 credits for the corresponding vc
    output  reg                                         return_cred[12:0],  

    //---------------- Debug Interface --------------------//
    // Debug Counters 
    output  wire  [127:0]                               debug_counters, 
    output  reg   [63:0]                                rcvd_cmds[2:0]
);

   

//////////////////////////////////////////////////////////////////////////
//////////////////////         Debug Signals         /////////////////////
//////////////////////////////////////////////////////////////////////////
reg     [35:0]                      sync_blks_detected; 
reg     [35:0]                      data_blks_detected;
reg     [7:0]                       undef_blks_detected; 
reg     [15:0]                      credit_ret_detected; 

reg     [7:0]                       ireq_rcvd; 
reg     [7:0]                       iack_rcvd; 
reg     [7:0]                       crc_match_blks; 
reg     [35:0]                      total_blks_recevied;  

reg     [3:0]                       word6_data_det;
reg     [3:0]                       word5_data_det;
reg     [3:0]                       word4_data_det;
reg     [3:0]                       word3_data_det;
reg     [3:0]                       word2_data_det;
reg     [3:0]                       word1_data_det;
reg     [3:0]                       word0_data_det;
reg     [3:0]                       data_cmd_detected;
reg     [3:0]                       rcvd_cnt;
//////////////////////////////////////////////////////////////////////////
//////////////////////      Signals Declaration      /////////////////////
//////////////////////////////////////////////////////////////////////////
//
DataBlock_t                         rx_data_block;
wire                                rx_data_block_valid;

wire    [63:0]                      vc_data_word_aligned[13:0][6:0];
wire                                vc_data_word_valid_aligned[13:0];
wire    [6 :0]                      vc_data_word_aligned_word_enable[13:0];


   
//////////////////////////////////////////////////////////////////////////
//////////////////////         Debug Counters         ////////////////////
//////////////////////////////////////////////////////////////////////////

assign  debug_counters = {total_blks_recevied[35:20],                  // 112-128
                          credit_ret_detected,                         // 
                          iack_rcvd, 
                          ireq_rcvd, 
                          crc_match_blks[7:0], 
                          undef_blks_detected, 
                          data_blks_detected[31:0], 
                          word0_data_det, word1_data_det, word2_data_det, word3_data_det, word4_data_det, word5_data_det, word6_data_det, data_cmd_detected};

always@(posedge clk) begin 
    if(~rst_n) begin
        sync_blks_detected  <= 36'b0;
        data_blks_detected  <= 36'b0;
        undef_blks_detected <= 8'b0;
        total_blks_recevied <= 36'b0;

        crc_match_blks <= 8'b0;

        credit_ret_detected  <= 16'b0;

        ireq_rcvd <= 0;
        iack_rcvd <= 0;

        rcvd_cnt  <= 0;

        word6_data_det <= 0;
        word5_data_det <= 0;
        word4_data_det <= 0;
        word3_data_det <= 0;
        word2_data_det <= 0;
        word1_data_det <= 0;
        word0_data_det <= 0;
        data_cmd_detected <= 0;

        rcvd_cmds[0] <= 0;
        rcvd_cmds[1] <= 0;
        rcvd_cmds[2] <= 0;
    end
    else if(blk_rx_valid) begin 
        sync_blks_detected  <= (sync_blks_detected  == 36'hFFFFFFFFF)? sync_blks_detected  : (blk_rx_data[63:61] == BTYPE_SYNC)? sync_blks_detected  + 1'b1 : sync_blks_detected;
        data_blks_detected  <= (data_blks_detected  == 36'hFFFFFFFFF)? data_blks_detected  : (blk_rx_data[63:62] == 2'b10)?      data_blks_detected  + 1'b1 : data_blks_detected;
        undef_blks_detected <= (undef_blks_detected == 8'hFF)?       undef_blks_detected : (!blk_rx_data[63])?                 undef_blks_detected + 1'b1 : undef_blks_detected;
        total_blks_recevied <= (total_blks_recevied == 36'hFFFFFFFFF)? total_blks_recevied : total_blks_recevied + 1'b1;

        crc_match_blks      <= (crc_match_blks == 8'hFF)? crc_match_blks : (~blk_crc_match)? crc_match_blks + 1'b1 : crc_match_blks;

        ireq_rcvd <= (ireq_rcvd == 8'hFF)? ireq_rcvd : ((blk_rx_data[63:61] == BTYPE_SYNC) && (blk_rx_data[53:52] == 2'b01))? ireq_rcvd + 1'b1 : ireq_rcvd;
        iack_rcvd <= (iack_rcvd == 8'hFF)? iack_rcvd : ((blk_rx_data[63:61] == BTYPE_SYNC) && (!blk_rx_data[53] && blk_rx_data[60]))? iack_rcvd + 1'b1 : iack_rcvd;

        credit_ret_detected  <= (credit_ret_detected  == 16'hFFFF)? credit_ret_detected  : ( (blk_rx_data[63:62] == 2'b10)?  ((|blk_rx_data[59:52])?     credit_ret_detected  + 1'b1 : credit_ret_detected):credit_ret_detected);

        if(blk_rx_data[63:62] == 2'b10) begin
            if(! (&blk_rx_data[51:24]) ) begin
                data_cmd_detected <= (data_cmd_detected  == 4'hF)? data_cmd_detected : data_cmd_detected + 1'b1;
            end
            if(! (&blk_rx_data[27:24])) begin
                word6_data_det <= (word6_data_det == 4'hF)? word6_data_det : word6_data_det + 1'b1;
            end

            if(! (&blk_rx_data[31:28])) begin
                word5_data_det <= (word5_data_det == 4'hF)? word5_data_det : word5_data_det + 1'b1;
            end

            if(! (&blk_rx_data[35:32])) begin
                word4_data_det <= (word4_data_det == 4'hF)? word4_data_det : word4_data_det + 1'b1;
            end

            if(! (&blk_rx_data[39:36])) begin
                word3_data_det <= (word3_data_det == 4'hF)? word3_data_det : word3_data_det + 1'b1;
            end

            if(! (&blk_rx_data[43:40])) begin
                word2_data_det <= (word2_data_det == 4'hF)? word2_data_det : word2_data_det + 1'b1;
            end

            if(! (&blk_rx_data[47:44])) begin
                word1_data_det <= (word1_data_det == 4'hF)? word1_data_det : word1_data_det + 1'b1;
            end

            if(! (&blk_rx_data[51:48])) begin
                word0_data_det <= (word0_data_det == 4'hF)? word0_data_det : word0_data_det + 1'b1;

                rcvd_cnt <= rcvd_cnt + 1'b1;

                if(rcvd_cnt == 3'b000) begin
                    rcvd_cmds[0] <= blk_rx_data[511:448];
                end
                else if(rcvd_cnt == 3'b011) begin
                    rcvd_cmds[1] <= blk_rx_data[511:448];
                end
                else if(rcvd_cnt == 3'b110) begin
                    rcvd_cmds[2] <= blk_rx_data[511:448];
                end
            end
        end
    end
end


//ila_0 ila_aligned_0_12_13
//(
//    .clk     (clk), 
//    .probe0  ({vc_data_word_aligned[0][6], vc_data_word_aligned[0][5], vc_data_word_aligned[0][4], vc_data_word_aligned[0][3], vc_data_word_aligned[0][2], vc_data_word_aligned[0][1], vc_data_word_aligned[0][0]}),    // 512
//    .probe1  (vc_data_word_aligned_size[0]),
//    .probe2  (vc_data_word_valid_aligned[0]),   // 1
//    .probe3  ({vc_data_word_aligned[12][6], vc_data_word_aligned[12][5], vc_data_word_aligned[12][4], vc_data_word_aligned[12][3], vc_data_word_aligned[12][2], vc_data_word_aligned[12][1], vc_data_word_aligned[12][0]}),   // 1
//    .probe4  (vc_data_word_aligned_size[12]),
//    .probe5  (vc_data_word_valid_aligned[12]),
//    .probe6  ({vc_data_word_aligned[13][6], vc_data_word_aligned[13][5], vc_data_word_aligned[13][4], vc_data_word_aligned[13][3], vc_data_word_aligned[13][2], vc_data_word_aligned[13][1], vc_data_word_aligned[13][0]}),
//    .probe7  (vc_data_word_aligned_size[13]),
//    .probe8  (vc_data_word_valid_aligned[13]),
//    .probe9  ({vc_data_word_aligned[2][6], vc_data_word_aligned[2][5], vc_data_word_aligned[2][4], vc_data_word_aligned[2][3], vc_data_word_aligned[2][2], vc_data_word_aligned[2][1], vc_data_word_aligned[2][0]}),
//    .probe10 (vc_data_word_aligned_size[2]),
//    .probe11 (vc_data_word_valid_aligned[2]),
//    .probe12 ({vc_data_word_aligned[3][6], vc_data_word_aligned[3][5], vc_data_word_aligned[3][4], vc_data_word_aligned[3][3], vc_data_word_aligned[3][2], vc_data_word_aligned[3][1], vc_data_word_aligned[3][0]}),
//    .probe13 (vc_data_word_aligned_size[3]),
//    .probe14 (vc_data_word_valid_aligned[3]),
//    .probe15 ({vc_data_word_aligned[6][6], vc_data_word_aligned[6][5], vc_data_word_aligned[6][4], vc_data_word_aligned[6][3], vc_data_word_aligned[6][2], vc_data_word_aligned[6][1], vc_data_word_aligned[6][0]}),
//    .probe16 (vc_data_word_aligned_size[6]),
//    .probe17 (vc_data_word_valid_aligned[6]),
//    .probe18 ({vc_data_word_aligned[7][6], vc_data_word_aligned[7][5], vc_data_word_aligned[7][4], vc_data_word_aligned[7][3], vc_data_word_aligned[7][2], vc_data_word_aligned[7][1], vc_data_word_aligned[7][0]}),
//    .probe19 (vc_data_word_aligned_size[7]),
//    .probe20 (vc_data_word_valid_aligned[7]), 
//    .probe21 ({vc_data_word_aligned[9][6], vc_data_word_aligned[9][5], vc_data_word_aligned[9][4], vc_data_word_aligned[9][3], vc_data_word_aligned[9][2], vc_data_word_aligned[9][1], vc_data_word_aligned[9][0]}),
//    .probe22 (vc_data_word_aligned_size[9]),
//    .probe23 (vc_data_word_valid_aligned[9])
//);

//////////////////////////////////////////////////////////////////////////
//////////////////////          Block Decoder         ////////////////////
//////////////////////////////////////////////////////////////////////////

block_decoder block_decoder(
    .clk                        (clk),   
    .rst_n                      (rst_n), 
    .blk_rx_data                (blk_rx_data),
    .blk_rx_valid               (blk_rx_valid),
    .blk_crc_match              (blk_crc_match),
    .rx_sync_block              (rx_sync_block),              
    .rx_sync_block_valid        (rx_sync_block_valid),
    .rx_blk_received            (rx_blk_received),         
    .rx_blk_error               (rx_blk_error),                
    .rx_data_block              (rx_data_block),
    .rx_data_block_valid        (rx_data_block_valid),
    .credits                    (credits),    
    .hi_credits                 (hi_credits), 
    .credits_valid              (credits_valid)        
);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////     VCs Decoder   /////////////////////////
//////////////////////////////////////////////////////////////////////////
//

vcs_decoder vcs_decoder(
    .clk                        (clk),   
    .rst_n                      (rst_n), 
    .rx_data_block              (rx_data_block),
    .rx_data_block_valid        (rx_data_block_valid),
    .vc_data_word_aligned       (vc_data_word_aligned),
    .vc_data_word_valid_aligned (vc_data_word_valid_aligned),
    .vc_data_word_aligned_word_enable  (vc_data_word_aligned_word_enable)
);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////       VCs FIFO        /////////////////////
//////////////////////////////////////////////////////////////////////////

rlk_fifos rlk_fifos(
    .clk                        (clk),   
    .rst_n                      (rst_n), 

    .vc_data_word_aligned       (vc_data_word_aligned),
    .vc_data_word_valid_aligned (vc_data_word_valid_aligned),
    .vc_data_word_aligned_word_enable  (vc_data_word_aligned_word_enable),

    .vc_co                      (vc_co),
    .vc_co_valid                (vc_co_valid),
    .vc_co_word_enable                 (vc_co_word_enable), 
    .vc_co_ready                (vc_co_ready),

    .vc_cd                      (vc_cd),
    .vc_cd_valid                (vc_cd_valid),
    .vc_cd_word_enable                 (vc_cd_word_enable), 
    .vc_cd_ready                (vc_cd_ready),

    .vc_io                      (vc_io),
    .vc_io_valid                (vc_io_valid),
    .vc_io_word_enable                 (vc_io_word_enable), 
    .vc_io_ready                (vc_io_ready),

    .vc_mcd                     (vc_mcd),
    .vc_mcd_valid               (vc_mcd_valid),
    .vc_mcd_ready               (vc_mcd_ready),

    .vc_mic                     (vc_mic),
    .vc_mic_valid               (vc_mic_valid),
    .vc_mic_word_enable                (vc_mic_word_enable), 
    .vc_mic_ready               (vc_mic_ready),

    .return_cred                (return_cred)
);



endmodule
