----------------------------------------------------------------------------------
-- Company: ETH Zürich
-- Engineer: Adam Turowski
--
-- Create Date: 06.09.2021 18:43:41
-- Design Name: Enzian
-- Module Name: eci_io_bridge - Behavioral
-- Description: VC0, VC1 and VC13 handler, register support, AXI-Lite bridge
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_io_bridge_lite is
generic (
    SECOND_LINK_ACTIVE : integer := 1
);
port (
    clk : in std_logic;
    reset : in std_logic;

    -- Link 1 interface
    link1_mib_vc_mcd        : in std_logic_vector(63 downto 0);
    link1_mib_vc_mcd_valid  : in std_logic;
    link1_mib_vc_mcd_ready  : buffer std_logic;

    link1_mib_vc0_io         : in WORDS(6 downto 0);
    link1_mib_vc0_io_valid   : in std_logic;
    link1_mib_vc0_io_word_enable    : in std_logic_vector(6 downto 0);
    link1_mib_vc0_io_ready   : out std_logic;

    link1_mib_vc1_io         : in WORDS(6 downto 0);
    link1_mib_vc1_io_valid   : in std_logic;
    link1_mib_vc1_io_word_enable    : in std_logic_vector(6 downto 0);
    link1_mib_vc1_io_ready   : out std_logic;

    link1_mob_lo_vc           : buffer std_logic_vector(63 downto 0);
    link1_mob_lo_vc_no        : buffer std_logic_vector(3 downto 0);
    link1_mob_lo_vc_valid     : buffer STD_LOGIC;
    link1_mob_lo_vc_ready     : in STD_LOGIC;

    -- Link 2 interface
    link2_mib_vc_mcd        : in std_logic_vector(63 downto 0);
    link2_mib_vc_mcd_valid  : in std_logic;
    link2_mib_vc_mcd_ready  : buffer  std_logic;

    link2_mib_vc0_io         : in WORDS(6 downto 0);
    link2_mib_vc0_io_valid   : in std_logic;
    link2_mib_vc0_io_word_enable    : in std_logic_vector(6 downto 0);
    link2_mib_vc0_io_ready   : out std_logic;

    link2_mib_vc1_io         : in WORDS(6 downto 0);
    link2_mib_vc1_io_valid   : in std_logic;
    link2_mib_vc1_io_word_enable    : in std_logic_vector(6 downto 0);
    link2_mib_vc1_io_ready   : out std_logic;

    link2_mob_lo_vc           : buffer std_logic_vector(63 downto 0);
    link2_mob_lo_vc_no        : buffer std_logic_vector(3 downto 0);
    link2_mob_lo_vc_valid     : buffer STD_LOGIC;
    link2_mob_lo_vc_ready     : in STD_LOGIC;

    -- AXI Lite master interface IO addr space
    m_io_axil_awaddr  : out std_logic_vector(39 downto 0);
    m_io_axil_awvalid : buffer std_logic;
    m_io_axil_awready : in  std_logic;

    m_io_axil_wdata   : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb   : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid  : buffer std_logic;
    m_io_axil_wready  : in  std_logic;

    m_io_axil_bresp   : in  std_logic_vector(1 downto 0);
    m_io_axil_bvalid  : in  std_logic;
    m_io_axil_bready  : buffer std_logic;

    m_io_axil_araddr  : out std_logic_vector(39 downto 0);
    m_io_axil_arvalid : buffer std_logic;
    m_io_axil_arready : in  std_logic;

    m_io_axil_rdata   : in std_logic_vector(63 downto 0);
    m_io_axil_rresp   : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid  : in std_logic;
    m_io_axil_rready  : buffer std_logic
);
end eci_io_bridge_lite;


architecture Behavioral of eci_io_bridge_lite is

-- Word serializer, read a word from the VC one by one
component vc_word_extractor_buffered is
port (
    clk             : in std_logic;
    input_words     : in WORDS(6 downto 0);
    input_valid     : in std_logic;
    input_word_enable   : in std_logic_vector(6 downto 0);
    input_ready     : out std_logic;
    output_word     : out std_logic_vector(63 downto 0);
    output_valid    : out std_logic;
    output_ready    : in std_logic
);
end component;

-- Registers
signal ocx_rlk_lnk_data     : WORDS(1 downto 0);

-- Link 1 request word before buffer
signal link1_mib_vc0        : std_logic_vector(63 downto 0);
signal link1_mib_vc0_valid  : std_logic;
signal link1_mib_vc0_ready  : std_logic;

-- Link 2 request word before buffer
signal link2_mib_vc0        : std_logic_vector(63 downto 0);
signal link2_mib_vc0_valid  : std_logic;
signal link2_mib_vc0_ready  : std_logic;

signal link1_mib_vc0_io_ready_int : std_logic;
signal link2_mib_vc0_io_ready_int : std_logic;

-- Unified request buffer
signal mib_vc0          : std_logic_vector(63 downto 0);
signal mib_vc0_valid    : std_logic;

signal busy : std_logic; -- Processing a request, not ready to accept a new one

signal request : WORDS(0 to 1);
signal request_ready : std_logic_vector(0 to 1);

signal second_cycle : std_logic; -- Process the second word of a request

-- Choose and use one link to process the whole request
signal active_link      : std_logic;
signal last_active_link : std_logic;
signal hold_link        : std_logic;

signal second_write     : std_logic := '0';
signal out_buffer       : std_logic_vector(63 downto 0);
signal out_buffer_vc_no : std_logic_vector(3 downto 0);

-- convert IOB/SL word size/select
function eci_io_word_to_bitmap(codeword : std_logic_vector(3 downto 0))
    return std_logic_vector is
    variable bitmap : std_logic_vector(7 downto 0);
begin
    case codeword is
        when "0000" => bitmap := "00000001";
        when "0001" => bitmap := "00000010";
        when "0010" => bitmap := "00000100";
        when "0011" => bitmap := "00001000";
        when "0100" => bitmap := "00010000";
        when "0101" => bitmap := "00100000";
        when "0110" => bitmap := "01000000";
        when "0111" => bitmap := "10000000";
        when "1000" => bitmap := "00000011";
        when "1001" => bitmap := "00001100";
        when "1010" => bitmap := "00110000";
        when "1011" => bitmap := "11000000";
        when "1100" => bitmap := "00001111";
        when "1101" => bitmap := "11110000";
        when "1110" => bitmap := "11111111";
        when "1111" => bitmap := "11111111";
    end case;
    return bitmap;
end eci_io_word_to_bitmap;

begin

link1_mib_vc0_io_ready <= link1_mib_vc0_io_ready_int;
link1_mib_vc1_io_ready <= '0';
link1_mib_vc_mcd_ready <= '1'; -- VC13 always read

link2_mib_vc0_io_ready <= link2_mib_vc0_io_ready_int;
link2_mib_vc1_io_ready <= '0';
link2_mib_vc_mcd_ready <= '1'; -- VC13 always read

link1_vc0_word_extractor : vc_word_extractor_buffered
port map (
    clk             => clk,
    input_words     => link1_mib_vc0_io,
    input_valid     => link1_mib_vc0_io_valid,
    input_word_enable   => link1_mib_vc0_io_word_enable,
    input_ready     => link1_mib_vc0_io_ready_int,
    output_word     => link1_mib_vc0,
    output_valid    => link1_mib_vc0_valid,
    output_ready    => link1_mib_vc0_ready
);

link2_vc0_word_extractor : vc_word_extractor_buffered
port map (
    clk             => clk,
    input_words     => link2_mib_vc0_io,
    input_valid     => link2_mib_vc0_io_valid,
    input_word_enable   => link2_mib_vc0_io_word_enable,
    input_ready     => link2_mib_vc0_io_ready_int,
    output_word     => link2_mib_vc0,
    output_valid    => link2_mib_vc0_valid,
    output_ready    => link2_mib_vc0_ready
);

active_link <= last_active_link when hold_link = '1' else link2_mib_vc0_valid and not link1_mib_vc0_valid;
mib_vc0 <= link1_mib_vc0 when active_link = '0' else link2_mib_vc0;
mib_vc0_valid <= link1_mib_vc0_valid when active_link = '0' else link2_mib_vc0_valid;

busy <= request_ready(0) or link1_mob_lo_vc_valid or link2_mob_lo_vc_valid
    or m_io_axil_arvalid or m_io_axil_rready or m_io_axil_awvalid or m_io_axil_wvalid
    or m_io_axil_bready;

hold_link <= busy or second_cycle; -- keep reading from the same link

process_vcs : process(clk)
    variable address : std_logic_vector(43 downto 0);
begin
    if rising_edge(clk) then
        if reset = '1' then
            link1_mob_lo_vc_valid <= '0';
            link2_mob_lo_vc_valid <= '0';

            link1_mib_vc0_ready <= '0';
            link2_mib_vc0_ready <= '0';

            request_ready <= "00";

            second_cycle <= '0';

            m_io_axil_awvalid <= '0';
            m_io_axil_wvalid  <= '0';
            m_io_axil_bready  <= '0';
            m_io_axil_arvalid <= '0';
            m_io_axil_rready  <= '0';

            second_write <= '0';
        else
            last_active_link <= active_link;

            if link1_mib_vc_mcd_valid = '1' then -- incoming link 1 discovery message
                ocx_rlk_lnk_data(0) <= "10000000" & link1_mib_vc_mcd(58 downto 3);
            end if;
            if link2_mib_vc_mcd_valid = '1' then -- incoming link 2 discovery message
                ocx_rlk_lnk_data(1) <= "10000000" & link2_mib_vc_mcd(58 downto 3);
            end if;

            if busy = '0' and mib_vc0_valid = '1' and link1_mib_vc0_ready = '0' and link2_mib_vc0_ready = '0' then -- there is a request
                if active_link = '0' then
                    link1_mib_vc0_ready <= '1';
                else
                    link2_mib_vc0_ready <= '1';
                end if;
                if second_cycle = '0' then -- first word
                    if mib_vc0(63 downto 59) = ECI_IREQ_IOBLD or
                        mib_vc0(63 downto 59) = ECI_IREQ_SLILD then -- IO read
                        request(0) <= mib_vc0;
                        request_ready <= "10";
                    elsif mib_vc0(63 downto 59) = ECI_IREQ_IOBST or
                        mib_vc0(63 downto 59) = ECI_IREQ_IOBSTA or
                        mib_vc0(63 downto 59) = ECI_IREQ_SLIST then -- IO write
                        request(0) <= mib_vc0;
                        second_cycle <= '1';
                    end if;
                else
                    request(1) <= mib_vc0;
                    request_ready <= "11";
                    second_cycle <= '0';
                end if;
            end if;

            if link1_mib_vc0_ready = '1' or link2_mib_vc0_ready = '1' then
                link1_mib_vc0_ready <= '0';
                link2_mib_vc0_ready <= '0';
            end if;

            if request_ready = "10" then -- load
                if request(0)(63 downto 59) = ECI_IREQ_IOBLD then -- prepare a response
                    link1_mob_lo_vc <= ECI_IRSP_IOBRSP & "0" & request(0)(57 downto 49) & "0" & X"000000000000";
                else
                    link1_mob_lo_vc <= ECI_IRSP_SLIRSP & "000" & X"00000000000000";
                end if;
                link1_mob_lo_vc_no <= X"1";
                second_write <= '1';

                address := request(0)(44 downto 4) & "000";
                if unsigned(address) < X"1000000000" then -- send READ to the AXI
                    m_io_axil_araddr <= address(39 downto 0);
                    m_io_axil_arvalid <= '1';
                    m_io_axil_rready <= '1';
                else -- internal registers
                    case address is
                        when X"7E011000000" =>
                            out_buffer <= X"0000000000000005";
                        when X"7E011000020" =>
                            out_buffer <= X"000000000000000c";
                        when X"7E011000028" =>
                            out_buffer <= X"0000000000000026";
                        when X"7E011000030" =>
                            if SECOND_LINK_ACTIVE = 1 then
                                out_buffer <= X"000000000000000c";
                            else
                                out_buffer <= X"0000000000000026";
                            end if;
                        when X"7E011018028" =>
                            out_buffer <= ocx_rlk_lnk_data(0);
                        when X"7E01101c028" =>
                            out_buffer <= ocx_rlk_lnk_data(1);
                        when others =>
                            out_buffer <= X"0000DEADBEEF0000";
                    end case;
                    link1_mob_lo_vc_valid <= '1';
                    out_buffer_vc_no <= X"1";
                end if;
                request_ready <= "00";
            elsif request_ready = "11" then -- store
                if request(0)(63 downto 59) = ECI_IREQ_IOBSTA then
                    link1_mob_lo_vc <= ECI_IRSP_IOBACK & "0" & request(0)(57 downto 49) & "0" & X"000000000000"; -- prepare ACK
                    link1_mob_lo_vc_no <= X"1";
                end if;
                address := request(0)(44 downto 4) & "000";
                if unsigned(address) < X"1000000000" then -- send WRITE to the AXI
                    m_io_axil_awaddr <= address(39 downto 0);
                    m_io_axil_awvalid <= '1';
                    m_io_axil_wdata <= request(1);
                    m_io_axil_wvalid <= '1';
                    m_io_axil_wstrb <= eci_io_word_to_bitmap(request(0)(3 downto 0));
                    m_io_axil_bready <= '1';
                else -- internal registers
                    case address is
                        when X"7E011010028" =>
                            link1_mob_lo_vc <= ECI_MDLD_LNKD & request(1)(55 downto 0) & "000";
                            link1_mob_lo_vc_valid <= '1';
                            link1_mob_lo_vc_no <= X"d";
                        when X"7E011014028" =>
                            link2_mob_lo_vc <= ECI_MDLD_LNKD & request(1)(55 downto 0) & "000";
                            link2_mob_lo_vc_valid <= '1';
                            link2_mob_lo_vc_no <= X"d";
                        when others =>
                    end case;
                    if request(0)(63 downto 59) = ECI_IREQ_IOBSTA then
                        link1_mob_lo_vc_valid <= '1'; -- send ACK
                    end if;
                end if;
                request_ready <= "00";
            end if;

            -- processing a request
            if link1_mob_lo_vc_valid = '1' and link1_mob_lo_vc_ready = '1' then -- link 1 message sent
                if second_write = '1' then
                    link1_mob_lo_vc <= out_buffer;
                    link1_mob_lo_vc_no <= out_buffer_vc_no;
                    second_write <= '0';
                else
                    link1_mob_lo_vc_valid <= '0';
                end if;
            end if;
            if link2_mob_lo_vc_valid = '1' and link2_mob_lo_vc_ready = '1' then -- link 2 response sent
                link2_mob_lo_vc_valid <= '0';
            end if;
            if m_io_axil_arvalid = '1' and m_io_axil_arready = '1' then -- AXI read address sent
                m_io_axil_arvalid <= '0';
            end if;
            if m_io_axil_rvalid = '1' and m_io_axil_rready = '1' then -- AXI read data received
                m_io_axil_rready <= '0';
                out_buffer <= m_io_axil_rdata;
                link1_mob_lo_vc_valid <= '1';
            end if;
            if m_io_axil_awvalid = '1' and m_io_axil_awready = '1' then -- AXI write address sent
                m_io_axil_awvalid <= '0';
            end if;
            if m_io_axil_wvalid = '1' and m_io_axil_wready = '1' then -- AXI write data sent
                m_io_axil_wvalid <= '0';
                m_io_axil_wstrb <= "00000000";
            end if;
            if m_io_axil_bvalid = '1' and m_io_axil_bready = '1' then -- AXI write ack received
                if request(0)(63 downto 59) = ECI_IREQ_IOBSTA then -- send ACK
                    link1_mob_lo_vc_valid <= '1';
                    link1_mob_lo_vc_no <= X"1";
                end if;
                m_io_axil_bready <= '0';
            end if;
        end if;
    end if;
end process;

end Behavioral;
