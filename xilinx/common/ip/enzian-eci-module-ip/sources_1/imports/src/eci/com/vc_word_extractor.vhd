----------------------------------------------------------------------------------
-- Company: ETH Zürich
-- Engineer: Adam Turowski
-- 
-- Create Date: 06.09.2021 19:11:32
-- Design Name: Enzian
-- Module Name: vc_word_extractor - Behavioral
-- Description: exctract word from a VC input data, one by one
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.eci_defs.all;

entity vc_word_extractor is
Port (
    clk             : in std_logic;
    input_words     : in WORDS(6 downto 0);
    input_valid     : in std_logic;
    input_word_enable   : in std_logic_vector(6 downto 0);
    input_ready     : out std_logic;
    output_word     : out std_logic_vector(63 downto 0);
    output_valid    : out std_logic;
    output_ready    : in std_logic
);
end vc_word_extractor;

architecture Behavioral of vc_word_extractor is

type POSITIONS is array (integer range <>) of integer range 0 to 6;

signal current_word : integer range 0 to 6 := 0;
signal next_word : POSITIONS(0 to 6);

begin

next_word(6) <= 5 when input_word_enable(1) = '1' else
                4 when input_word_enable(2) = '1' else
                3 when input_word_enable(3) = '1' else
                2 when input_word_enable(4) = '1' else
                1 when input_word_enable(5) = '1' else
                0;
next_word(5) <= 4 when input_word_enable(2) = '1' else
                3 when input_word_enable(3) = '1' else
                2 when input_word_enable(4) = '1' else
                1 when input_word_enable(5) = '1' else
                0;
next_word(4) <= 3 when input_word_enable(3) = '1' else
                2 when input_word_enable(4) = '1' else
                1 when input_word_enable(5) = '1' else
                0;
next_word(3) <= 2 when input_word_enable(4) = '1' else
                1 when input_word_enable(5) = '1' else
                0;
next_word(2) <= 1 when input_word_enable(5) = '1' else
                0;
next_word(1) <= 0;
next_word(0) <= 6 when input_word_enable(0) = '1' else
                5 when input_word_enable(1) = '1' else
                4 when input_word_enable(2) = '1' else
                3 when input_word_enable(3) = '1' else
                2 when input_word_enable(4) = '1' else
                1 when input_word_enable(5) = '1' else
                0;

output_word <= input_words(6-next_word(current_word));
output_valid <= '1' when input_valid = '1' and (next_word(current_word) /= 0 or input_word_enable(6) = '1') else '0';
input_ready <= '1' when output_ready = '1' and ((next_word(next_word(current_word)) = 0 and input_word_enable(6) = '0') or (next_word(current_word) = 0 and input_word_enable(6) = '1')) else '0';

choose_next_word : process(clk)
begin
    if rising_edge(clk) then
        if input_valid = '0' then
            current_word <= 0;
        elsif input_valid = '1' and output_ready = '1' then
            if (next_word(next_word(current_word)) /= 0 or input_word_enable(6) = '1') then
                current_word <= next_word(current_word);
            else
                current_word <= 0;
            end if;
        end if;
    end if;
end process;

end Behavioral;
