

/*
    The VCs decoder, separates the data words in an incoming block into separate VCs and align the words of 
    the single VC beside each other even when the placed separated inside the data block. 
    For each VC it outputs an array of valid words up to 7 words, and a size field that tells how many valid
    words in the array.
*/

import block_types::*;
import eci_package::*;

module vcs_decoder (
	input   wire 										clk,    // Clock
	input   wire 										rst_n,  // Asynchronous reset active low

	//------------------ Data Block Detected --------------//
    input   DataBlock_t                         		rx_data_block,
	input   wire                                		rx_data_block_valid,

	output  wire    [63:0]                      		vc_data_word_aligned[13:0][6:0],
    output  wire                                		vc_data_word_valid_aligned[13:0],
    output  wire    [6 :0]                      		vc_data_word_aligned_word_enable[13:0]
);


wire    [63:0]                      vc_data_word[13:0][6:0];
wire    [6 :0]                      vc_data_word_valid[13:0];

genvar i, j;

generate for (i = 0; i < 14; i=i+1) begin: aligners
    // Level one: Broadcast all 7 words to all VCs, but set valid/invalid for each word on each channel
    for (j = 0; j < 7; j=j+1) begin 
        assign vc_data_word[i][j]       = rx_data_block.Data[j];
        assign vc_data_word_valid[i][j] = (rx_data_block.Vcs[j] == i) && rx_data_block_valid;
    end
    assign vc_data_word_aligned[i] = vc_data_word[i];
    assign vc_data_word_valid_aligned[i] = vc_data_word_valid[i] != 7'h0;
    assign vc_data_word_aligned_word_enable[i] = vc_data_word_valid[i];
    
    // Level two: Aligners
//    word_aligner word_alignerX
//    (
//        .clk                     (clk),                      
//        .rst_n                   (rst_n),      
//        // 
//        .data_word_in            (vc_data_word[i]),  
//        .data_word_in_valid      (vc_data_word_valid[i]),  

//        .data_word_out           (vc_data_word_aligned[i]),  
//        .data_word_out_valid     (vc_data_word_valid_aligned[i]), 
//        .data_word_out_size      (vc_data_word_aligned_size[i])
//    );
end
endgenerate

/*//////////////////////////////////////////////////////////////////////////
//////////////////////         ILA Debug Block        ////////////////////
//////////////////////////////////////////////////////////////////////////

ila_blk_dbg ila_vc0_prealigned
(
    .clk     (clk), 
    .probe0  ({vc_data_word[0][6], vc_data_word[0][5], vc_data_word[0][4], vc_data_word[0][3], vc_data_word[0][2], vc_data_word[0][1], vc_data_word[0][0]}),    // 512
    .probe1  ({vc_data_word_valid[0][6], vc_data_word_valid[0][5], vc_data_word_valid[0][4], vc_data_word_valid[0][3], vc_data_word_valid[0][2], vc_data_word_valid[0][1], vc_data_word_valid[0][0]})
);*/

//ila_blk_dbg ila_vc0_prealigned
//(
//    .clk     (clk), 
//    .probe0  (rx_data_block),    // 512
//    .probe1  (vc_data_word_valid[0]),
//    .probe2  (vc_data_word_valid[1]),
//    .probe3  (vc_data_word_valid[13])
//);

endmodule