----------------------------------------------------------------------------------
-- Company: ETH Zurich
-- Engineer: Adam S. Turowski
-- 
-- Create Date: 10.03.2022 13:32:47
-- Design Name: 
-- Module Name: eci_packet_rx_mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Multiplex incoming data from two links into a single output, round-robin
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

entity eci_packet_rx_mux is
generic (
    WIDTH           : integer
);
port (
    clk             : in std_logic;
    link1_data      : in std_logic_vector(WIDTH-1 downto 0);
    link1_valid     : in std_logic;
    link1_ready     : out std_logic;
    link2_data      : in std_logic_vector(WIDTH-1 downto 0);
    link2_valid     : in std_logic;
    link2_ready     : out std_logic;
    output_data     : out std_logic_vector(WIDTH-1 downto 0);
    output_valid    : buffer std_logic;
    output_ready    : in std_logic
);
end eci_packet_rx_mux;

architecture Behavioral of eci_packet_rx_mux is

signal hold_link    : std_logic := '0';
signal last_active  : std_logic;
signal link_active  : std_logic;

begin

link_active     <= '1' when (hold_link = '0' and ((link1_valid = '0' and link2_valid = '1') or (link1_valid = '1' and link2_valid = '1' and last_active = '0'))) or (hold_link = '1' and last_active = '1') else '0';
output_data     <= link1_data when link_active = '0' else link2_data;
output_valid    <= link1_valid when link_active = '0' else link2_valid;
link1_ready     <= output_ready when link_active = '0' else '0';
link2_ready     <= output_ready when link_active = '1' else '0';

i_process : process(clk) is
begin
    if rising_edge(clk) then
        if output_ready = '1' then
            hold_link <= '0';
            last_active <= link_active;
        elsif output_valid = '1' then -- but output_ready = '0'
            hold_link <= '1';
            last_active <= link_active;
        end if;
    end if;
end process;

end Behavioral;
