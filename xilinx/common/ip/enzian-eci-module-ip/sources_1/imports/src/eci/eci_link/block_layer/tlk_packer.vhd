----------------------------------------------------------------------------------
-- tlk_packer
-- Adam Turowski
-- ETH Zürich 2021
--
-- Packs incoming data from Hi bandwidth VCs and Lo bandwidth VCs
-- Hi bandwidth data comes in 2 cycles: first 9 words, then 8 words
-- Lo bandwidth data always comes one word per cycle
-- There are 4 packing patterns:
-- H, G - hi vc word slots
-- L - lo vc word slots
-- x - unused slots
--
-- Pattern 1, hi vc data is available with/without lo vc data
-- 1. HHHHHHH
-- 2. HHHHHHH
-- 3. LHHHxxx -- if no more hi vc data or
-- Pattern 2, if there is more hi vc data
-- 3. LHHHGGG
-- 4. GGGGGGG
-- 5. GGGGGGG
-- Pattern 3, if there are lo vc words as well as hi vc words
-- 6. LHHHHHH
-- 7. LHHHHHH
-- 8. LxHHHHH
-- or pattern 4, if only there is a lo vc word
-- 1. Lxxxxxx
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity tlk_packer is
    port (
        clk       : in std_logic;
        hi_words  : in WORDS (8 downto 0);
        hi_vc     : in std_logic_vector(3 downto 0);
        hi_size   : in std_logic_vector(1 downto 0); -- 10 - 8 words, 11 - 9 words
        hi_valid  : in std_logic;
        hi_ready  : buffer std_logic;
        lo_word   : in std_logic_vector(63 downto 0);
        lo_vc     : in std_logic_vector(3 downto 0);
        lo_valid  : in std_logic;
        lo_ready  : out std_logic;
        out_words : out WORDS (6 downto 0);
        out_vc    : out VCS (6 downto 0);
        out_valid : out std_logic;
        out_ready : in std_logic
    );
end tlk_packer;

architecture Behavioral of tlk_packer is
    signal buffer1    : WORDS(6 downto 0);
    signal buffer1_vc : VCS(6 downto 0);

    signal buffer_size : integer range 0 to 7 := 0;
    signal phase       : integer              := 0;

begin
    -- vc numbers
    out_vc(6) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    buffer1_vc(0) when hi_valid = '1' and phase = 0 and buffer_size = 2 else
    lo_vc when lo_valid = '1' and ((phase = 0 and buffer_size = 3) or (phase = 1 and buffer_size = 0) or (hi_valid = '1' and phase = 1 and buffer_size = 3)
    or (phase = 1 and buffer_size = 5) or (hi_valid = '0' and phase = 0 and buffer_size = 0)) else
    buffer1_vc(0) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(0) when phase = 0 and buffer_size = 7 else
    X"f";

    out_vc(5) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    buffer1_vc(1) when hi_valid = '1' and phase = 0 and buffer_size = 2 else
    buffer1_vc(1) when phase = 0 and buffer_size = 3 else
    buffer1_vc(1) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(1) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1_vc(1) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    X"f";

    out_vc(4) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    buffer1_vc(2) when phase = 0 and buffer_size = 3 else
    buffer1_vc(2) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(2) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1_vc(2) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    buffer1_vc(2) when phase = 1 and buffer_size = 5 else
    X"f";

    out_vc(3) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    buffer1_vc(3) when phase = 0 and buffer_size = 3 else
    buffer1_vc(3) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(3) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1_vc(3) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    buffer1_vc(3) when phase = 1 and buffer_size = 5 else
    X"f";

    out_vc(2) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    buffer1_vc(4) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(4) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1_vc(4) when phase = 1 and buffer_size = 5 else
    X"f";

    out_vc(1) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    buffer1_vc(5) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1_vc(5) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1_vc(5) when phase = 1 and buffer_size = 5 else
    X"f";

    out_vc(0) <= hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 6 else
    buffer1_vc(6) when phase = 0 and buffer_size = 7 else
    hi_vc when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_vc when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1_vc(6) when phase = 1 and buffer_size = 5 else
    X"f";

    -- words
    out_words(6) <= hi_words(0) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    buffer1(0) when hi_valid = '1' and phase = 0 and buffer_size = 2 else
    lo_word when lo_valid = '1' and ((phase = 0 and buffer_size = 3) or (phase = 1 and buffer_size = 0) or (hi_valid = '1' and phase = 1 and buffer_size = 3)
    or (phase = 1 and buffer_size = 5) or (hi_valid = '0' and phase = 0 and buffer_size = 0)) else
    buffer1(0) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(0) when phase = 0 and buffer_size = 7 else
    (others => 'X');

    out_words(5) <= hi_words(1) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    buffer1(1) when hi_valid = '1' and phase = 0 and buffer_size = 2 else
    buffer1(1) when phase = 0 and buffer_size = 3 else
    buffer1(1) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(1) when phase = 0 and buffer_size = 7 else
    hi_words(0) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1(1) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    (others => 'X');

    out_words(4) <= hi_words(2) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_words(1) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    buffer1(2) when phase = 0 and buffer_size = 3 else
    buffer1(2) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(2) when phase = 0 and buffer_size = 7 else
    hi_words(1) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1(2) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    buffer1(2) when phase = 1 and buffer_size = 5 else
    (others => 'X');

    out_words(3) <= hi_words(3) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_words(2) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    buffer1(3) when phase = 0 and buffer_size = 3 else
    buffer1(3) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(3) when phase = 0 and buffer_size = 7 else
    hi_words(2) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    buffer1(3) when hi_valid = '1' and phase = 1 and buffer_size = 3 else
    buffer1(3) when phase = 1 and buffer_size = 5 else
    (others => 'X');

    out_words(2) <= hi_words(4) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_words(3) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_words(0) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    buffer1(4) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(4) when phase = 0 and buffer_size = 7 else
    hi_words(3) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_words(1) when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1(4) when phase = 1 and buffer_size = 5 else
    (others => 'X');

    out_words(1) <= hi_words(5) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_words(4) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_words(1) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    buffer1(5) when hi_valid = '1' and phase = 0 and buffer_size = 6 else
    buffer1(5) when phase = 0 and buffer_size = 7 else
    hi_words(4) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_words(2) when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1(5) when phase = 1 and buffer_size = 5 else
    (others => 'X');

    out_words(0) <= hi_words(6) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 else
    hi_words(5) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 else
    hi_words(2) when hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 else
    hi_words(1) when hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 6 else
    buffer1(6) when phase = 0 and buffer_size = 7 else
    hi_words(5) when hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 else
    hi_words(3) when hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 else
    buffer1(6) when phase = 1 and buffer_size = 5 else
    (others => 'X');

    hi_ready <= '1' when (out_ready = '1' and hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0)
        or (out_ready = '1' and hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2)
        or (out_ready = '1' and hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3)
        or (out_ready = '1' and hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 6)
        or (out_ready = '1' and hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0)
        or (out_ready = '1' and hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3)
        else
        '0';

    lo_ready <= '1' when (out_ready = '1' and lo_valid = '1' and phase = 0 and buffer_size = 3)
        or (out_ready = '1' and lo_valid = '1' and hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0)
        or (out_ready = '1' and lo_valid = '1' and hi_valid = '1' and phase = 1 and buffer_size = 3)
        or (out_ready = '1' and lo_valid = '1' and phase = 1 and buffer_size = 5)
        or (out_ready = '1' and lo_valid = '1' and hi_valid = '0' and phase = 0 and buffer_size = 0)
        else
        '0';

    out_valid <= '1' when hi_valid = '1' or lo_valid = '1' or buffer_size /= 0
        else
        '0';

    i_process : process (clk)
        variable i, l : integer;
    begin
        if rising_edge(clk) then
            if out_ready = '1' then
                if hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 0 then
                    for i in 0 to 1 loop
                        buffer1(i)    <= hi_words(i + 7);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 2;
                elsif hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 2 then
                    for i in 1 to 3 loop
                        buffer1(i)    <= hi_words(i + 5);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 3;
                elsif hi_valid = '1' and hi_size = "11" and phase = 0 and buffer_size = 3 then
                    for i in 0 to 5 loop
                        buffer1(i)    <= hi_words(i + 3);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 6;
                elsif hi_valid = '0' and phase = 0 and buffer_size = 3 then
                    buffer_size <= 0;
                elsif hi_valid = '1' and hi_size = "10" and phase = 0 and buffer_size = 6 then
                    for i in 0 to 6 loop
                        buffer1(i)    <= hi_words(i + 2);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 7;
                elsif phase = 0 and buffer_size = 7 then
                    buffer_size <= 0;
                    if lo_valid = '1' and hi_valid = '1' then
                        phase <= 1;
                    else
                        phase <= 0;
                    end if;
                elsif hi_valid = '1' and hi_size = "11" and phase = 1 and buffer_size = 0 then
                    for i in 1 to 3 loop
                        buffer1(i)    <= hi_words(i + 5);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 3;
                elsif hi_valid = '1' and hi_size = "10" and phase = 1 and buffer_size = 3 then
                    for i in 2 to 6 loop
                        buffer1(i)    <= hi_words(i + 2);
                        buffer1_vc(i) <= hi_vc;
                    end loop;
                    buffer_size <= 5;
                elsif phase = 1 and buffer_size = 5 then
                    buffer_size <= 0;
                    phase       <= 0;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
