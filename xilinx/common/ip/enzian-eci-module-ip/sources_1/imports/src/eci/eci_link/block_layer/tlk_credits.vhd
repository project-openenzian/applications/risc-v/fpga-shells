----------------------------------------------------------------------------------
-- tlk_credits
-- Adam Turowski
-- ETH Zürich 2021
--
-- Manage VC credits
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity tlk_credits is
    port (
        clk           : in std_logic;
        rst_n         : in std_logic;
        in_hi_words   : in WORDS (8 downto 0);
        in_hi_vc      : in std_logic_vector(3 downto 0);
        in_hi_size    : in std_logic_vector(1 downto 0); -- 00 - 4 words, 01 - 5 words, 10 - 8 words, 11 - 9 words
        in_hi_valid   : in std_logic;
        in_hi_ready   : buffer std_logic;
        in_lo_word    : in std_logic_vector(63 downto 0);
        in_lo_vc      : in std_logic_vector(3 downto 0);
        in_lo_valid   : in std_logic;
        in_lo_ready   : buffer std_logic;
        out_hi_words  : buffer WORDS (8 downto 0);
        out_hi_vc     : buffer std_logic_vector(3 downto 0);
        out_hi_size   : buffer std_logic_vector(1 downto 0); -- 00 - 4 words, 01 - 5 words, 10 - 8 words, 11 - 9 words
        out_hi_valid  : buffer std_logic;
        out_hi_ready  : in std_logic;
        out_lo_word   : buffer std_logic_vector(63 downto 0);
        out_lo_vc     : buffer std_logic_vector(3 downto 0);
        out_lo_valid  : buffer std_logic;
        out_lo_ready  : in std_logic;
        credits       : in std_logic_vector(7 downto 0);
        hi_credits    : in std_logic;
        credits_valid : in std_logic
    );
end tlk_credits;

architecture Behavioral of tlk_credits is

    type CREDIT is array (integer range <>) of unsigned(8 downto 0);
    signal credits_available : CREDIT(12 downto 0);
    signal in_hi_credits     : unsigned(8 downto 0);
    signal rst_n_old         : std_logic := '0';
    signal returned_credits  : CREDIT(12 downto 0);

begin

    c_r : for i in 0 to 12 generate
        returned_credits(i) <= to_unsigned(8, 9) when credits_valid = '1' and ((i < 8 and hi_credits = '0' and credits(i) = '1') or (i >= 8 and hi_credits = '1' and credits(i - 8) = '1')) else
        to_unsigned(0, 9);
    end generate;

    in_hi_credits <=
        "000000100" when in_hi_size = "00" else
        "000000101" when in_hi_size = "01" else
        "000001000" when in_hi_size = "10" else
        "000001001";

    in_hi_ready <= '1' when in_hi_credits <= credits_available(to_integer(unsigned(in_hi_vc))) and (out_hi_valid = '0' or (out_hi_valid = '1' and out_hi_ready = '1'))
        else
        '0';
    in_lo_ready <= '1' when (in_lo_vc = X"1101" or credits_available(to_integer(unsigned(in_lo_vc))) /= 0) and (out_lo_valid = '0' or (out_lo_valid = '1' and out_lo_ready = '1'))
        else
        '0';

    i_process : process (clk)
        variable i, j         : integer;
        variable popped_words : CREDIT(12 downto 0);
    begin
        if rising_edge(clk) then
            rst_n_old <= rst_n;
            if rst_n_old = '1' and rst_n = '0' then -- reset the credits on the falling edge, not on the level, because credits can arrive one cycle before deasserted rst_n
                out_hi_valid <= '0';
                out_lo_valid <= '0';
                for i in 0 to 12 loop
                    credits_available(i) <= (others => '0');
                end loop;
            else
                for i in 0 to 12 loop
                    popped_words(i) := (others => '0');
                end loop;
                if in_hi_ready = '1' and in_hi_valid = '1' then
                    out_hi_valid <= '1';
                    out_hi_words <= in_hi_words;
                    out_hi_vc    <= in_hi_vc;
                    out_hi_size  <= in_hi_size;
                    popped_words(to_integer(unsigned(in_hi_vc))) := in_hi_credits;
                elsif out_hi_valid = '1' and out_hi_ready = '1' then
                    out_hi_valid <= '0';
                end if;

                if in_lo_ready = '1' and in_lo_valid = '1' then
                    out_lo_valid <= '1';
                    out_lo_word  <= in_lo_word;
                    out_lo_vc    <= in_lo_vc;
                    if in_lo_vc /= X"1101" then
                        popped_words(to_integer(unsigned(in_lo_vc))) := "000000001";
                    end if;
                elsif out_lo_valid = '1' and out_lo_ready = '1' then
                    out_lo_valid <= '0';
                end if;

                for i in 0 to 12 loop
                    credits_available(i) <= credits_available(i) + returned_credits(i) - popped_words(i);
                end loop;
            end if;
        end if;
    end process;

end Behavioral;
