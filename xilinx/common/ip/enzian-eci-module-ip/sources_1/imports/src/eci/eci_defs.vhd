library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

package eci_defs is
    subtype WORD is std_logic_vector(63 downto 0);
    type WORDS is array (integer range <>) of WORD;
    type WORDS_MUX_ARRAY is array (integer range <>) of WORDS(8 downto 0);
    type WORDS_ARRAY is array (integer range <>) of WORDS(16 downto 0);
    type VCS is array (integer range <>) of std_logic_vector(3 downto 0);
    type VC_MUX_SIZES is array (integer range <>) of std_logic_vector(1 downto 0);
    type VC_SIZES is array (integer range <>) of std_logic_vector(4 downto 0);

    constant ECI_IREQ_IOBLD     : std_logic_vector(4 downto 0) := "00000";
    constant ECI_IREQ_IOBST     : std_logic_vector(4 downto 0) := "00001";
    constant ECI_IREQ_IOBSTA    : std_logic_vector(4 downto 0) := "00011";
    constant ECI_IREQ_SLILD     : std_logic_vector(4 downto 0) := "11100";
    constant ECI_IREQ_SLIST     : std_logic_vector(4 downto 0) := "11101";

    constant ECI_IRSP_IOBRSP    : std_logic_vector(4 downto 0) := "00000";
    constant ECI_IRSP_IOBACK    : std_logic_vector(4 downto 0) := "00001";
    constant ECI_IRSP_SLIRSP    : std_logic_vector(4 downto 0) := "00010";
    constant ECI_IRSP_IDLE      : std_logic_vector(4 downto 0) := "11111";

    constant ECI_MDLD_LNKD      : std_logic_vector(4 downto 0) := "10000";

    type MIB_VC is record
        data                : WORDS(6 downto 0);
        valid               : std_logic;
        word_enable         : std_logic_vector(6 downto 0);
    end record MIB_VC;
    
    type MIB_VCS is record
        vc13                : WORD;
        vc13_valid          : std_logic;
        vc12                : MIB_VC;
        vc11                : MIB_VC;
        vc10                : MIB_VC;
        vc9                 : MIB_VC;
        vc8                 : MIB_VC;
        vc7                 : MIB_VC;
        vc6                 : MIB_VC;
        vc5                 : MIB_VC;
        vc4                 : MIB_VC;
        vc3                 : MIB_VC;
        vc2                 : MIB_VC;
        vc1                 : MIB_VC;
        vc0                 : MIB_VC;
    end record MIB_VCS;
    
    type MOB_HI_VC is record
        data        : WORDS (8 downto 0);
        no          : std_logic_vector(3 downto 0);
        size        : std_logic_vector(1 downto 0); -- 00 - 4 words, 01 - 5 words, 10 - 8 words, 11 - 9 words
        valid       : std_logic;
    end record MOB_HI_VC;
    
    type MOB_LO_VC is record
        data        : WORD;
        no          : std_logic_vector(3 downto 0);
        valid       : std_logic;
    end record MOB_LO_VC;

    function vector_to_words(X : std_logic_vector(447 downto 0))
        return WORDS;
    function words_to_vector(X : WORDS)
        return std_logic_vector;        
end package;


package body eci_defs is
    function vector_to_words(X : std_logic_vector(447 downto 0))
        return WORDS is
        variable i : integer;
        variable tmp : WORDS;
    begin
        return (X(447 downto 384), X(383 downto 320), X(319 downto 256), X(255 downto 192), X(191 downto 128), X(127 downto 64), X(63 downto 0));
    end vector_to_words;
    
    function words_to_vector(X : WORDS)
        return std_logic_vector is
    begin
        return X(6) & X(5) & X(4) & X(3) & X(2) & X(1) & X(0);
    end words_to_vector;
end package body eci_defs;
