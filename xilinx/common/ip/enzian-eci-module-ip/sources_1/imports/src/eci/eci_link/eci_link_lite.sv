`ifndef ECI_LINK_LITE_SV
`define ECI_LINK_LITE_SV
import block_types::*;
import eci_package::*;

module eci_link_lite #
(
    parameter AXI_ADDR_WIDTH = 36,
    parameter AXI_DATA_WIDTH = WORD_WIDTH,
    parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH / 8)
) (
    input wire clk,  // Clock
    input wire reset,  // Actrlbtype_is_synchronous reset active high

    //------------------ I/O to/from ECI Link interfaces -------------------//
    // RX
    input wire [BLOCK_WIDTH-1:0] blk_rx_data,
    input wire                   blk_rx_valid,
    input wire                   blk_crc_match,

    // TX
    output wire [BLOCK_WIDTH-1:0] blk_tx_data,
    input  wire                   blk_tx_ready,

    //-------------------------- MIB VCs Outputs ----------------------------//
    // VC 11 - 6
    output wire [WORD_WIDTH-1:0]                mib_vc11_co[VC_CO_MAX_SIZE],
    output wire                                 mib_vc11_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc11_co_word_enable,
    input  wire                                 mib_vc11_co_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc10_co[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc10_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc10_co_word_enable,
    input  wire                                 mib_vc10_co_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc9_co[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc9_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc9_co_word_enable,
    input  wire                                 mib_vc9_co_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc8_co[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc8_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc8_co_word_enable,
    input  wire                                 mib_vc8_co_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc7_co[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc7_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc7_co_word_enable,
    input  wire                                 mib_vc7_co_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc6_co[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc6_co_valid,
    output wire [           VC_CO_MAX_SIZE-1:0] mib_vc6_co_word_enable,
    input  wire                                 mib_vc6_co_ready,
    // VC 5 - 2
    output wire [WORD_WIDTH-1:0]                mib_vc5_cd[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc5_cd_valid,
    output wire [           VC_CD_MAX_SIZE-1:0] mib_vc5_cd_word_enable,
    input  wire                                 mib_vc5_cd_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc4_cd[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc4_cd_valid,
    output wire [           VC_CD_MAX_SIZE-1:0] mib_vc4_cd_word_enable,
    input  wire                                 mib_vc4_cd_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc3_cd[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc3_cd_valid,
    output wire [           VC_CD_MAX_SIZE-1:0] mib_vc3_cd_word_enable,
    input  wire                                 mib_vc3_cd_ready,
    output wire [WORD_WIDTH-1:0]                mib_vc2_cd[VC_CO_MAX_SIZE-1:0],
    output wire                                 mib_vc2_cd_valid,
    output wire [           VC_CD_MAX_SIZE-1:0] mib_vc2_cd_word_enable,
    input  wire                                 mib_vc2_cd_ready,

    // VC 1 - 0
    output wire [    WORD_WIDTH-1:0] mib_vc0_io            [VC_IO_MAX_SIZE-1:0],
    output wire                      mib_vc0_io_valid,
    output wire [VC_IO_MAX_SIZE-1:0] mib_vc0_io_word_enable,
    input  wire                      mib_vc0_io_ready,

    output wire [    WORD_WIDTH-1:0] mib_vc1_io            [VC_IO_MAX_SIZE-1:0],
    output wire                      mib_vc1_io_valid,
    output wire [VC_IO_MAX_SIZE-1:0] mib_vc1_io_word_enable,
    input  wire                      mib_vc1_io_ready,

    // MCD VC13
    output wire [                WORD_WIDTH-1:0] mib_vc_mcd,
    output wire                                  mib_vc_mcd_valid,
    input  wire                                  mib_vc_mcd_ready,
    // MIC VC12
    output wire [WORD_WIDTH-1:0]                 mib_vc_mic[VC_MXC_MAX_SIZE-1:0],
    output wire                                  mib_vc_mic_valid,
    output wire [           VC_MXC_MAX_SIZE-1:0] mib_vc_mic_word_enable,
    input  wire                                  mib_vc_mic_ready,

    //--------------------------- MOB VCs Inputs ----------------------------//
    // Lo bandwidth VC
    input  wire [63:0] mob_lo_vc,
    input  wire        mob_lo_vc_valid,
    input  wire [ 3:0] mob_lo_vc_no,
    output wire        mob_lo_vc_ready,

    // Hi bandwidth VC
    input  wire [63:0] mob_hi_vc      [8:0],
    input  wire        mob_hi_vc_valid,
    input  wire [ 3:0] mob_hi_vc_no,
    input  wire [ 1:0] mob_hi_vc_size,  // 10 - mob_hi_vc[8:1], 11 - mob_hi_vc[8:0]
    output wire        mob_hi_vc_ready,

    //------------------- IO Facing the Generic IO User Interface -------------//
    // User Slave AXI Interface: carries gpio requests from thunderx
    output logic [AXI_ADDR_WIDTH-1:0] m_axi_awaddr,
    output logic                      m_axi_awvalid,
    input  logic                      m_axi_awready,
    // Write data channel
    output logic [AXI_DATA_WIDTH-1:0] m_axi_wdata,
    output logic [AXI_STRB_WIDTH-1:0] m_axi_wstrb,
    output logic                      m_axi_wvalid,
    input  logic                      m_axi_wready,
    // Write response channel
    input  logic [               1:0] m_axi_bresp,
    input  logic                      m_axi_bvalid,
    output logic                      m_axi_bready,
    //AXI Lite Read Interface
    // Read address channel
    output logic [AXI_ADDR_WIDTH-1:0] m_axi_araddr,
    output logic                      m_axi_arvalid,
    input  logic                      m_axi_arready,
    // Read data channel
    input  logic [    WORD_WIDTH-1:0] m_axi_rdata,
    input  logic [               1:0] m_axi_rresp,
    input  logic                      m_axi_rvalid,
    output logic                      m_axi_rready,

    // Link Status
    output reg link_up
);

  // declare signals
  wire rst_n;
  SyncBlock_t rx_sync_block;  // Sync Block from RLK
  wire rx_sync_block_valid;
  wire rx_blk_received;  // A valid data block received
  wire rx_blk_error;

  wire [7:0] credits;
  wire hi_credits;
  wire credits_valid;
  wire return_cred[12:0];

  wire [WORD_WIDTH-1:0] mib_vc_co[NUM_CO_VCS-1:0][VC_CO_MAX_SIZE-1:0];
  wire mib_vc_co_valid[NUM_CO_VCS-1:0];
  wire [VC_CO_MAX_SIZE-1:0] mib_vc_co_word_enable[NUM_CO_VCS-1:0];
  wire mib_vc_co_ready[NUM_CO_VCS-1:0];
  // VC 5 - 2
  wire [WORD_WIDTH-1:0] mib_vc_cd[NUM_CD_VCS-1:0][VC_CD_MAX_SIZE-1:0];
  wire mib_vc_cd_valid[NUM_CD_VCS-1:0];
  wire [VC_CD_MAX_SIZE-1:0] mib_vc_cd_word_enable[NUM_CD_VCS-1:0];
  wire mib_vc_cd_ready[NUM_CD_VCS-1:0];

  wire [WORD_WIDTH-1:0] mib_vc_mic_w[VC_MXC_MAX_SIZE-1:0];

  wire tx_has_data_payload;
  wire rx_has_data_payload;
  wire rx_has_sync_block;


  wire [WORD_WIDTH-1:0] tlk_gpio_vc1[1:0];
  wire tlk_gpio_vc1_valid[1:0];
  wire tlk_gpio_vc1_ready;

  wire [WORD_WIDTH-1:0] tlk_gpio_vc0[1:0];
  wire tlk_gpio_vc0_valid[1:0];
  wire tlk_gpio_vc0_ready;

wire [5:0] out_link_state;
logic out_link_state_change;
logic [5:0] out_link_state_old;
logic [29:0] counter;

  // Pipeline registers for timing
  (* DONT_TOUCH="true" *) logic [BLOCK_WIDTH-1:0] blk_rx_data_reg;
  (* DONT_TOUCH="true" *) logic blk_rx_valid_reg;
  (* DONT_TOUCH="true" *) logic blk_crc_match_reg;

  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//

  wire [WORD_WIDTH-1:0] vio_cmd;
  wire vio_send;
  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//

  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//
  //*******************************************************************************//

  assign rst_n = ~reset;
  //////////////////////////////////////////////////////////////////////////
  //////////////////////       Link RLK Module         /////////////////////
  //////////////////////////////////////////////////////////////////////////

  //Adding a pipeline register for timing
  always @(posedge clk) begin
    if (~rst_n) begin
      blk_rx_valid_reg <= 0;
      blk_crc_match_reg <= 0;
    end else begin
      blk_rx_data_reg <= blk_rx_data;
      blk_rx_valid_reg <= blk_rx_valid;
      blk_crc_match_reg <= blk_crc_match;
    end
  end

  assign mib_vc11_co = mib_vc_co[5];
  assign mib_vc10_co = mib_vc_co[4];
  assign mib_vc9_co = mib_vc_co[3];
  assign mib_vc8_co = mib_vc_co[2];
  assign mib_vc7_co = mib_vc_co[1];
  assign mib_vc6_co = mib_vc_co[0];

  assign mib_vc11_co_valid = mib_vc_co_valid[5];
  assign mib_vc11_co_word_enable = mib_vc_co_word_enable[5];
  assign mib_vc_co_ready[5] = mib_vc11_co_ready;

  assign mib_vc10_co_valid = mib_vc_co_valid[4];
  assign mib_vc10_co_word_enable = mib_vc_co_word_enable[4];
  assign mib_vc_co_ready[4] = mib_vc10_co_ready;

  assign mib_vc9_co_valid = mib_vc_co_valid[3];
  assign mib_vc9_co_word_enable = mib_vc_co_word_enable[3];
  assign mib_vc_co_ready[3] = mib_vc9_co_ready;

  assign mib_vc8_co_valid = mib_vc_co_valid[2];
  assign mib_vc8_co_word_enable = mib_vc_co_word_enable[2];
  assign mib_vc_co_ready[2] = mib_vc8_co_ready;

  assign mib_vc7_co_valid = mib_vc_co_valid[1];
  assign mib_vc7_co_word_enable = mib_vc_co_word_enable[1];
  assign mib_vc_co_ready[1] = mib_vc7_co_ready;

  assign mib_vc6_co_valid = mib_vc_co_valid[0];
  assign mib_vc6_co_word_enable = mib_vc_co_word_enable[0];
  assign mib_vc_co_ready[0] = mib_vc6_co_ready;

  // MIB VCS 5 - 2
  assign mib_vc5_cd = mib_vc_cd[3];
  assign mib_vc4_cd = mib_vc_cd[2];
  assign mib_vc3_cd = mib_vc_cd[1];
  assign mib_vc2_cd = mib_vc_cd[0];

  assign mib_vc5_cd_valid = mib_vc_cd_valid[3];
  assign mib_vc5_cd_word_enable = mib_vc_cd_word_enable[3];
  assign mib_vc_cd_ready[3] = mib_vc5_cd_ready;

  assign mib_vc4_cd_valid = mib_vc_cd_valid[2];
  assign mib_vc4_cd_word_enable = mib_vc_cd_word_enable[2];
  assign mib_vc_cd_ready[2] = mib_vc4_cd_ready;

  assign mib_vc3_cd_valid = mib_vc_cd_valid[1];
  assign mib_vc3_cd_word_enable = mib_vc_cd_word_enable[1];
  assign mib_vc_cd_ready[1] = mib_vc3_cd_ready;

  assign mib_vc2_cd_valid = mib_vc_cd_valid[0];
  assign mib_vc2_cd_word_enable = mib_vc_cd_word_enable[0];
  assign mib_vc_cd_ready[0] = mib_vc2_cd_ready;

  // MIB MIC
  assign mib_vc_mic = mib_vc_mic_w;

  ///////////////////////////////////////////////////////////////////////////////////////////////////

  link_rlk rlk (
      .clk  (clk),
      .rst_n(rst_n),

      //------------------ Input Block ----------------------//
      .blk_rx_data  (blk_rx_data_reg),
      .blk_rx_valid (blk_rx_valid_reg),
      .blk_crc_match(blk_crc_match_reg),

      //---------------- VCs Data Words Received ------------//
      .vc_co            (mib_vc_co),
      .vc_co_valid      (mib_vc_co_valid),
      .vc_co_word_enable(mib_vc_co_word_enable),
      .vc_co_ready      (mib_vc_co_ready),

      .vc_cd            (mib_vc_cd),
      .vc_cd_valid      (mib_vc_cd_valid),
      .vc_cd_word_enable(mib_vc_cd_word_enable),
      .vc_cd_ready      (mib_vc_cd_ready),

      .vc_io            ('{mib_vc1_io, mib_vc0_io}),
      .vc_io_valid      ('{mib_vc1_io_valid, mib_vc0_io_valid}),
      .vc_io_word_enable('{mib_vc1_io_word_enable, mib_vc0_io_word_enable}),
      .vc_io_ready      ('{mib_vc1_io_ready, mib_vc0_io_ready}),

      .vc_mcd      (mib_vc_mcd),
      .vc_mcd_valid(mib_vc_mcd_valid),
      .vc_mcd_ready(mib_vc_mcd_ready),

      .vc_mic            (mib_vc_mic_w),
      .vc_mic_valid      (mib_vc_mic_valid),
      .vc_mic_word_enable(mib_vc_mic_word_enable),
      .vc_mic_ready      (mib_vc_mic_ready),

      //------------------ Synch Block Detected -------------//
      .rx_sync_block      (rx_sync_block),
      .rx_sync_block_valid(rx_sync_block_valid),
      .rx_blk_received    (rx_blk_received),
      .rx_blk_error       (rx_blk_error),

      //--------- Returned Credit from Thunderx -------------//
      .credits      (credits),
      .hi_credits   (hi_credits),
      .credits_valid(credits_valid),

      //-------- Credit to Return to Thunderx ---------------//
      .return_cred(return_cred),

      //---------------- Debug Interface --------------------//
      .debug_counters(),
      .rcvd_cmds     ()
  );

  ////////////////////////////////////
  link_tlk_lite tlk (
      .clk  (clk),
      .rst_n(rst_n),

      //------------------ Output Block to Thunderx ------------------//
      .blk_tx_data (blk_tx_data),
      .blk_tx_ready(blk_tx_ready),

      //--------------------- Received Synch Block -------------------//
      .rx_sync_block        (rx_sync_block),
      .rx_sync_block_valid  (rx_sync_block_valid),
      .rx_blk_received      (rx_blk_received),
      .rx_blk_error         (rx_blk_error),
      .rx_blk_crc_match     (blk_crc_match_reg),

      //----------------- Returned Credit from Thunderx --------------//
      .credits      (credits),
      .hi_credits   (hi_credits),
      .credits_valid(credits_valid),

      //----------------- Credit to Return to Thunderx ---------------//
      .return_cred(return_cred),

      //---------------- VCs Data Words TO SEND ------------//
      .mob_lo_vc      (mob_lo_vc),
      .mob_lo_vc_valid(mob_lo_vc_valid),
      .mob_lo_vc_no   (mob_lo_vc_no),
      .mob_lo_vc_ready(mob_lo_vc_ready),
      .mob_hi_vc      (mob_hi_vc),
      .mob_hi_vc_valid(mob_hi_vc_valid),
      .mob_hi_vc_no   (mob_hi_vc_no),
      .mob_hi_vc_size (mob_hi_vc_size),
      .mob_hi_vc_ready(mob_hi_vc_ready),
      //--------------- Link status to block com module --------------//
      .link_up        (link_up),
        .out_link_state (out_link_state),
        
      //---------------------- Debug Interface -----------------------//
      .debug_counters (),
      .debug_counters2(),
      .blk_trx_state  ()
  );

//  ila_3 eci_edge_ila (
//      .clk   (clk),
//      .probe0(blk_rx_data_reg),
//      .probe1(blk_rx_valid_reg),
//      .probe2(blk_crc_match_reg),
//      .probe3(rx_blk_received),
//      .probe4(rx_blk_error),
//      .probe5(blk_tx_data),
//      .probe6(rx_has_sync_block),
//      .probe7(tx_has_data_payload),
//      .probe8(rx_has_data_payload),
//      .probe9(blk_tx_ready),
//      .probe10(link_up),
//      .probe11(out_link_state),
//      .probe12(out_link_state_change),
//      .probe13(counter)
//  );

//always @(posedge clk) begin
//    counter <= counter + 1;
//    if (out_link_state != out_link_state_old) begin
//      out_link_state_change <= 1;
//      out_link_state_old <= out_link_state;
//    end else begin
//      out_link_state_change <= 0;
//    end
//  end

//  eci_logger i_eci_logger (
//      .clk     (clk),
//      .reset   (reset),
//      .tx_data (blk_tx_data),
//      .tx_valid(tx_has_data_payload),
//      .rx_data (blk_rx_data_reg),
//      .rx_valid(rx_has_data_payload)
//  );

  assign tx_has_data_payload =
      blk_tx_data[63] & ~blk_tx_data[62] & blk_tx_ready & (blk_tx_data[51:24] != 28'hFFFFFFF);
  assign rx_has_data_payload = blk_rx_data_reg[63] & ~blk_rx_data_reg[62] & blk_rx_valid_reg & (
      blk_rx_data_reg[51:24] != 28'hFFFFFFF);
  assign rx_has_sync_block = (blk_rx_data_reg[63:61] == BTYPE_SYNC) & blk_rx_valid_reg;

endmodule
`endif
