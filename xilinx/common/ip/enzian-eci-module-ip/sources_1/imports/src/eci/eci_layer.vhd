----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Adam S. Turowski
-- 
-- Create Date: 15.05.2022 16:22:20
-- Design Name: 
-- Module Name: eci_layer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_layer is
generic (
    SECOND_LINK_ACTIVE : integer := 1
);
port (
    clk_io          : in std_logic;
    clk_sys         : out std_logic;
    reset_sys       : out std_logic;

    -- 156.25MHz transceiver reference clocks
    eci_gt_clk_p_link1 : in std_logic_vector(2 downto 0);
    eci_gt_clk_n_link1 : in std_logic_vector(2 downto 0);

    eci_gt_clk_p_link2 : in std_logic_vector(5 downto 3);
    eci_gt_clk_n_link2 : in std_logic_vector(5 downto 3);

    -- RX differential pairs
    eci_gt_rx_p_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_p_link2    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link2    : in std_logic_vector(11 downto 0);

    -- TX differential pairs
    eci_gt_tx_p_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_p_link2    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link2    : out std_logic_vector(11 downto 0);

    link1_mib               : out MIB_VCS;
    link1_mib_ready         : in std_logic_vector(13 downto 0);
    -------------------------- MOB VCs Inputs ----------------------------//
    link1_mob_hi_vc         : in MOB_HI_VC;
    link1_mob_hi_vc_ready   : out STD_LOGIC;
    link1_mob_lo_vc         : in MOB_LO_VC;
    link1_mob_lo_vc_ready   : out STD_LOGIC;

    link2_mib               : out MIB_VCS;
    link2_mib_ready         : in std_logic_vector(13 downto 0);
    -------------------------- MOB VCs Inputs ----------------------------//
    link2_mob_hi_vc         : in MOB_HI_VC;
    link2_mob_hi_vc_ready   : out STD_LOGIC;
    link2_mob_lo_vc         : in MOB_LO_VC;
    link2_mob_lo_vc_ready   : out STD_LOGIC;
    
    link_up                 : out std_logic;

    -- for debug purposes
    link1_eci_block_out         : out std_logic_vector(511 downto 0);
    link1_eci_block_out_valid   : out std_logic;
    link1_eci_crc_match_out     : out std_logic;

    link2_eci_block_out         : out std_logic_vector(511 downto 0);
    link2_eci_block_out_valid   : out std_logic;
    link2_eci_crc_match_out     : out std_logic
);
end eci_layer;

architecture Behavioral of eci_layer is

component xcvr_link1
port (
    gtwiz_userclk_tx_active_in : in std_logic_vector(0 downto 0);
    gtwiz_userclk_rx_active_in : in std_logic_vector(0 downto 0);
    rxoutclk_out : out std_logic_vector(11 downto 0);
    rxusrclk_in : in std_logic_vector(11 downto 0);
    rxusrclk2_in : in std_logic_vector(11 downto 0);
    txoutclk_out : out std_logic_vector(11 downto 0);
    txusrclk_in : in std_logic_vector(11 downto 0);
    txusrclk2_in : in std_logic_vector(11 downto 0);

    ---- Reset Controller Signals
    -- 250MHz free-running clock for the reset controller
    gtwiz_reset_clk_freerun_in         :  in std_logic_vector(0 downto 0);
    -- Reset everything
    gtwiz_reset_all_in                 :  in std_logic_vector(0 downto 0);
    -- Reset TX-side components
    gtwiz_reset_tx_pll_and_datapath_in :  in std_logic_vector(0 downto 0);
    gtwiz_reset_tx_datapath_in         :  in std_logic_vector(0 downto 0);
    -- Reset RX-side components
    gtwiz_reset_rx_pll_and_datapath_in :  in std_logic_vector(0 downto 0);
    gtwiz_reset_rx_datapath_in         :  in std_logic_vector(0 downto 0);
    -- Clock Recovery is stable
    gtwiz_reset_rx_cdr_stable_out      : out std_logic_vector(0 downto 0);
    -- TX/RX subsystem is out of reset
    gtwiz_reset_tx_done_out            : out std_logic_vector(0 downto 0);
    gtwiz_reset_rx_done_out            : out std_logic_vector(0 downto 0);

    -- Data to be transmitted, synchronised to tx_usrclk2
    gtwiz_userdata_tx_in  :  in std_logic_vector(12*64-1 downto 0);
    -- Received data, synchronised to rx_usrclk2
    gtwiz_userdata_rx_out : out std_logic_vector(12*64-1 downto 0);

    -- The 156.25MHz reference clocks
    gtrefclk00_in : in std_logic_vector(2 downto 0);

    -- The recovered (10GHz) clocks, and the buffered reference clock.
    qpll0outclk_out    : out std_logic_vector(2 downto 0);
    qpll0outrefclk_out : out std_logic_vector(2 downto 0);

    -- RX differential pairs
    gtyrxn_in : in std_logic_vector(11 downto 0);
    gtyrxp_in : in std_logic_vector(11 downto 0);

    -- TX differential pair
    gtytxn_out : out std_logic_vector(11 downto 0);
    gtytxp_out : out std_logic_vector(11 downto 0);

    -- Gearbox
    rxgearboxslip_in :  in std_logic_vector(11 downto 0);
    rxdatavalid_out :   out std_logic_vector(2*12-1 downto 0);
    rxheader_out :      out std_logic_vector(6*12-1 downto 0);
    rxheadervalid_out : out std_logic_vector(2*12-1 downto 0);
    rxstartofseq_out :  out std_logic_vector(2*12-1 downto 0);
    txheader_in       : in std_logic_vector(6*12-1 downto 0);
    txsequence_in     : in std_logic_vector(7*12-1 downto 0);

    -- RX bypass buffer
    gtwiz_buffbypass_rx_reset_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_start_user_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_done_out : out std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_error_out : out std_logic_vector(0 downto 0);

    -- TX bypass buffer
    gtwiz_buffbypass_tx_reset_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_start_user_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_done_out : out std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_error_out : out std_logic_vector(0 downto 0);

    -- Internal reset status.
    rxpmaresetdone_out    : out std_logic_vector(11 downto 0);
    txpmaresetdone_out    : out std_logic_vector(11 downto 0);
    txprgdivresetdone_out : out std_logic_vector(11 downto 0);
    gtpowergood_out       : out std_logic_vector(11 downto 0);

    -- TX driver control
    txdiffctrl_in   : in std_logic_vector(5*12-1 downto 0);
    txpostcursor_in : in std_logic_vector(5*12-1 downto 0);
    txprecursor_in  : in std_logic_vector(5*12-1 downto 0)
);
end component;

component xcvr_link2
port (
    gtwiz_userclk_tx_active_in : in std_logic_vector(0 downto 0);
    gtwiz_userclk_rx_active_in : in std_logic_vector(0 downto 0);
    rxoutclk_out : out std_logic_vector(11 downto 0);
    rxusrclk_in : in std_logic_vector(11 downto 0);
    rxusrclk2_in : in std_logic_vector(11 downto 0);
    txoutclk_out : out std_logic_vector(11 downto 0);
    txusrclk_in : in std_logic_vector(11 downto 0);
    txusrclk2_in : in std_logic_vector(11 downto 0);

    ---- Reset Controller Signals
    -- 250MHz free-running clock for the reset controller
    gtwiz_reset_clk_freerun_in         :  in std_logic_vector(0 downto 0);
    -- Reset everything
    gtwiz_reset_all_in                 :  in std_logic_vector(0 downto 0);
    -- Reset TX-side components
    gtwiz_reset_tx_pll_and_datapath_in :  in std_logic_vector(0 downto 0);
    gtwiz_reset_tx_datapath_in         :  in std_logic_vector(0 downto 0);
    -- Reset RX-side components
    gtwiz_reset_rx_pll_and_datapath_in :  in std_logic_vector(0 downto 0);
    gtwiz_reset_rx_datapath_in         :  in std_logic_vector(0 downto 0);
    -- Clock Recovery is stable
    gtwiz_reset_rx_cdr_stable_out      : out std_logic_vector(0 downto 0);
    -- TX/RX subsystem is out of reset
    gtwiz_reset_tx_done_out            : out std_logic_vector(0 downto 0);
    gtwiz_reset_rx_done_out            : out std_logic_vector(0 downto 0);

    -- Data to be transmitted, synchronised to tx_usrclk2
    gtwiz_userdata_tx_in  :  in std_logic_vector(12*64-1 downto 0);
    -- Received data, synchronised to rx_usrclk2
    gtwiz_userdata_rx_out : out std_logic_vector(12*64-1 downto 0);

    -- The 156.25MHz reference clocks
    gtrefclk00_in : in std_logic_vector(2 downto 0);

    -- The recovered (10GHz) clocks, and the buffered reference clock.
    qpll0outclk_out    : out std_logic_vector(2 downto 0);
    qpll0outrefclk_out : out std_logic_vector(2 downto 0);

    -- RX differential pairs
    gtyrxn_in : in std_logic_vector(11 downto 0);
    gtyrxp_in : in std_logic_vector(11 downto 0);

    -- TX differential pair
    gtytxn_out : out std_logic_vector(11 downto 0);
    gtytxp_out : out std_logic_vector(11 downto 0);

    -- Gearbox
    rxgearboxslip_in :  in std_logic_vector(11 downto 0);
    rxdatavalid_out :   out std_logic_vector(2*12-1 downto 0);
    rxheader_out :      out std_logic_vector(6*12-1 downto 0);
    rxheadervalid_out : out std_logic_vector(2*12-1 downto 0);
    rxstartofseq_out :  out std_logic_vector(2*12-1 downto 0);
    txheader_in       : in std_logic_vector(6*12-1 downto 0);
    txsequence_in     : in std_logic_vector(7*12-1 downto 0);

    -- RX bypass buffer
    gtwiz_buffbypass_rx_reset_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_start_user_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_done_out : out std_logic_vector(0 downto 0);
    gtwiz_buffbypass_rx_error_out : out std_logic_vector(0 downto 0);

    -- TX bypass buffer
    gtwiz_buffbypass_tx_reset_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_start_user_in : in std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_done_out : out std_logic_vector(0 downto 0);
    gtwiz_buffbypass_tx_error_out : out std_logic_vector(0 downto 0);

    -- Internal reset status.
    rxpmaresetdone_out    : out std_logic_vector(11 downto 0);
    txpmaresetdone_out    : out std_logic_vector(11 downto 0);
    txprgdivresetdone_out : out std_logic_vector(11 downto 0);
    gtpowergood_out       : out std_logic_vector(11 downto 0);

    -- TX driver control
    txdiffctrl_in   : in std_logic_vector(5*12-1 downto 0);
    txpostcursor_in : in std_logic_vector(5*12-1 downto 0);
    txprecursor_in  : in std_logic_vector(5*12-1 downto 0);
    
    txpd_in         : in std_logic_vector(23 downto 0)
);
end component;

component il_rx_link_gearbox is
generic (
    LANES : integer;
    METAFRAME : integer
);
port (
    clk_rx : in std_logic;
    reset  : in std_logic;

    xcvr_rxdata  : in std_logic_vector(LANES*64 - 1 downto 0);
    xcvr_rxdatavalid : in std_logic_vector(2*LANES - 1 downto 0);
    xcvr_rxheader    : in std_logic_vector(6*LANES - 1 downto 0);
    xcvr_rxheadervalid : in std_logic_vector(2*LANES - 1 downto 0);
    xcvr_rxgearboxslip : out std_logic_vector(LANES - 1 downto 0);

    output        : out std_logic_vector(LANES*64 - 1 downto 0);
    output_valid  : out std_logic;
    ctrl_word_out : out std_logic_vector(LANES - 1 downto 0);

    lane_word_lock  : out std_logic_vector(  LANES - 1 downto 0);
    lane_frame_lock : out std_logic_vector(  LANES - 1 downto 0);
    lane_crc32_bad  : out std_logic_vector(  LANES - 1 downto 0);
    lane_status     : out std_logic_vector(2*LANES - 1 downto 0);

    link_aligned  : out std_logic;
    total_skew     : out std_logic_vector(2 downto 0)
);
end component;

component il_tx_link_gearbox is
generic (
    LANES : integer;
    METAFRAME : integer
);
port (
    clk_tx : in std_logic;
    reset  : in std_logic;

    input        :  in std_logic_vector(LANES*64 - 1 downto 0);
    input_ready  : out std_logic;
    ctrl_word_in :  in std_logic_vector(LANES - 1 downto 0);

    xcvr_txdata  : out std_logic_vector(LANES*64 - 1 downto 0);
    xcvr_txheader     : out std_logic_vector(6*LANES-1 downto 0);
    xcvr_txsequence   : out std_logic_vector(7*LANES-1 downto 0)
);
end component;


component eci_rx_blk is
generic (
    LANES : integer := 12
);
port (
    clk_rx : in std_logic;

    clk_blk   : in std_logic;

    link_aligned : in std_logic;

    link_data       : in std_logic_vector(64*12-1 downto 0);
    link_data_valid : in std_logic;
    ctrl_word       : in std_logic_vector( 11 downto 0);

    block_out       : out std_logic_vector(511 downto 0);
    block_out_valid : out std_logic;
    crc_match_out   : out std_logic
);
end component;

component eci_tx_blk is
generic (
    LANES : integer := 12
);
port (
    clk_tx          : in std_logic;

    clk_blk         : in std_logic;

    block_in        :  in std_logic_vector(511 downto 0);
    block_in_ready  : out std_logic;

    link_data       : out std_logic_vector(LANES*64-1 downto 0);
    link_data_ready :  in std_logic;
    ctrl_word_out   : out std_logic_vector( LANES-1 downto 0)
);
end component;

component eci_blk is
port (
    clk_blk   : in std_logic;
    reset_blk : in std_logic;

    block_in       : in std_logic_vector(511 downto 0);
    block_in_valid : in std_logic;
    crc_match_in   : in std_logic;

    block_out       : out std_logic_vector(511 downto 0);
    block_out_ready :  in std_logic
);
end component;

component eci_link_lite is
   port(
    clk     : in std_logic;
    reset   : in std_logic;
    link_up : out std_logic;
    
    blk_rx_data   : in std_logic_vector(511 downto 0);
    blk_rx_valid  : in std_logic;
    blk_crc_match : in std_logic;
    blk_tx_data   : out std_logic_vector(511 downto 0);
    blk_tx_ready  : in std_logic;

    mib_vc_mic              : out WORDS(6 downto 0);
    mib_vc_mic_valid        : out std_logic;
    mib_vc_mic_word_enable  : out std_logic_vector(6 downto 0);
    mib_vc_mic_ready        : in std_logic;

    mib_vc11_co       : out WORDS(6 downto 0);
    mib_vc11_co_valid : out std_logic;
    mib_vc11_co_word_enable  : out std_logic_vector(6 downto 0);
    mib_vc11_co_ready : in  std_logic;

    mib_vc10_co       : out WORDS(6 downto 0);
    mib_vc10_co_valid : out std_logic;
    mib_vc10_co_word_enable  : out std_logic_vector(6 downto 0);
    mib_vc10_co_ready : in  std_logic;

    mib_vc9_co        : out WORDS(6 downto 0);
    mib_vc9_co_valid  : out std_logic;
    mib_vc9_co_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc9_co_ready  : in  std_logic;
    mib_vc8_co        : out WORDS(6 downto 0);
    mib_vc8_co_valid  : out std_logic;
    mib_vc8_co_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc8_co_ready  : in  std_logic;
    mib_vc7_co        : out WORDS(6 downto 0);
    mib_vc7_co_valid  : out std_logic;
    mib_vc7_co_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc7_co_ready  : in  std_logic;
    mib_vc6_co        : out WORDS(6 downto 0);
    mib_vc6_co_valid  : out std_logic;
    mib_vc6_co_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc6_co_ready  : in  std_logic;
    -- VC 5 - 2
    mib_vc5_cd        : out WORDS(6 downto 0);
    mib_vc5_cd_valid  : out std_logic;
    mib_vc5_cd_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc5_cd_ready  : in  std_logic;
    mib_vc4_cd        : out WORDS(6 downto 0);
    mib_vc4_cd_valid  : out std_logic;
    mib_vc4_cd_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc4_cd_ready  : in  std_logic;
    mib_vc3_cd        : out WORDS(6 downto 0);
    mib_vc3_cd_valid  : out std_logic;
    mib_vc3_cd_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc3_cd_ready  : in  std_logic;
    mib_vc2_cd        : out WORDS(6 downto 0);
    mib_vc2_cd_valid  : out std_logic;
    mib_vc2_cd_word_enable   : out std_logic_vector(6 downto 0);
    mib_vc2_cd_ready  : in  std_logic;
    -- MCD VC13
    mib_vc_mcd        : out std_logic_vector(63 downto 0);
    mib_vc_mcd_valid  : out std_logic;
    mib_vc_mcd_ready  : in  std_logic;

    mib_vc0_io         : out WORDS(6 downto 0);
    mib_vc0_io_valid   : out std_logic;
    mib_vc0_io_word_enable    : out std_logic_vector(6 downto 0);
    mib_vc0_io_ready   : in std_logic;

    mib_vc1_io         : out WORDS(6 downto 0);
    mib_vc1_io_valid   : out std_logic;
    mib_vc1_io_word_enable    : out std_logic_vector(6 downto 0);
    mib_vc1_io_ready   : in std_logic;

    -------------------------- MOB VCs Inputs ----------------------------//
    mob_hi_vc           : in WORDS (8 downto 0);
    mob_hi_vc_no        : in std_logic_vector(3 downto 0);
    mob_hi_vc_size      : in std_logic_vector(1 downto 0); -- 00 - 4 words, 01 - 5 words, 10 - 8 words, 11 - 9 words
    mob_hi_vc_valid     : in STD_LOGIC;
    mob_hi_vc_ready     : out STD_LOGIC;
    mob_lo_vc           : in std_logic_vector(63 downto 0);
    mob_lo_vc_no        : in std_logic_vector(3 downto 0);
    mob_lo_vc_valid     : in STD_LOGIC;
    mob_lo_vc_ready     : out STD_LOGIC
);
end component;

type LINK is record
    xcvr_txd_raw        : std_logic_vector(64 * 12-1 downto 0);
    xcvr_rxd_raw        : std_logic_vector(64 * 12-1 downto 0);
    xcvr_txd            : std_logic_vector(64 * 12-1 downto 0);
    xcvr_rxd            : std_logic_vector(64 * 12-1 downto 0);
    xcvr_reset_rx       : std_logic;
    xcvr_tx_ready       : std_logic;
    xcvr_rx_ready       : std_logic;

    xcvr_txdiffctrl     : std_logic_vector(5*12-1 downto 0);
    xcvr_txpostcursor   : std_logic_vector(5*12-1 downto 0);
    xcvr_txprecursor    : std_logic_vector(5*12-1 downto 0);

    xcvr_rxgearboxslip  : std_logic_vector(11 downto 0);
    xcvr_rxdatavalid    : std_logic_vector(2*12-1 downto 0);
    xcvr_rxheader       : std_logic_vector(6*12-1 downto 0);
    xcvr_rxheadervalid  : std_logic_vector(2*12-1 downto 0);
    xcvr_rxstartofseq   : std_logic_vector(2*12-1 downto 0);
    xcvr_txheader       : std_logic_vector(6*12-1 downto 0);
    xcvr_txsequence     : std_logic_vector(7*12-1 downto 0);

    userclk_tx_active_in : std_logic;
    userclk_rx_active_in : std_logic;
    rxoutclk : std_logic_vector(11 downto 0);
    rxusrclk : std_logic_vector(11 downto 0);
    rxusrclk2 : std_logic_vector(11 downto 0);
    txoutclk : std_logic_vector(11 downto 0);
    txusrclk: std_logic_vector(11 downto 0);
    txusrclk2 : std_logic_vector(11 downto 0);

    -- The per-link transceiver data
    txd : std_logic_vector(64*12-1 downto 0);
    txd_header     : std_logic_vector(3 * 12-1 downto 0);
    txd_ready      : std_logic;
    rxd : std_logic_vector(64*12-1 downto 0);

    -- Transceiver-derived clocks.
    clk_tx, clk_rx : std_logic;
    clk_ref : std_logic_vector(2 downto 0);
    reset : std_logic;
end record LINK;

-- Interlaken link signals
type INTERLAKEN is record
    rx_data             : std_logic_vector(64*12-1 downto 0);
    rx_data_valid       : std_logic;
    rx_ctrl_word        : std_logic_vector(11 downto 0);
    rx_lane_word_lock   : std_logic_vector(11 downto 0);
    rx_lane_frame_lock  : std_logic_vector(11 downto 0);
    rx_word_lock        : std_logic;
    rx_frame_lock       : std_logic;
    rx_lane_crc32_bad   : std_logic_vector(11 downto 0);
    rx_lane_status      : std_logic_vector(2*12-1 downto 0);
    rx_aligned          : std_logic;
    rx_aligned_old      : std_logic;
    rx_total_skew       : std_logic_vector(2 downto 0);

    tx_data             : std_logic_vector(64*12-1 downto 0);
    tx_data_ready       : std_logic;
    tx_ctrl_word        : std_logic_vector(11 downto 0);

    usr_rx_reset : std_logic;
    usr_tx_reset : std_logic;
end record INTERLAKEN;

-- ECI block-layer signals, on clk
type ECI_BLOCK is record
    link_up             : std_logic;
    rx_block            : std_logic_vector(511 downto 0);
    rx_block_valid      : std_logic;
    rx_block_crc_match  : std_logic;
    tx_block            : std_logic_vector(511 downto 0);
    tx_block_ready      : std_logic;
end record ECI_BLOCK;

-- power on reset 
signal por_counter : unsigned(4 downto 0) := "00000";
signal reset    : std_logic;

signal clk      : std_logic;
signal clk_gt   : std_logic;

signal link2_txpd           : std_logic_vector(23 downto 0);

signal realign_counter      : integer range 0 to 131071 := 0;
signal realign_counter_int  : unsigned(27 downto 0) := (others => '0');

signal link1, link2 : LINK;
signal link1_il, link2_il : INTERLAKEN;
signal link1_eci, link2_eci : ECI_BLOCK;

signal link_aligned_counter : unsigned(29 downto 0) := to_unsigned(0, 30);

begin

---- Power-on-reset

i_reset_delayer : process(clk)
begin
    if rising_edge(clk) then
        if por_counter < 16 then -- first, deassert reset
            reset <= '0';
            por_counter <= por_counter + 1;
        elsif por_counter < 31 then -- next, assert reset
            reset <= '1';
            por_counter <= por_counter + 1;
        else -- and then deassert reset
            reset <= '0';
        end if;
    end if;
end process;

reset_sys <= reset;

-- The Ultrascale transceiver wizard expects a single-ended reference clock.
-- This instantiates the clock buffer in the transceiver quad - a normal
-- IBUFDS *won't* work.
eci_refclks: for i in 0 to 2 generate
ref_buf_link1 : IBUFDS_GTE4
generic map (
    REFCLK_EN_TX_PATH  => '0',
    REFCLK_HROW_CK_SEL => "00",
    REFCLK_ICNTL_RX    => "00"
)
port map (
    O   => link1.clk_ref(i),
    I   => eci_gt_clk_p_link1(i),
    IB  => eci_gt_clk_n_link1(i),
    CEB => '0'
);

ref_buf_link2 : IBUFDS_GTE4
generic map (
    REFCLK_EN_TX_PATH  => '0',
    REFCLK_HROW_CK_SEL => "00",
    REFCLK_ICNTL_RX    => "00"
)
port map (
    O   => link2.clk_ref(i),
    I   => eci_gt_clk_p_link2(i + 3),
    IB  => eci_gt_clk_n_link2(i + 3),
    CEB => '0'
);
end generate eci_refclks;

-- 322.265625 MHz
i_clk_gt_link1 : BUFG_GT
port map (
    I => link1.txoutclk(5),
    CE => '1',
    CEMASK => '0',
    CLR =>'0',
    CLRMASK => '0',
    DIV => "000",
    O => clk
);

clk_sys <= clk;

-- 161.1328125 MHz
i_clk_gt2_link1 : BUFG_GT
port map (
    I => link1.txoutclk(5),
    CE => '1',
    CEMASK => '0',
    CLR =>'0',
    CLRMASK => '0',
    DIV => "001", -- divide by 2
    O => clk_gt
);

link1.clk_rx <= clk_gt;
link1.clk_tx <= clk_gt;

link1.rxusrclk <= (others => clk);
link1.rxusrclk2 <= (others => clk_gt);
link1.txusrclk <= (others => clk);
link1.txusrclk2 <= (others => clk_gt);

i_userclk_active : xpm_cdc_single
port map (
    src_in => '1',
    src_clk => clk_gt,
    dest_clk => clk_io,
    dest_out => link1.userclk_tx_active_in
);

link1.userclk_rx_active_in <= link1.userclk_tx_active_in;
link2.userclk_tx_active_in <= link1.userclk_tx_active_in;
link2.userclk_rx_active_in <= link1.userclk_tx_active_in;

-- Maximum swing
link1.xcvr_txdiffctrl <= (others => '1');
link1.xcvr_txpostcursor <= (others => '0');
link1.xcvr_txprecursor <= (others => '0');
--link1.xcvr_reset <= reset;

--- Transceivers

xcvr1 : xcvr_link1
port map (
    gtwiz_userclk_tx_active_in(0) => link1.userclk_tx_active_in,
    gtwiz_userclk_rx_active_in(0) => link1.userclk_rx_active_in,
    rxoutclk_out    => link1.rxoutclk,
    rxusrclk_in     => link1.rxusrclk,
    rxusrclk2_in    => link1.rxusrclk2,
    txoutclk_out    => link1.txoutclk,
    txusrclk_in     => link1.txusrclk,
    txusrclk2_in    => link1.txusrclk2,

    gtwiz_buffbypass_rx_reset_in(0)       => '0',
    gtwiz_buffbypass_rx_start_user_in(0)  => '0',
    gtwiz_buffbypass_tx_reset_in(0)       => '0',
    gtwiz_buffbypass_tx_start_user_in(0)  => '0',

    gtwiz_reset_clk_freerun_in(0)         => clk_io,
    gtwiz_reset_all_in(0)                 => '0',
    gtwiz_reset_tx_pll_and_datapath_in(0) => '0',
    gtwiz_reset_tx_datapath_in(0)         => '0',
    gtwiz_reset_rx_pll_and_datapath_in(0) => '0',
    gtwiz_reset_rx_datapath_in(0)         => link1.xcvr_reset_rx,
    gtwiz_reset_tx_done_out(0)            => link1.xcvr_tx_ready,
    gtwiz_reset_rx_done_out(0)            => link1.xcvr_rx_ready,

    gtwiz_userdata_tx_in                  => link1.xcvr_txd,
    gtwiz_userdata_rx_out                 => link1.xcvr_rxd,

    rxgearboxslip_in                      => link1.xcvr_rxgearboxslip,
    rxdatavalid_out                       => link1.xcvr_rxdatavalid,
    rxheader_out                          => link1.xcvr_rxheader,
    rxheadervalid_out                     => link1.xcvr_rxheadervalid,
    rxstartofseq_out                      => link1.xcvr_rxstartofseq,
    txheader_in                           => link1.xcvr_txheader,
    txsequence_in                         => link1.xcvr_txsequence,

    gtrefclk00_in(2)                      => link1.clk_ref(2),
    gtrefclk00_in(1)                      => link1.clk_ref(1),
    gtrefclk00_in(0)                      => link1.clk_ref(0),

    gtyrxn_in                             => eci_gt_rx_n_link1,
    gtyrxp_in                             => eci_gt_rx_p_link1,
    gtytxn_out                            => eci_gt_tx_n_link1,
    gtytxp_out                            => eci_gt_tx_p_link1,

    txdiffctrl_in                         => link1.xcvr_txdiffctrl,
    txpostcursor_in                       => link1.xcvr_txpostcursor,
    txprecursor_in                        => link1.xcvr_txprecursor
);

link2.clk_rx <= clk_gt;
link2.clk_tx <= clk_gt;

link2.rxusrclk <= (others => clk);
link2.rxusrclk2 <= (others => clk_gt);
link2.txusrclk <= (others => clk);
link2.txusrclk2 <= (others => clk_gt);

-- Maximum swing
link2.xcvr_txdiffctrl <= (others => '1');
link2.xcvr_txpostcursor <= (others => '0');
link2.xcvr_txprecursor <= (others => '0');

xcvr2 : xcvr_link2
port map (
    gtwiz_userclk_tx_active_in(0) => link2.userclk_tx_active_in,
    gtwiz_userclk_rx_active_in(0) => link2.userclk_rx_active_in,
    rxoutclk_out    => link2.rxoutclk,
    rxusrclk_in     => link2.rxusrclk,
    rxusrclk2_in    => link2.rxusrclk2,
    txoutclk_out    => link2.txoutclk,
    txusrclk_in     => link2.txusrclk,
    txusrclk2_in    => link2.txusrclk2,

    gtwiz_buffbypass_rx_reset_in(0)       => '0',
    gtwiz_buffbypass_rx_start_user_in(0)  => '0',
    gtwiz_buffbypass_tx_reset_in(0)       => '0',
    gtwiz_buffbypass_tx_start_user_in(0)  => '0',

    gtwiz_reset_clk_freerun_in(0)         => clk_io,
    gtwiz_reset_all_in(0)                 => '0',
    gtwiz_reset_tx_pll_and_datapath_in(0) => '0',
    gtwiz_reset_tx_datapath_in(0)         => '0',
    gtwiz_reset_rx_pll_and_datapath_in(0) => '0',
    gtwiz_reset_rx_datapath_in(0)         => link2.xcvr_reset_rx,
    gtwiz_reset_tx_done_out(0)            => link2.xcvr_tx_ready,
    gtwiz_reset_rx_done_out(0)            => link2.xcvr_rx_ready,

    gtwiz_userdata_tx_in                  => link2.xcvr_txd,
    gtwiz_userdata_rx_out                 => link2.xcvr_rxd,

    rxgearboxslip_in                      => link2.xcvr_rxgearboxslip,
    rxdatavalid_out                       => link2.xcvr_rxdatavalid,
    rxheader_out                          => link2.xcvr_rxheader,
    rxheadervalid_out                     => link2.xcvr_rxheadervalid,
    rxstartofseq_out                      => link2.xcvr_rxstartofseq,
    txheader_in                           => link2.xcvr_txheader,
    txsequence_in                         => link2.xcvr_txsequence,

    gtrefclk00_in(2)                      => link2.clk_ref(2),
    gtrefclk00_in(1)                      => link2.clk_ref(1),
    gtrefclk00_in(0)                      => link2.clk_ref(0),

    gtyrxn_in                             => eci_gt_rx_n_link2,
    gtyrxp_in                             => eci_gt_rx_p_link2,
    gtytxn_out                            => eci_gt_tx_n_link2,
    gtytxp_out                            => eci_gt_tx_p_link2,

    txdiffctrl_in                         => link2.xcvr_txdiffctrl,
    txpostcursor_in                       => link2.xcvr_txpostcursor,
    txprecursor_in                        => link2.xcvr_txprecursor,
    
    txpd_in                               => link2_txpd
);

-- Power down the 2nd link transmitter if if the link is not used
link2_txpd <= (others => '0') when SECOND_LINK_ACTIVE = 1 else (others => '1');

-- Reset the receivers if there's no Interlaken word lock
i_realign_rx : process(clk_gt)
begin
    if rising_edge(clk_gt) then
        link1_il.rx_aligned_old <= link1_il.rx_word_lock;
        link2_il.rx_aligned_old <= link2_il.rx_word_lock;
        if realign_counter = 131071 or (link1_il.rx_aligned_old = '1' and link1_il.rx_word_lock = '0') or
            (SECOND_LINK_ACTIVE = 1 and link2_il.rx_aligned_old = '1' and link2_il.rx_word_lock = '0') then
            if link1_il.rx_word_lock = '0' then
                link1.xcvr_reset_rx <= '1';
            end if;
            if link2_il.rx_word_lock = '0' then
                link2.xcvr_reset_rx <= '1';
            end if;
            realign_counter <= 0;
        elsif link1_il.rx_word_lock = '0' or (SECOND_LINK_ACTIVE = 1 and link2_il.rx_word_lock = '0') then
            realign_counter <= realign_counter + 1;
        else
            realign_counter <= 0;
        end if;
        if link1.xcvr_reset_rx = '1' and link1.xcvr_rx_ready = '0' then
                link1.xcvr_reset_rx <= '0';
        end if;
        if link2.xcvr_reset_rx = '1' and link2.xcvr_rx_ready = '0' then
                link2.xcvr_reset_rx <= '0';
        end if;
        link_aligned_counter <= link_aligned_counter + 1;
    end if;
end process;

link1_il.rx_word_lock <= '1' when link1_il.rx_lane_word_lock = X"fff" else '0';
link1_il.rx_frame_lock <= '1' when link1_il.rx_lane_frame_lock = X"fff" else '0';
link2_il.rx_word_lock <= '1' when link2_il.rx_lane_word_lock = X"fff" else '0';
link2_il.rx_frame_lock <= '1' when link2_il.rx_lane_frame_lock = X"fff" else '0';

-- Link 1
rx_link1_il : il_rx_link_gearbox
generic map (
    LANES     => 12,
    METAFRAME => 2048
)
port map (
    clk_rx          => link1.clk_rx,
    reset           => reset,

    xcvr_rxdata         => link1.xcvr_rxd,
    xcvr_rxdatavalid    => link1.xcvr_rxdatavalid,
    xcvr_rxheader       => link1.xcvr_rxheader,
    xcvr_rxheadervalid  => link1.xcvr_rxheadervalid,
    xcvr_rxgearboxslip  => link1.xcvr_rxgearboxslip,

    output          => link1_il.rx_data,
    output_valid    => link1_il.rx_data_valid,
    ctrl_word_out   => link1_il.rx_ctrl_word,
    link_aligned    => link1_il.rx_aligned,
    lane_word_lock  => link1_il.rx_lane_word_lock,
    lane_frame_lock => link1_il.rx_lane_frame_lock,
    lane_crc32_bad  => link1_il.rx_lane_crc32_bad,
    lane_status     => link1_il.rx_lane_status,
    total_skew      => link1_il.rx_total_skew
);

-- ECI RX Link
rx_eci_blk_link1 : eci_rx_blk
generic map (
    LANES => 12
)
port map (
    clk_rx          => link1.clk_rx,
    clk_blk         => clk,
    link_aligned    => link1_il.rx_aligned,
    link_data       => link1_il.rx_data,
    link_data_valid => link1_il.rx_data_valid,
    ctrl_word       => link1_il.rx_ctrl_word,
    block_out       => link1_eci.rx_block,
    block_out_valid => link1_eci.rx_block_valid,
    crc_match_out   => link1_eci.rx_block_crc_match
);

link1_eci_block_out         <= link1_eci.rx_block;
link1_eci_block_out_valid   <= link1_eci.rx_block_valid;
link1_eci_crc_match_out     <= link1_eci.rx_block_crc_match;

i_link1_eci : eci_link_lite
port map (
    clk              => clk,
    reset            => reset,
    link_up          => link1_eci.link_up,
    blk_rx_data      => link1_eci.rx_block,
    blk_rx_valid     => link1_eci.rx_block_valid,
    blk_crc_match    => link1_eci.rx_block_crc_match,
    blk_tx_data      => link1_eci.tx_block,
    blk_tx_ready     => link1_eci.tx_block_ready,

    mib_vc_mcd              => link1_mib.vc13,
    mib_vc_mcd_valid        => link1_mib.vc13_valid,
    mib_vc_mcd_ready        => link1_mib_ready(13),

    mib_vc_mic              => link1_mib.vc12.data,
    mib_vc_mic_valid        => link1_mib.vc12.valid,
    mib_vc_mic_word_enable  => link1_mib.vc12.word_enable,
    mib_vc_mic_ready        => link1_mib_ready(12),
    
    mib_vc11_co             => link1_mib.vc11.data,
    mib_vc11_co_valid       => link1_mib.vc11.valid,
    mib_vc11_co_word_enable => link1_mib.vc11.word_enable,
    mib_vc11_co_ready       => link1_mib_ready(11),
    
    mib_vc10_co             => link1_mib.vc10.data,
    mib_vc10_co_valid       => link1_mib.vc10.valid,
    mib_vc10_co_word_enable => link1_mib.vc10.word_enable,
    mib_vc10_co_ready       => link1_mib_ready(10),
    
    mib_vc9_co              => link1_mib.vc9.data,
    mib_vc9_co_valid        => link1_mib.vc9.valid,
    mib_vc9_co_word_enable  => link1_mib.vc9.word_enable,
    mib_vc9_co_ready        => link1_mib_ready(9),
    
    mib_vc8_co              => link1_mib.vc8.data,
    mib_vc8_co_valid        => link1_mib.vc8.valid,
    mib_vc8_co_word_enable  => link1_mib.vc8.word_enable,
    mib_vc8_co_ready        => link1_mib_ready(8),

    mib_vc7_co              => link1_mib.vc7.data,
    mib_vc7_co_valid        => link1_mib.vc7.valid,
    mib_vc7_co_word_enable  => link1_mib.vc7.word_enable,
    mib_vc7_co_ready        => link1_mib_ready(7),

    mib_vc6_co              => link1_mib.vc6.data,
    mib_vc6_co_valid        => link1_mib.vc6.valid,
    mib_vc6_co_word_enable  => link1_mib.vc6.word_enable,
    mib_vc6_co_ready        => link1_mib_ready(6),

    -- VC 5 - 2
    mib_vc5_cd              => link1_mib.vc5.data,
    mib_vc5_cd_valid        => link1_mib.vc5.valid,
    mib_vc5_cd_word_enable  => link1_mib.vc5.word_enable,
    mib_vc5_cd_ready        => link1_mib_ready(5),
    
    mib_vc4_cd              => link1_mib.vc4.data,
    mib_vc4_cd_valid        => link1_mib.vc4.valid,
    mib_vc4_cd_word_enable  => link1_mib.vc4.word_enable,
    mib_vc4_cd_ready        => link1_mib_ready(4),
    
    mib_vc3_cd              => link1_mib.vc3.data,
    mib_vc3_cd_valid        => link1_mib.vc3.valid,
    mib_vc3_cd_word_enable  => link1_mib.vc3.word_enable,
    mib_vc3_cd_ready        => link1_mib_ready(3),
    
    mib_vc2_cd              => link1_mib.vc2.data,
    mib_vc2_cd_valid        => link1_mib.vc2.valid,
    mib_vc2_cd_word_enable  => link1_mib.vc2.word_enable,
    mib_vc2_cd_ready        => link1_mib_ready(2),
    
    mib_vc1_io              => link1_mib.vc1.data,
    mib_vc1_io_valid        => link1_mib.vc1.valid,
    mib_vc1_io_word_enable  => link1_mib.vc1.word_enable,
    mib_vc1_io_ready        => link1_mib_ready(1),

    mib_vc0_io              => link1_mib.vc0.data,
    mib_vc0_io_valid        => link1_mib.vc0.valid,
    mib_vc0_io_word_enable  => link1_mib.vc0.word_enable,
    mib_vc0_io_ready        => link1_mib_ready(0),

    -------------------------- MOB VCs Inputs ----------------------------//
    mob_hi_vc               => link1_mob_hi_vc.data,
    mob_hi_vc_no            => link1_mob_hi_vc.no,
    mob_hi_vc_size          => link1_mob_hi_vc.size,
    mob_hi_vc_valid         => link1_mob_hi_vc.valid,
    mob_hi_vc_ready         => link1_mob_hi_vc_ready,
    mob_lo_vc               => link1_mob_lo_vc.data,
    mob_lo_vc_no            => link1_mob_lo_vc.no,
    mob_lo_vc_valid         => link1_mob_lo_vc.valid,
    mob_lo_vc_ready         => link1_mob_lo_vc_ready
);

tx_eci_blk_link1 : eci_tx_blk
generic map (
    LANES => 12
)
port map (
    clk_tx          => link1.clk_tx,
    clk_blk         => clk,
    block_in        => link1_eci.tx_block,
    block_in_ready  => link1_eci.tx_block_ready,
    link_data       => link1_il.tx_data,
    link_data_ready => link1_il.tx_data_ready,
    ctrl_word_out   => link1_il.tx_ctrl_word
);

tx_link1_il : il_tx_link_gearbox
generic map (
    LANES     => 12,
    METAFRAME => 2048
)
port map (
    clk_tx       => link1.clk_tx,
    reset        => reset,
    input        => link1_il.tx_data,
    input_ready  => link1_il.tx_data_ready,
    ctrl_word_in => link1_il.tx_ctrl_word,
    xcvr_txdata  => link1.xcvr_txd,
    xcvr_txheader       => link1.xcvr_txheader,
    xcvr_txsequence     => link1.xcvr_txsequence
);

-- Link 2
i_link2 : if SECOND_LINK_ACTIVE = 1 generate

rx_link2_il : il_rx_link_gearbox
generic map (
    LANES     => 12,
    METAFRAME => 2048
)
port map (
    clk_rx          => link2.clk_rx,
    reset           => reset,

    xcvr_rxdata         => link2.xcvr_rxd,
    xcvr_rxdatavalid    => link2.xcvr_rxdatavalid,
    xcvr_rxheader       => link2.xcvr_rxheader,
    xcvr_rxheadervalid  => link2.xcvr_rxheadervalid,
    xcvr_rxgearboxslip  => link2.xcvr_rxgearboxslip,

    output          => link2_il.rx_data,
    output_valid    => link2_il.rx_data_valid,
    ctrl_word_out   => link2_il.rx_ctrl_word,
    link_aligned    => link2_il.rx_aligned,
    lane_word_lock  => link2_il.rx_lane_word_lock,
    lane_frame_lock => link2_il.rx_lane_frame_lock,
    lane_crc32_bad  => link2_il.rx_lane_crc32_bad,
    lane_status     => link2_il.rx_lane_status,
    total_skew      => link2_il.rx_total_skew
);

rx_eci_blk_link2 : eci_rx_blk
generic map (
    LANES => 12
)
port map (
    clk_rx          => link2.clk_rx,
    clk_blk         => clk,
    link_aligned    => link2_il.rx_aligned,
    link_data       => link2_il.rx_data,
    link_data_valid => link2_il.rx_data_valid,
    ctrl_word       => link2_il.rx_ctrl_word,
    block_out       => link2_eci.rx_block,
    block_out_valid => link2_eci.rx_block_valid,
    crc_match_out   => link2_eci.rx_block_crc_match
);

link2_eci_block_out         <= link2_eci.rx_block;
link2_eci_block_out_valid   <= link2_eci.rx_block_valid;
link2_eci_crc_match_out     <= link2_eci.rx_block_crc_match;

i_link2_eci : eci_link_lite
port map (
    clk              => clk,
    reset            => reset,
    link_up          => link2_eci.link_up,
    blk_rx_data      => link2_eci.rx_block,
    blk_rx_valid     => link2_eci.rx_block_valid,
    blk_crc_match    => link2_eci.rx_block_crc_match,
    blk_tx_data      => link2_eci.tx_block,
    blk_tx_ready     => link2_eci.tx_block_ready,

    mib_vc_mcd              => link2_mib.vc13,
    mib_vc_mcd_valid        => link2_mib.vc13_valid,
    mib_vc_mcd_ready        => link2_mib_ready(13),

    mib_vc_mic              => link2_mib.vc12.data,
    mib_vc_mic_valid        => link2_mib.vc12.valid,
    mib_vc_mic_word_enable  => link2_mib.vc12.word_enable,
    mib_vc_mic_ready        => link2_mib_ready(12),
    
    mib_vc11_co             => link2_mib.vc11.data,
    mib_vc11_co_valid       => link2_mib.vc11.valid,
    mib_vc11_co_word_enable => link2_mib.vc11.word_enable,
    mib_vc11_co_ready       => link2_mib_ready(11),
    
    mib_vc10_co             => link2_mib.vc10.data,
    mib_vc10_co_valid       => link2_mib.vc10.valid,
    mib_vc10_co_word_enable => link2_mib.vc10.word_enable,
    mib_vc10_co_ready       => link2_mib_ready(10),
    
    mib_vc9_co              => link2_mib.vc9.data,
    mib_vc9_co_valid        => link2_mib.vc9.valid,
    mib_vc9_co_word_enable  => link2_mib.vc9.word_enable,
    mib_vc9_co_ready        => link2_mib_ready(9),
    
    mib_vc8_co              => link2_mib.vc8.data,
    mib_vc8_co_valid        => link2_mib.vc8.valid,
    mib_vc8_co_word_enable  => link2_mib.vc8.word_enable,
    mib_vc8_co_ready        => link2_mib_ready(8),

    mib_vc7_co              => link2_mib.vc7.data,
    mib_vc7_co_valid        => link2_mib.vc7.valid,
    mib_vc7_co_word_enable  => link2_mib.vc7.word_enable,
    mib_vc7_co_ready        => link2_mib_ready(7),

    mib_vc6_co              => link2_mib.vc6.data,
    mib_vc6_co_valid        => link2_mib.vc6.valid,
    mib_vc6_co_word_enable  => link2_mib.vc6.word_enable,
    mib_vc6_co_ready        => link2_mib_ready(6),

    -- VC 5 - 2
    mib_vc5_cd              => link2_mib.vc5.data,
    mib_vc5_cd_valid        => link2_mib.vc5.valid,
    mib_vc5_cd_word_enable  => link2_mib.vc5.word_enable,
    mib_vc5_cd_ready        => link2_mib_ready(5),
    
    mib_vc4_cd              => link2_mib.vc4.data,
    mib_vc4_cd_valid        => link2_mib.vc4.valid,
    mib_vc4_cd_word_enable  => link2_mib.vc4.word_enable,
    mib_vc4_cd_ready        => link2_mib_ready(4),
    
    mib_vc3_cd              => link2_mib.vc3.data,
    mib_vc3_cd_valid        => link2_mib.vc3.valid,
    mib_vc3_cd_word_enable  => link2_mib.vc3.word_enable,
    mib_vc3_cd_ready        => link2_mib_ready(3),
    
    mib_vc2_cd              => link2_mib.vc2.data,
    mib_vc2_cd_valid        => link2_mib.vc2.valid,
    mib_vc2_cd_word_enable  => link2_mib.vc2.word_enable,
    mib_vc2_cd_ready        => link2_mib_ready(2),
    
    mib_vc1_io              => link2_mib.vc1.data,
    mib_vc1_io_valid        => link2_mib.vc1.valid,
    mib_vc1_io_word_enable  => link2_mib.vc1.word_enable,
    mib_vc1_io_ready        => link2_mib_ready(1),

    mib_vc0_io              => link2_mib.vc0.data,
    mib_vc0_io_valid        => link2_mib.vc0.valid,
    mib_vc0_io_word_enable  => link2_mib.vc0.word_enable,
    mib_vc0_io_ready        => link2_mib_ready(0),

    -------------------------- MOB VCs Inputs ----------------------------//
    mob_hi_vc               => link2_mob_hi_vc.data,
    mob_hi_vc_no            => link2_mob_hi_vc.no,
    mob_hi_vc_size          => link2_mob_hi_vc.size,
    mob_hi_vc_valid         => link2_mob_hi_vc.valid,
    mob_hi_vc_ready         => link2_mob_hi_vc_ready,
    mob_lo_vc               => link2_mob_lo_vc.data,
    mob_lo_vc_no            => link2_mob_lo_vc.no,
    mob_lo_vc_valid         => link2_mob_lo_vc.valid,
    mob_lo_vc_ready         => link2_mob_lo_vc_ready
);

tx_eci_blk_link2 : eci_tx_blk
generic map (
    LANES => 12
)
port map (
    clk_tx          => link2.clk_tx,
    clk_blk         => clk,
    block_in        => link2_eci.tx_block,
    block_in_ready  => link2_eci.tx_block_ready,
    link_data       => link2_il.tx_data,
    link_data_ready => link2_il.tx_data_ready,
    ctrl_word_out   => link2_il.tx_ctrl_word
);

tx_link2_il : il_tx_link_gearbox
generic map (
    LANES     => 12,
    METAFRAME => 2048
)
port map (
    clk_tx       => link2.clk_tx,
    reset        => reset,
    input        => link2_il.tx_data,
    input_ready  => link2_il.tx_data_ready,
    ctrl_word_in => link2_il.tx_ctrl_word,
    xcvr_txdata  => link2.xcvr_txd,
    xcvr_txheader       => link2.xcvr_txheader,
    xcvr_txsequence     => link2.xcvr_txsequence
);

link_up <= link1_eci.link_up and link2_eci.link_up;
realign_counter_int <= to_unsigned(realign_counter, 28);

end generate i_link2;

end Behavioral;
