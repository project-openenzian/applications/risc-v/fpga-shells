----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2021 20:14:38
-- Design Name: 
-- Module Name: lo_vc_muxer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity lo_vc_muxer is
generic (
    CHANNELS    : integer
);
Port (
    clk         : in STD_LOGIC;
    
    lo_vc       : out std_logic_vector(63 downto 0);
    lo_vc_no    : out std_logic_vector(3 downto 0);
    lo_valid    : out std_logic;
    lo_ready    : in std_logic;

    ch_vc       : in WORDS(CHANNELS - 1 downto 0);
    ch_vc_no    : in VCS(CHANNELS - 1 downto 0);
    ch_valid    : in std_logic_vector(CHANNELS - 1 downto 0);
    ch_ready    : out std_logic_vector(CHANNELS - 1 downto 0)
);
end lo_vc_muxer;

architecture Behavioral of lo_vc_muxer is

signal s            : integer range 0 to (CHANNELS - 1) := 0;
signal ch_active    : integer range -1 to (CHANNELS - 1) := -1;

function find_one(vec : std_logic_vector; start : integer) return integer is
begin    
    for i in 0 to vec'high loop
        if i >= start and vec(i) = '1' then
            return i;
        end if;
    end loop;
    if start > vec'low then  
        for i in vec'low to vec'high loop
            if i < start and vec(i) = '1' then
                return i;
            end if;
        end loop;
    end if;
    return -1;
end function;

begin

ch_active <= find_one(ch_valid, s);
lo_vc <= ch_vc(ch_active) when ch_active /= -1 else (others => 'X');
lo_vc_no <= ch_vc_no(ch_active) when ch_active /= -1 else (others => 'X');
lo_valid <= '1' when unsigned(ch_valid) /= 0 else '0';

gen_ready : for I in 0 to (CHANNELS-1) generate
    ch_ready(I) <= '1' when I = ch_active and lo_ready = '1' else '0';
end generate gen_ready;

i_process : process(clk)
begin
    if rising_edge(clk) then
        if lo_ready = '1' then
            if ch_active /= -1 and ch_active /= CHANNELS - 1 then
                s <= ch_active + 1;
            else
                s <= 0;
            end if; 
        end if;             
    end if;
end process;

end Behavioral;
