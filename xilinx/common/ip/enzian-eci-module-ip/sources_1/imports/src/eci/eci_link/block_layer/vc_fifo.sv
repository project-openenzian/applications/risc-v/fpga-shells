

module vc_fifo #(parameter NUM_WORDS_IN     = 7, 
				 parameter NUM_WORDS_OUT    = 7,
				 parameter WORD_ENABLE_SIZE = 7)
(
	input   wire 								clk,    // Clock
	input   wire 								rst_n,  // Asynchronous reset active low

	input   wire  [63:0] 				        data_word_in[NUM_WORDS_IN-1:0],
    input   wire  		 				        data_word_in_valid,
    input   wire  [WORD_ENABLE_SIZE-1 :0]           data_word_in_word_enable,
    output  wire 								data_word_in_ready,

    output  wire  [63:0] 				        data_word_out[NUM_WORDS_OUT-1:0],
    output  wire       				            data_word_out_valid,
    output  wire  [WORD_ENABLE_SIZE-1 :0] 	        data_word_out_word_enable,
    input   wire  	 							data_word_out_ready,

    output  wire  [8:0] 						vc_fifo_count, 
    output  wire 								vc_fifo_full, 
    output  wire 								vc_fifo_almostfull, 
    output  wire 								vc_fifo_empty
);


wire  [64*NUM_WORDS_IN-1:0]       	fifo_in_data;
wire  [64*NUM_WORDS_OUT-1:0]       	fifo_out_data;


//ila_tlk_fifo i_ila_vc_fifo
//(
//    .clk        (clk),
//    .probe0     (data_word_in[6]),
//    .probe1     (data_word_in[5]),
//    .probe2     (data_word_in_valid),
//    .probe3     (data_word_in_word_enable[2:0]),
//    .probe4     (data_word_in_ready)
//);


genvar i;

generate for (i = 0; i < NUM_WORDS_OUT; i=i+1) begin
	assign fifo_in_data[i*64+63:i*64] = data_word_in[i];
	assign data_word_out[i]           = fifo_out_data[i*64+63:i*64];
end
endgenerate

assign data_word_in_ready = ~vc_fifo_full;

axi_fifo #(.FIFO_WIDTH(64*NUM_WORDS_OUT + WORD_ENABLE_SIZE), 
		   .FIFO_DEPTH_BITS(9), 
		   .FIFO_ALMOSTFULL_THRESHOLD(508) ) 
			axi_fifo_x 
			(
			  	.clk 			(clk), 
			  	.rst_n 			(rst_n), 

			  	.din 			({data_word_in_word_enable, fifo_in_data[64*NUM_WORDS_OUT-1:0]}), 
			  	.we 			(data_word_in_valid), 

			  	.re             (data_word_out_ready), 
			  	.dout 			({data_word_out_word_enable, fifo_out_data}), 
			  	.valid          (data_word_out_valid), 

			  	.count     		(vc_fifo_count), 
			  	.full      		(vc_fifo_full), 
			  	.empty     		(vc_fifo_empty), 
			  	.almostfull     (vc_fifo_almostfull)
			 );

endmodule