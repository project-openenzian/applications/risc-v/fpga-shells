
/*
    The list of FIFOs that store received VC words, and maintain credits. 
*/

import block_types::*;
import eci_package::*;

module rlk_fifos (
	input  wire 										clk,    // Clock
	input  wire 										rst_n,  // Asynchronous reset active low

	input  wire   [63:0]                      			vc_data_word_aligned[13:0][6:0],
    input  wire                                			vc_data_word_valid_aligned[13:0],
    input  wire   [6 :0]                      			vc_data_word_aligned_word_enable[13:0], 

    //---------------- VCs Data Words Received ------------//
    // VC 11 - 6
    output wire   [WORD_WIDTH-1:0]                      vc_co[NUM_CO_VCS-1:0][VC_CO_MAX_SIZE-1:0],              
    output wire                                         vc_co_valid[NUM_CO_VCS-1:0],
    output wire   [VC_CO_MAX_SIZE-1:0]                  vc_co_word_enable[NUM_CO_VCS-1:0],
    input  wire                                         vc_co_ready[NUM_CO_VCS-1:0],
    // VC 5 - 2
    output wire   [WORD_WIDTH-1:0]                      vc_cd[NUM_CD_VCS-1:0][VC_CD_MAX_SIZE-1:0],              
    output wire                                         vc_cd_valid[NUM_CD_VCS-1:0],
    output wire   [VC_CD_MAX_SIZE-1:0]                  vc_cd_word_enable[NUM_CD_VCS-1:0],
    input  wire                                         vc_cd_ready[NUM_CD_VCS-1:0],
    // VC 1 - 0
    output wire   [WORD_WIDTH-1:0]                      vc_io[NUM_IO_VCS-1:0][VC_IO_MAX_SIZE-1:0],              
    output wire                                         vc_io_valid[NUM_IO_VCS-1:0],
    output wire   [VC_IO_MAX_SIZE-1:0]                  vc_io_word_enable[NUM_IO_VCS-1:0],
    input  wire                                         vc_io_ready[NUM_IO_VCS-1:0],
    // MCD VC13
    output wire   [WORD_WIDTH-1:0]                      vc_mcd,              
    output wire                                         vc_mcd_valid,
    input  wire                                         vc_mcd_ready,
    // MIC VC12
    output wire   [WORD_WIDTH-1:0]                      vc_mic[VC_MXC_MAX_SIZE-1:0],              
    output wire                                         vc_mic_valid,
    output wire   [VC_MXC_MAX_SIZE-1:0]                 vc_mic_word_enable,
    input  wire                                         vc_mic_ready, 

    //-------- Credit to Return to Thunderx ---------------//
    // Return credit from FPGA to partner: each set bit represents 8 credits for the corresponding vc
    output reg                                          return_cred[12:0]
);


reg     [4:0]                       vc_ret_credit[12:0];
wire    [4:0]                       poped_words_count[12:0];
reg     [63:0]  vc_mcd_buffer;
reg             vc_mcd_valid_buffer;
wire    [63:0]  vc_mcd_2;
wire            vc_mcd_valid_2;

function [2:0] count_ones;  
    input [6:0] a;
    begin
        count_ones = a[0] + a[1] + a[2] + a[3] + a[4] + a[5] + a[6];  
    end
endfunction

genvar i;
// Level three: VC CO FIFOs
generate for (i = 0; i < NUM_CO_VCS; i=i+1) begin: vc_co_fifos
    vc_fifo #(.NUM_WORDS_IN  (7),
              .NUM_WORDS_OUT (VC_CO_MAX_SIZE) ) 
    vc_co_fifo_x
    ( 
        .clk                     (clk),                      
        .rst_n                   (rst_n), 
        .data_word_in            (vc_data_word_aligned[6+i]),  
        .data_word_in_valid      (vc_data_word_valid_aligned[6+i]),  
        .data_word_in_word_enable       (vc_data_word_aligned_word_enable[6+i]), 
        .data_word_out           (vc_co[i]),  
        .data_word_out_valid     (vc_co_valid[i]),
        .data_word_out_word_enable      (vc_co_word_enable[i]),
        .data_word_out_ready     (vc_co_ready[i])
    );
    assign poped_words_count[6+i] = (vc_co_ready[i] && vc_co_valid[i])? count_ones(vc_co_word_enable[i]) : 5'b0;
end
endgenerate

// Level three: VC CD FIFOs
generate for (i = 0; i < NUM_CD_VCS; i=i+1) begin: vc_cd_fifos
    vc_fifo #(.NUM_WORDS_IN  (7),
              .NUM_WORDS_OUT (VC_CD_MAX_SIZE) ) 
    vc_cd_fifo_x
    ( 
        .clk                     (clk),                      
        .rst_n                   (rst_n), 
        .data_word_in            (vc_data_word_aligned[2+i]),  
        .data_word_in_valid      (vc_data_word_valid_aligned[2+i]),  
        .data_word_in_word_enable (vc_data_word_aligned_word_enable[2+i]), 
        .data_word_out           (vc_cd[i]),  
        .data_word_out_valid     (vc_cd_valid[i]),
        .data_word_out_word_enable      (vc_cd_word_enable[i]),
        .data_word_out_ready     (vc_cd_ready[i])
    );
    assign poped_words_count[2+i] = (vc_cd_ready[i] && vc_cd_valid[i])? count_ones(vc_cd_word_enable[i]) : 5'b0;
end
endgenerate

// Level three: VC IO FIFOs
generate for (i = 0; i < NUM_IO_VCS; i=i+1) begin: vc_io_fifos
    vc_fifo #(.NUM_WORDS_IN  (7),
              .NUM_WORDS_OUT (VC_IO_MAX_SIZE) ) 
    vc_io_fifo_x
    ( 
        .clk                     (clk),                      
        .rst_n                   (rst_n), 
        .data_word_in            (vc_data_word_aligned[i]),  
        .data_word_in_valid      (vc_data_word_valid_aligned[i]),  
        .data_word_in_word_enable (vc_data_word_aligned_word_enable[i]), 
        .data_word_out           (vc_io[i]),  
        .data_word_out_valid     (vc_io_valid[i]),
        .data_word_out_word_enable      (vc_io_word_enable[i]), 
        .data_word_out_ready     (vc_io_ready[i])
    );
    assign poped_words_count[i] = (vc_io_ready[i] && vc_io_valid[i])? count_ones(vc_io_word_enable[i]) : 5'b0;
end
endgenerate

// Level three: VC MXC FIFOs
vc_fifo #(.NUM_WORDS_IN  (7),
          .NUM_WORDS_OUT (VC_MXC_MAX_SIZE) ) 
    vc_mxc_fifo_x
    ( 
        .clk                     (clk),                      
        .rst_n                   (rst_n), 
        .data_word_in            (vc_data_word_aligned[12]),  
        .data_word_in_valid      (vc_data_word_valid_aligned[12]),  
        .data_word_in_word_enable       (vc_data_word_aligned_word_enable[12]), 
        .data_word_out           (vc_mic),  
        .data_word_out_valid     (vc_mic_valid),
        .data_word_out_word_enable      (vc_mic_word_enable), 
        .data_word_out_ready     (vc_mic_ready)
    );

 assign poped_words_count[12] = (vc_mic_ready && vc_mic_valid)? count_ones(vc_mic_word_enable) : 5'b0;

vc_word_extractor mcd_word
(
    .clk    (clk),
    .input_words     (vc_data_word_aligned[13]),
    .input_valid     (vc_data_word_valid_aligned[13]),
    .input_word_enable   (vc_data_word_aligned_word_enable[13]),
    .output_word     (vc_mcd_2),
    .output_valid    (vc_mcd_valid_2),
    .output_ready    (1'h1)
);

//////////////////////////////////////////////////////////////////////////
//////////////////////////////     Return Credit     /////////////////////
//////////////////////////////////////////////////////////////////////////

generate for (i = 0; i < 13; i=i+1) begin: ret_cred

    always@(posedge clk) begin 
        if(~rst_n) begin
            vc_ret_credit[i] <= 5'b0;
            return_cred[i]   <= 1'b0;
        end 
        else begin 
            vc_ret_credit[i] <= vc_ret_credit[i] - ({5{return_cred[i]}} & 5'b01000) + poped_words_count[i];

            return_cred[i]   <= |vc_ret_credit[i][4:3] && ~return_cred[i];
        end
    end 
end 
endgenerate

assign vc_mcd = vc_mcd_buffer;
assign vc_mcd_valid = vc_mcd_valid_buffer;
 
always@(posedge clk) begin 
    if (vc_mcd_valid_2) begin
        vc_mcd_buffer <= vc_mcd_2;
        vc_mcd_valid_buffer <= 1'b1;
    end
    if (vc_mcd_valid_buffer && vc_mcd_ready) begin
        vc_mcd_valid_buffer <= 1'b0;
    end
end

//ila_vc0 i_ila_vc0
//(
//    .clk     (clk), 
//    .probe0  ({vc_data_word_aligned[0][6], vc_data_word_aligned[0][5], vc_data_word_aligned[0][4], vc_data_word_aligned[0][3], vc_data_word_aligned[0][2], vc_data_word_aligned[0][1], vc_data_word_aligned[0][0]}),
//    .probe1  (vc_data_word_valid_aligned[0]),
//    .probe2  (vc_data_word_aligned_word_enable[0]),
//    .probe3  ({vc_io[0][6], vc_io[0][5], vc_io[0][4], vc_io[0][3], vc_io[0][2], vc_io[0][1], vc_io[0][0]}),
//    .probe4  (vc_io_valid[0]),
//    .probe5  (vc_io_word_enable[0]),
//    .probe6  (vc_io_ready[0])
//);

//ila_vc13 i_ila_vc13
//(
//    .clk     (clk), 
//    .probe0  ({vc_data_word_aligned[13][6], vc_data_word_aligned[13][5], vc_data_word_aligned[13][4], vc_data_word_aligned[13][3], vc_data_word_aligned[13][2], vc_data_word_aligned[13][1], vc_data_word_aligned[13][0]}),
//    .probe1  (vc_data_word_valid_aligned[13]),
//    .probe2  (vc_data_word_aligned_word_enable[13]),
//    .probe3  (vc_mcd_2),
//    .probe4  (vc_mcd_valid_2),
//    .probe5  (vc_mcd),
//    .probe6  (vc_mcd_valid)
//);

endmodule