----------------------------------------------------------------------------------
-- Company: ETH Zurich
-- Engineer: Adam S. Turowski
-- 
-- Create Date: 06/22/2022 04:41:13 PM
-- Design Name: 
-- Module Name: eci_module - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: ECI module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity eci_module is
port (
    clk_io              : in std_logic;
    clk_sys             : out std_logic;
    reset_sys           : out std_logic;

    -- 156.25MHz transceiver reference clocks
    eci_gt_clk_p_link1 : in std_logic_vector(2 downto 0);
    eci_gt_clk_n_link1 : in std_logic_vector(2 downto 0);

    eci_gt_clk_p_link2 : in std_logic_vector(5 downto 3);
    eci_gt_clk_n_link2 : in std_logic_vector(5 downto 3);

    -- RX differential pairs
    eci_gt_rx_p_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_p_link2    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link2    : in std_logic_vector(11 downto 0);

    -- TX differential pairs
    eci_gt_tx_p_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_p_link2    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link2    : out std_logic_vector(11 downto 0);

    link_up             : out std_logic;

    -- AXI Lite master interface IO addr space
    m_io_axil_awaddr    : out std_logic_vector(39 downto 0);
    m_io_axil_awvalid   : out std_logic;
    m_io_axil_awready   : in std_logic;

    m_io_axil_wdata     : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb     : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid    : out std_logic;
    m_io_axil_wready    : in std_logic;

    m_io_axil_bresp     : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid    : in std_logic;
    m_io_axil_bready    : out std_logic;

    m_io_axil_araddr    : out std_logic_vector(39 downto 0);
    m_io_axil_arvalid   : out std_logic;
    m_io_axil_arready   : in std_logic;

    m_io_axil_rdata     : in std_logic_vector(63 downto 0);
    m_io_axil_rresp     : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid    : in std_logic;
    m_io_axil_rready    : out std_logic
);
end eci_module;

architecture Behavioral of eci_module is

component eci_layer is
generic (
    SECOND_LINK_ACTIVE : integer := 1
);
port (
    clk_io          : in std_logic;
    clk_sys         : out std_logic;
    reset_sys       : out std_logic;

    -- 156.25MHz transceiver reference clocks
    eci_gt_clk_p_link1 : in std_logic_vector(2 downto 0);
    eci_gt_clk_n_link1 : in std_logic_vector(2 downto 0);

    eci_gt_clk_p_link2 : in std_logic_vector(5 downto 3);
    eci_gt_clk_n_link2 : in std_logic_vector(5 downto 3);

    -- RX differential pairs
    eci_gt_rx_p_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link1    : in std_logic_vector(11 downto 0);
    eci_gt_rx_p_link2    : in std_logic_vector(11 downto 0);
    eci_gt_rx_n_link2    : in std_logic_vector(11 downto 0);

    -- TX differential pairs
    eci_gt_tx_p_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link1    : out std_logic_vector(11 downto 0);
    eci_gt_tx_p_link2    : out std_logic_vector(11 downto 0);
    eci_gt_tx_n_link2    : out std_logic_vector(11 downto 0);

    link1_mib               : out MIB_VCS;
    link1_mib_ready         : in std_logic_vector(13 downto 0);
    link1_mob_hi_vc         : in MOB_HI_VC;
    link1_mob_hi_vc_ready   : out STD_LOGIC;
    link1_mob_lo_vc         : in MOB_LO_VC;
    link1_mob_lo_vc_ready   : out STD_LOGIC;

    link2_mib               : out MIB_VCS;
    link2_mib_ready         : in std_logic_vector(13 downto 0);
    link2_mob_hi_vc         : in MOB_HI_VC;
    link2_mob_hi_vc_ready   : out STD_LOGIC;
    link2_mob_lo_vc         : in MOB_LO_VC;
    link2_mob_lo_vc_ready   : out STD_LOGIC;
    
    link_up                 : out std_logic
);
end component;

component eci_io_bridge_lite is
generic (
    SECOND_LINK_ACTIVE : integer
);
port (
    clk : in std_logic;
    reset : in std_logic;

    -- Link 1 interface
    link1_mib_vc_mcd        : in std_logic_vector(63 downto 0);
    link1_mib_vc_mcd_valid  : in std_logic;
    link1_mib_vc_mcd_ready  : out  std_logic;

    link1_mib_vc0_io         : in WORDS(6 downto 0);
    link1_mib_vc0_io_valid   : in std_logic;
    link1_mib_vc0_io_word_enable    : in std_logic_vector(6 downto 0);
    link1_mib_vc0_io_ready   : out std_logic;

    link1_mib_vc1_io         : in WORDS(6 downto 0);
    link1_mib_vc1_io_valid   : in std_logic;
    link1_mib_vc1_io_word_enable    : in std_logic_vector(6 downto 0);
    link1_mib_vc1_io_ready   : out std_logic;

    link1_mob_lo_vc           : out std_logic_vector(63 downto 0);
    link1_mob_lo_vc_no        : out std_logic_vector(3 downto 0);
    link1_mob_lo_vc_valid     : out STD_LOGIC;
    link1_mob_lo_vc_ready     : in STD_LOGIC;

    -- Link 2 interface
    link2_mib_vc_mcd        : in std_logic_vector(63 downto 0);
    link2_mib_vc_mcd_valid  : in std_logic;
    link2_mib_vc_mcd_ready  : out  std_logic;

    link2_mib_vc0_io         : in WORDS(6 downto 0);
    link2_mib_vc0_io_valid   : in std_logic;
    link2_mib_vc0_io_word_enable    : in std_logic_vector(6 downto 0);
    link2_mib_vc0_io_ready   : out std_logic;

    link2_mib_vc1_io         : in WORDS(6 downto 0);
    link2_mib_vc1_io_valid   : in std_logic;
    link2_mib_vc1_io_word_enable    : in std_logic_vector(6 downto 0);
    link2_mib_vc1_io_ready   : out std_logic;

    link2_mob_lo_vc           : out std_logic_vector(63 downto 0);
    link2_mob_lo_vc_no        : out std_logic_vector(3 downto 0);
    link2_mob_lo_vc_valid     : out STD_LOGIC;
    link2_mob_lo_vc_ready     : in STD_LOGIC;

    -- AXI Lite master interface IO addr space
    m_io_axil_awaddr  : out std_logic_vector(39 downto 0);
    m_io_axil_awvalid : buffer std_logic;
    m_io_axil_awready : in  std_logic;

    m_io_axil_wdata   : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb   : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid  : buffer std_logic;
    m_io_axil_wready  : in  std_logic;

    m_io_axil_bresp   : in  std_logic_vector(1 downto 0);
    m_io_axil_bvalid  : in  std_logic;
    m_io_axil_bready  : buffer std_logic;

    m_io_axil_araddr  : out std_logic_vector(39 downto 0);
    m_io_axil_arvalid : buffer std_logic;
    m_io_axil_arready : in  std_logic;

    m_io_axil_rdata   : in std_logic_vector(63 downto 0);
    m_io_axil_rresp   : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid  : in std_logic;
    m_io_axil_rready  : buffer std_logic
);
end component;

component lo_vc_muxer
generic (
    CHANNELS    : integer
);
Port (
    clk         : in STD_LOGIC;
    
    lo_vc       : out std_logic_vector(63 downto 0);
    lo_vc_no    : out std_logic_vector(3 downto 0);
    lo_valid    : out std_logic;
    lo_ready    : in std_logic;

    ch_vc       : in WORDS(CHANNELS - 1 downto 0);
    ch_vc_no    : in VCS(CHANNELS - 1 downto 0);
    ch_valid    : in std_logic_vector(CHANNELS - 1 downto 0);
    ch_ready    : out std_logic_vector(CHANNELS - 1 downto 0)
);
end component;

component eci_packet_rx_mux is
generic (
    WIDTH           : integer
);
port (
    clk             : in std_logic;
    link1_data      : in std_logic_vector(WIDTH-1 downto 0);
    link1_valid     : in std_logic;
    link1_ready     : out std_logic;
    link2_data      : in std_logic_vector(WIDTH-1 downto 0);
    link2_valid     : in std_logic;
    link2_ready     : out std_logic;
    output_data     : out std_logic_vector(WIDTH-1 downto 0);
    output_valid    : out std_logic;
    output_ready    : in std_logic
);
end component eci_packet_rx_mux;

-- Module takes data from VCs, deserializes it and generates ECI packets
-- corresponding to events in the VCs
component vc_eci_pkt_router is
port (
    clk   : in std_logic;
    reset : in std_logic;

    -- Incoming CPU to FPGA events (MIB VCs) c_ -> CPU initated events
    -- VC11 to VC2 (VC1, VC0) are IO VCs not handled here
    -- Incoming response without data VC 11,10
    c11_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c11_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c11_vc_pkt_valid_i : in  std_logic;
    c11_vc_pkt_ready_o : out std_logic;

    c10_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c10_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c10_vc_pkt_valid_i : in  std_logic;
    c10_vc_pkt_ready_o : out std_logic;

    -- Incoming Forwards VC 9,8
    -- Forwards are command only no data, currently not connected
    c9_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c9_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c9_vc_pkt_valid_i : in  std_logic;
    c9_vc_pkt_ready_o : out std_logic;

    c8_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c8_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c8_vc_pkt_valid_i : in  std_logic;
    c8_vc_pkt_ready_o : out std_logic;

   -- Incoming requests without data VC 7,6
    c7_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c7_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c7_vc_pkt_valid_i : in  std_logic;
    c7_vc_pkt_ready_o : out std_logic;

    c6_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c6_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c6_vc_pkt_valid_i : in  std_logic;
    c6_vc_pkt_ready_o : out std_logic;

    -- Incoming resoponse with data VC 5,4
    c5_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c5_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c5_vc_pkt_valid_i : in  std_logic;
    c5_vc_pkt_ready_o : out std_logic;

    c4_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c4_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c4_vc_pkt_valid_i : in  std_logic;
    c4_vc_pkt_ready_o : out std_logic;

    -- Incoming request with data VC 3,2
    c3_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c3_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c3_vc_pkt_valid_i : in  std_logic;
    c3_vc_pkt_ready_o : out std_logic;

    c2_vc_pkt_i       : in  std_logic_vector(447 downto 0);
    c2_vc_pkt_word_enable_i  : in  std_logic_vector(  6 downto 0);
    c2_vc_pkt_valid_i : in  std_logic;
    c2_vc_pkt_ready_o : out std_logic;

    -- ECI packets corresponding to the VCs holding CPU initiated events
    -- ECI packets for VC 11 - 2 (VC1,0 are IO VCs not handled here)

    -- ECI packet for CPU initiated response without data VC 11,10
    -- No payload only header
    c11_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c11_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c11_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c11_eci_pkt_valid_o : out std_logic;
    c11_eci_pkt_ready_i : in  std_logic;

    c10_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c10_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c10_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c10_eci_pkt_valid_o : out std_logic;
    c10_eci_pkt_ready_i : in  std_logic;

    -- ECI packet for CPU initiated forwards VC 9,8
    -- No payload only header
    c9_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c9_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c9_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c9_eci_pkt_valid_o : out std_logic;
    c9_eci_pkt_ready_i : in  std_logic;

    c8_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c8_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c8_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c8_eci_pkt_valid_o : out std_logic;
    c8_eci_pkt_ready_i : in  std_logic;

    -- ECI packet for CPU initiated request without data VC 7,6
    -- No payload only header
    c7_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c7_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c7_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c7_eci_pkt_valid_o : out std_logic;
    c7_eci_pkt_ready_i : in  std_logic;

    c6_eci_hdr_o       : out std_logic_vector(63 downto 0);
    c6_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c6_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c6_eci_pkt_valid_o : out std_logic;
    c6_eci_pkt_ready_i : in  std_logic;

    -- ECI packet for CPU initiated response with data VC 5,4
    -- Header + payload
    c5_eci_pkt_o       : out WORDS(16 downto 0);
    c5_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c5_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c5_eci_pkt_valid_o : out std_logic;
    c5_eci_pkt_ready_i : in  std_logic;

    c4_eci_pkt_o       : out WORDS(16 downto 0);
    c4_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c4_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c4_eci_pkt_valid_o : out std_logic;
    c4_eci_pkt_ready_i : in  std_logic;

    -- ECI packet for CPU initiated requests with data VC 3,2
    c3_eci_pkt_o       : out std_logic_vector(17*64-1 downto 0);
    c3_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c3_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c3_eci_pkt_valid_o : out std_logic;
    c3_eci_pkt_ready_i : in  std_logic;

    c2_eci_pkt_o       : out std_logic_vector(17*64-1 downto 0);
    c2_eci_pkt_size_o  : out std_logic_vector( 4 downto 0);
    c2_eci_pkt_vc_o    : out std_logic_vector( 4 downto 0);
    c2_eci_pkt_valid_o : out std_logic;
    c2_eci_pkt_ready_i : in  std_logic;

    -- VC 1,0 are IO VCs, not handled here

    -- Special Handlers
    -- GSYNC from VC7,6 (req without data)
    c7_gsync_hdr_o   : out std_logic_vector(63 downto 0);
    c7_gsync_valid_o : out std_logic;
    c7_gsync_ready_i : in  std_logic;

    c6_gsync_hdr_o   : out std_logic_vector(63 downto 0);
    c6_gsync_valid_o : out std_logic;
    c6_gsync_ready_i : in  std_logic
);
end component;

component eci_pkt_vc_router is
port (
   clk   : in std_logic;
   reset : in std_logic;

   -- Input ECI packets to corresponding VCs from dir controller
   -- VC11 - VC2 (VC1,0 are not handled here)

   -- ECI packet Outgoing response without data VC 11,10
   -- Note: no data sends only header
   dc11_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc11_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc11_eci_pkt_valid_i : in  std_logic;
   dc11_eci_pkt_ready_o : out std_logic;

   dc10_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc10_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc10_eci_pkt_valid_i : in  std_logic;
   dc10_eci_pkt_ready_o : out std_logic;

   -- ECI packet outgoing forwareds without data VC 9,8
   dc9_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc9_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc9_eci_pkt_valid_i : in  std_logic;
   dc9_eci_pkt_ready_o : out std_logic;

   dc8_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc8_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc8_eci_pkt_valid_i : in  std_logic;
   dc8_eci_pkt_ready_o : out std_logic;

   -- ECI packet outgoing requests without data VC 7,6
   dc7_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc7_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc7_eci_pkt_valid_i : in  std_logic;
   dc7_eci_pkt_ready_o : out std_logic;

   dc6_eci_hdr_i       : in  std_logic_vector(63 downto 0);
   dc6_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc6_eci_pkt_valid_i : in  std_logic;
   dc6_eci_pkt_ready_o : out std_logic;

   -- ECI packet outgoing responses with data VC 5,4
   -- header+payload
   dc5_eci_pkt_i       : in  WORDS(16 downto 0);
   dc5_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc5_eci_pkt_valid_i : in  std_logic;
   dc5_eci_pkt_ready_o : out std_logic;

   dc4_eci_pkt_i       : in  WORDS(16 downto 0);
   dc4_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc4_eci_pkt_valid_i : in  std_logic;
   dc4_eci_pkt_ready_o : out std_logic;

   -- ECI packet outgoing requests with data VC 3,2
   -- header+payload
   dc3_eci_pkt_i       : in  std_logic_vector(17*64-1 downto 0);
   dc3_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc3_eci_pkt_valid_i : in  std_logic;
   dc3_eci_pkt_ready_o : out std_logic;

   dc2_eci_pkt_i       : in  std_logic_vector(17*64-1 downto 0);
   dc2_eci_pkt_size_i  : in  std_logic_vector( 4 downto 0);
   dc2_eci_pkt_valid_i : in  std_logic;
   dc2_eci_pkt_ready_o : out std_logic;

   -- Input ECI packets from special handlers
   -- GSDN for GSYNC
   gsdn11_hdr_i   : in  std_logic_vector(63 downto 0);
   gsdn11_valid_i : in  std_logic;
   gsdn11_ready_o : out std_logic;

   gsdn10_hdr_i   : in  std_logic_vector(63 downto 0);
   gsdn10_valid_i : in  std_logic;
   gsdn10_ready_o : out std_logic;

   -- Output VC packets generated by the FPGA
   -- VC11 - VC2 (VC1,0 are not handled here)

   -- VC packet Outgoing response without data VC 11,10
   -- Note: no data sends only header
   f11_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f11_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f11_vc_pkt_valid_o : out std_logic;
   f11_vc_pkt_ready_i : in  std_logic;

   f10_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f10_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f10_vc_pkt_valid_o : out std_logic;
   f10_vc_pkt_ready_i : in  std_logic;

   -- Incoming Forwards VC 9,8
   -- Forwards are command only no data, currently not connected
   f9_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f9_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f9_vc_pkt_valid_o : out std_logic;
   f9_vc_pkt_ready_i : in  std_logic;

   f8_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f8_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f8_vc_pkt_valid_o : out std_logic;
   f8_vc_pkt_ready_i : in  std_logic;

   -- Incoming requests without data VC 7,6
   f7_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f7_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f7_vc_pkt_valid_o : out std_logic;
   f7_vc_pkt_ready_i : in  std_logic;

   f6_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f6_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f6_vc_pkt_valid_o : out std_logic;
   f6_vc_pkt_ready_i : in  std_logic;

   -- Incoming resoponse with data VC 5,4
   f5_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f5_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f5_vc_pkt_valid_o : out std_logic;
   f5_vc_pkt_ready_i : in  std_logic;

   f4_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f4_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f4_vc_pkt_valid_o : out std_logic;
   f4_vc_pkt_ready_i : in  std_logic;

   -- Incoming request with data VC 3,2
   f3_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f3_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f3_vc_pkt_valid_o : out std_logic;
   f3_vc_pkt_ready_i : in  std_logic;

   f2_vc_pkt_o       : out std_logic_vector(447 downto 0);
   f2_vc_pkt_size_o  : out std_logic_vector(  2 downto 0);
   f2_vc_pkt_valid_o : out std_logic;
   f2_vc_pkt_ready_i : in  std_logic
);
end component;

component loopback_vc_resp_nodata is
generic (
   WORD_WIDTH : integer;
   GSDN_GSYNC_FN : integer
);
port (
    clk, reset : in std_logic;

    -- ECI Request input stream
    vc_req_i       : in  std_logic_vector(WORD_WIDTH-1 downto 0);
    vc_req_valid_i : in  std_logic;
    vc_req_ready_o : out std_logic;

    -- ECI Response output stream
    vc_resp_o       : out std_logic_vector(WORD_WIDTH-1 downto 0);
    vc_resp_valid_o : out std_logic;
    vc_resp_ready_i : in  std_logic
);
end component;

-------------------------------------------------------------------------------------------------------------
-- RECORDS

type AXI_LITE is record
    araddr  : std_logic_vector(31 downto 0);
    arprot  : std_logic_vector( 2 downto 0);
    arready : std_logic;
    arvalid : std_logic;
    awaddr  : std_logic_vector(31 downto 0);
    awprot  : std_logic_vector( 2 downto 0);
    awready : std_logic;
    awvalid : std_logic;
    bready  : std_logic;
    bresp   : std_logic_vector( 1 downto 0);
    bvalid  : std_logic;
    rdata   : std_logic_vector(31 downto 0);
    rready  : std_logic;
    rresp   : std_logic_vector( 1 downto 0);
    rvalid  : std_logic;
    wdata   : std_logic_vector(31 downto 0);
    wready  : std_logic;
    wstrb   : std_logic_vector( 3 downto 0);
    wvalid  : std_logic;
end record AXI_LITE;

type IO_AXI_LITE is record
    awaddr  :  std_logic_vector(39 downto 0);
    awvalid :  std_logic;
    awready :  std_logic;
    wdata   :  std_logic_vector(63 downto 0);
    wstrb   :  std_logic_vector(7 downto 0);
    wvalid  :  std_logic;
    wready  :  std_logic;
    bresp   :  std_logic_vector(1 downto 0);
    bvalid  :  std_logic;
    bready  :  std_logic;
    araddr  :  std_logic_vector(39 downto 0);
    arvalid :  std_logic;
    arready :  std_logic;
    rdata   :  std_logic_vector(63 downto 0);
    rresp   :  std_logic_vector(1 downto 0);
    rvalid  :  std_logic;
    rready  :  std_logic;
end record IO_AXI_LITE;

type MIB is record
    vc          : MIB_VCS;
    vc_ready    : std_logic_vector(13 downto 0);
end record MIB;

type MOB is record
    hi_vc       : MOB_HI_VC;
    lo_vc       : MOB_LO_VC;
    hi_vc_ready : std_logic;
    lo_vc_ready : std_logic;
end record MOB;

-- DDR4 types

type ECI_PACKET_RX is record
    c7_gsync_hdr_rx   : std_logic_vector(63 downto 0);
    c7_gsync_valid_rx : std_logic;
    c7_gsync_ready_rx : std_logic;

    c6_gsync_hdr_rx   : std_logic_vector(63 downto 0);
    c6_gsync_valid_rx : std_logic;
    c6_gsync_ready_rx : std_logic;
end record ECI_PACKET_RX;

type ECI_PACKET_TX is record
-- VC packets inputs, from the ThunderX
-- GSYNC packets to be looped back
    c11_gsync_hdr_tx   : std_logic_vector(63 downto 0);
    c11_gsync_valid_tx : std_logic;
    c11_gsync_ready_tx : std_logic;

    c10_gsync_hdr_tx   : std_logic_vector(63 downto 0);
    c10_gsync_valid_tx : std_logic;
    c10_gsync_ready_tx : std_logic;
end record ECI_PACKET_TX;

-- Even block AXI Lite signals (clk)
type IO_AXI_LITE_WRITE is record
    awaddr  :  std_logic_vector(34 downto 0);
    awvalid :  std_logic;
    awready :  std_logic;
    wdata   :  std_logic_vector(63 downto 0);
    wstrb   :  std_logic_vector(7 downto 0);
    wvalid  :  std_logic;
    wready  :  std_logic;
    bresp   :  std_logic_vector(1 downto 0);
    bvalid  :  std_logic;
    bready  :  std_logic;
end record IO_AXI_LITE_WRITE;

-- Single-ended, buffered clocks.
signal clk : std_logic;

-- AXI Busses
signal AXI_aresetn : std_logic;

signal io_axil_link     : IO_AXI_LITE;

signal link1_mib : MIB;
signal link2_mib : MIB;

signal link1_mob : MOB;
signal link2_mob : MOB;

signal link1_io_vc       : std_logic_vector(63 downto 0);
signal link1_io_vc_no    : std_logic_vector(3 downto 0);
signal link1_io_valid    : std_logic;
signal link1_io_ready    : std_logic;

signal link2_io_vc       : std_logic_vector(63 downto 0);
signal link2_io_vc_no    : std_logic_vector(3 downto 0);
signal link2_io_valid    : std_logic;
signal link2_io_ready    : std_logic;

signal reset            : std_logic;
signal eci_link_up      : std_logic;

signal link_eci_packet_rx, link1_eci_packet_rx, link2_eci_packet_rx : ECI_PACKET_RX;
signal link_eci_packet_tx : ECI_PACKET_TX;

begin

clk_sys <= clk;
reset_sys <= reset;

i_eci_layer : eci_layer
port map (
    clk_io          => clk_io,
    clk_sys         => clk,
    reset_sys       => reset,

    eci_gt_clk_p_link1 => eci_gt_clk_p_link1,
    eci_gt_clk_n_link1 => eci_gt_clk_n_link1,

    eci_gt_clk_p_link2 => eci_gt_clk_p_link2,
    eci_gt_clk_n_link2 => eci_gt_clk_n_link2,

    eci_gt_rx_p_link1    => eci_gt_rx_p_link1,
    eci_gt_rx_n_link1    => eci_gt_rx_n_link1,
    eci_gt_rx_p_link2    => eci_gt_rx_p_link2,
    eci_gt_rx_n_link2    => eci_gt_rx_n_link2,

    eci_gt_tx_p_link1    => eci_gt_tx_p_link1,
    eci_gt_tx_n_link1    => eci_gt_tx_n_link1,
    eci_gt_tx_p_link2    => eci_gt_tx_p_link2,
    eci_gt_tx_n_link2    => eci_gt_tx_n_link2,

    link1_mib               => link1_mib.vc,
    link1_mib_ready         => link1_mib.vc_ready,
    link1_mob_hi_vc         => link1_mob.hi_vc,
    link1_mob_hi_vc_ready   => link1_mob.hi_vc_ready,
    link1_mob_lo_vc         => link1_mob.lo_vc,
    link1_mob_lo_vc_ready   => link1_mob.lo_vc_ready,

    link2_mib               => link2_mib.vc,
    link2_mib_ready         => link2_mib.vc_ready,
    link2_mob_hi_vc         => link2_mob.hi_vc,
    link2_mob_hi_vc_ready   => link2_mob.hi_vc_ready,
    link2_mob_lo_vc         => link2_mob.lo_vc,
    link2_mob_lo_vc_ready   => link2_mob.lo_vc_ready,
    
    link_up                 => eci_link_up
);

i_eci_io_bridge : eci_io_bridge_lite
generic map (
    SECOND_LINK_ACTIVE => 1
)
port map (
    clk => clk,
    reset => reset,

    link1_mib_vc_mcd        => link1_mib.vc.vc13,
    link1_mib_vc_mcd_valid  => link1_mib.vc.vc13_valid,
    link1_mib_vc_mcd_ready  => link1_mib.vc_ready(13),

    link1_mib_vc0_io         => link1_mib.vc.vc0.data,
    link1_mib_vc0_io_valid   => link1_mib.vc.vc0.valid,
    link1_mib_vc0_io_word_enable    => link1_mib.vc.vc0.word_enable,
    link1_mib_vc0_io_ready   => link1_mib.vc_ready(0),

    link1_mib_vc1_io         => link1_mib.vc.vc1.data,
    link1_mib_vc1_io_valid   => link1_mib.vc.vc1.valid,
    link1_mib_vc1_io_word_enable    => link1_mib.vc.vc1.word_enable,
    link1_mib_vc1_io_ready   => link1_mib.vc_ready(1),

    link1_mob_lo_vc         => link1_io_vc,
    link1_mob_lo_vc_no      => link1_io_vc_no,
    link1_mob_lo_vc_valid   => link1_io_valid,
    link1_mob_lo_vc_ready   => link1_io_ready,

    -- Link 2 interface
    link2_mib_vc_mcd        => link2_mib.vc.vc13,
    link2_mib_vc_mcd_valid  => link2_mib.vc.vc13_valid,
    link2_mib_vc_mcd_ready  => link2_mib.vc_ready(13),

    link2_mib_vc0_io         => link2_mib.vc.vc0.data,
    link2_mib_vc0_io_valid   => link2_mib.vc.vc0.valid,
    link2_mib_vc0_io_word_enable    => link2_mib.vc.vc0.word_enable,
    link2_mib_vc0_io_ready   => link2_mib.vc_ready(0),

    link2_mib_vc1_io         => link2_mib.vc.vc1.data,
    link2_mib_vc1_io_valid   => link2_mib.vc.vc1.valid,
    link2_mib_vc1_io_word_enable    => link2_mib.vc.vc1.word_enable,
    link2_mib_vc1_io_ready   => link2_mib.vc_ready(1),

    link2_mob_lo_vc         => link2_io_vc,
    link2_mob_lo_vc_no      => link2_io_vc_no,
    link2_mob_lo_vc_valid   => link2_io_valid,
    link2_mob_lo_vc_ready   => link2_io_ready,

    -- AXI Lite master interface for IO addr space
    m_io_axil_awaddr  => io_axil_link.awaddr,
    m_io_axil_awvalid => io_axil_link.awvalid,
    m_io_axil_awready => io_axil_link.awready,
    m_io_axil_wdata   => io_axil_link.wdata,
    m_io_axil_wstrb   => io_axil_link.wstrb,
    m_io_axil_wvalid  => io_axil_link.wvalid,
    m_io_axil_wready  => io_axil_link.wready,
    m_io_axil_bresp   => io_axil_link.bresp,
    m_io_axil_bvalid  => io_axil_link.bvalid,
    m_io_axil_bready  => io_axil_link.bready,
    m_io_axil_araddr  => io_axil_link.araddr,
    m_io_axil_arvalid => io_axil_link.arvalid,
    m_io_axil_arready => io_axil_link.arready,
    m_io_axil_rdata   => io_axil_link.rdata,
    m_io_axil_rresp   => io_axil_link.rresp,
    m_io_axil_rvalid  => io_axil_link.rvalid,
    m_io_axil_rready  => io_axil_link.rready
);

m_io_axil_awaddr <= io_axil_link.awaddr;
m_io_axil_awvalid <= io_axil_link.awvalid;
io_axil_link.awready <= m_io_axil_awready;
m_io_axil_wdata <= io_axil_link.wdata;
m_io_axil_wstrb <= io_axil_link.wstrb;
m_io_axil_wvalid <= io_axil_link.wvalid;
io_axil_link.wready <= m_io_axil_wready;
io_axil_link.bresp <= m_io_axil_bresp;
io_axil_link.bvalid <= m_io_axil_bvalid;
m_io_axil_bready <= io_axil_link.bready;
m_io_axil_araddr <= io_axil_link.araddr;
m_io_axil_arvalid <= io_axil_link.arvalid;
io_axil_link.arready <= m_io_axil_arready;
io_axil_link.rdata <= m_io_axil_rdata;
io_axil_link.rresp <= m_io_axil_rresp;
io_axil_link.rvalid <= m_io_axil_rvalid;
m_io_axil_rready <= io_axil_link.rready;

link1_lo_vc_muxer : lo_vc_muxer
generic map (
    CHANNELS    => 2
)
port map (
    clk         => clk,
    
    lo_vc       => link1_mob.lo_vc.data,
    lo_vc_no    => link1_mob.lo_vc.no,
    lo_valid    => link1_mob.lo_vc.valid,
    lo_ready    => link1_mob.lo_vc_ready,
    
    ch_vc(0)    => link1_io_vc,
    ch_vc(1)    => link_eci_packet_tx.c10_gsync_hdr_tx,

    ch_vc_no(0) => link1_io_vc_no,
    ch_vc_no(1) => X"a",

    ch_valid(0) => link1_io_valid,
    ch_valid(1) => link_eci_packet_tx.c10_gsync_valid_tx,

    ch_ready(0) => link1_io_ready,
    ch_ready(1) => link_eci_packet_tx.c10_gsync_ready_tx
);

link2_lo_vc_muxer : lo_vc_muxer
generic map (
    CHANNELS    => 2
)
port map (
    clk         => clk,
    
    lo_vc       => link2_mob.lo_vc.data,
    lo_vc_no    => link2_mob.lo_vc.no,
    lo_valid    => link2_mob.lo_vc.valid,
    lo_ready    => link2_mob.lo_vc_ready,
    
    ch_vc(0)    => link2_io_vc,
    ch_vc(1)    => link_eci_packet_tx.c11_gsync_hdr_tx,

    ch_vc_no(0) => link2_io_vc_no,
    ch_vc_no(1) => X"b",

    ch_valid(0) => link2_io_valid,
    ch_valid(1) => link_eci_packet_tx.c11_gsync_valid_tx,

    ch_ready(0) => link2_io_ready,
    ch_ready(1) => link_eci_packet_tx.c11_gsync_ready_tx
);

link1_packet_deserialiser : vc_eci_pkt_router
port map (
    clk => clk,
    reset => reset,

    -- Incoming CPU to FPGA events (MIB VCs) c_ -> CPU initated events
    -- VC11 to VC2 (VC1, VC0) are IO VCs not handled here
    -- Incoming response without data VC 11,10
    c11_vc_pkt_i       => words_to_vector(link1_mib.vc.vc11.data),
    c11_vc_pkt_word_enable_i  => link1_mib.vc.vc11.word_enable,
    c11_vc_pkt_valid_i => link1_mib.vc.vc11.valid,
    c11_vc_pkt_ready_o => link1_mib.vc_ready(11),

    c10_vc_pkt_i       => words_to_vector(link1_mib.vc.vc10.data),
    c10_vc_pkt_word_enable_i  => link1_mib.vc.vc10.word_enable,
    c10_vc_pkt_valid_i => link1_mib.vc.vc10.valid,
    c10_vc_pkt_ready_o => link1_mib.vc_ready(10),

    -- Incoming Forwards VC 9,8
    -- Forwards are command only no data, currently not connected
    c9_vc_pkt_i       => words_to_vector(link1_mib.vc.vc9.data),
    c9_vc_pkt_word_enable_i  => link1_mib.vc.vc9.word_enable,
    c9_vc_pkt_valid_i => link1_mib.vc.vc9.valid,
    c9_vc_pkt_ready_o => link1_mib.vc_ready(9),

    c8_vc_pkt_i       => words_to_vector(link1_mib.vc.vc8.data),
    c8_vc_pkt_word_enable_i  => link1_mib.vc.vc8.word_enable,
    c8_vc_pkt_valid_i => link1_mib.vc.vc8.valid,
    c8_vc_pkt_ready_o => link1_mib.vc_ready(8),

   -- Incoming requests without data VC 7,6
    c7_vc_pkt_i       => words_to_vector(link1_mib.vc.vc7.data),
    c7_vc_pkt_word_enable_i  => link1_mib.vc.vc7.word_enable,
    c7_vc_pkt_valid_i => link1_mib.vc.vc7.valid,
    c7_vc_pkt_ready_o => link1_mib.vc_ready(7),

    c6_vc_pkt_i       => words_to_vector(link1_mib.vc.vc6.data),
    c6_vc_pkt_word_enable_i  => link1_mib.vc.vc6.word_enable,
    c6_vc_pkt_valid_i => link1_mib.vc.vc6.valid,
    c6_vc_pkt_ready_o => link1_mib.vc_ready(6),

    -- Incoming resoponse with data VC 5,4
    c5_vc_pkt_i       => words_to_vector(link1_mib.vc.vc5.data),
    c5_vc_pkt_word_enable_i  => link1_mib.vc.vc5.word_enable,
    c5_vc_pkt_valid_i => link1_mib.vc.vc5.valid,
    c5_vc_pkt_ready_o => link1_mib.vc_ready(5),

    c4_vc_pkt_i       => words_to_vector(link1_mib.vc.vc4.data),
    c4_vc_pkt_word_enable_i  => link1_mib.vc.vc4.word_enable,
    c4_vc_pkt_valid_i => link1_mib.vc.vc4.valid,
    c4_vc_pkt_ready_o => link1_mib.vc_ready(4),

    -- Incoming request with data VC 3,2
    c3_vc_pkt_i       => words_to_vector(link1_mib.vc.vc3.data),
    c3_vc_pkt_word_enable_i  => link1_mib.vc.vc3.word_enable,
    c3_vc_pkt_valid_i => link1_mib.vc.vc3.valid,
    c3_vc_pkt_ready_o => link1_mib.vc_ready(3),

    c2_vc_pkt_i       => words_to_vector(link1_mib.vc.vc2.data),
    c2_vc_pkt_word_enable_i  => link1_mib.vc.vc2.word_enable,
    c2_vc_pkt_valid_i => link1_mib.vc.vc2.valid,
    c2_vc_pkt_ready_o => link1_mib.vc_ready(2),

    -- ECI packets corresponding to the VCs holding CPU initiated events
    -- ECI packets for VC 11 - 2 (VC1,0 are IO VCs not handled here)

    -- All VCs other than request without data (6,7 i.e. reads) are just
    -- allowed to drain.

    -- ECI packet for CPU initiated response without data VC 11,10
    -- No payload only header
    c11_eci_hdr_o       => open,
    c11_eci_pkt_size_o  => open,
    c11_eci_pkt_vc_o    => open,
    c11_eci_pkt_valid_o => open,
    c11_eci_pkt_ready_i => '1',

    c10_eci_hdr_o       => open,
    c10_eci_pkt_size_o  => open,
    c10_eci_pkt_vc_o    => open,
    c10_eci_pkt_valid_o => open,
    c10_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated forwards VC 9,8
    -- No payload only header
    c9_eci_hdr_o       => open,
    c9_eci_pkt_size_o  => open,
    c9_eci_pkt_vc_o    => open,
    c9_eci_pkt_valid_o => open,
    c9_eci_pkt_ready_i => '1',

    c8_eci_hdr_o       => open,
    c8_eci_pkt_size_o  => open,
    c8_eci_pkt_vc_o    => open,
    c8_eci_pkt_valid_o => open,
    c8_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated request without data VC 7,6
    -- No payload only header
    c7_eci_hdr_o       => open,
    c7_eci_pkt_size_o  => open,
    c7_eci_pkt_vc_o    => open,
    c7_eci_pkt_valid_o => open,
    c7_eci_pkt_ready_i => '1',

    c6_eci_hdr_o       => open,
    c6_eci_pkt_size_o  => open,
    c6_eci_pkt_vc_o    => open,
    c6_eci_pkt_valid_o => open,
    c6_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated response with data VC 5,4
    -- Header + payload
    c5_eci_pkt_o       => open,
    c5_eci_pkt_size_o  => open,
    c5_eci_pkt_vc_o    => open,
    c5_eci_pkt_valid_o => open,
    c5_eci_pkt_ready_i => '1',

    c4_eci_pkt_o       => open,
    c4_eci_pkt_size_o  => open,
    c4_eci_pkt_vc_o    => open,
    c4_eci_pkt_valid_o => open,
    c4_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated requests with data VC 3,2
    c3_eci_pkt_o       => open,
    c3_eci_pkt_size_o  => open,
    c3_eci_pkt_vc_o    => open,
    c3_eci_pkt_valid_o => open,
    c3_eci_pkt_ready_i => '1',

    c2_eci_pkt_o       => open,
    c2_eci_pkt_size_o  => open,
    c2_eci_pkt_vc_o    => open,
    c2_eci_pkt_valid_o => open,
    c2_eci_pkt_ready_i => '1',

    -- VC 1,0 are IO VCs, not handled here

    -- Special Handlers
    -- GSYNC from VC7,6 (req without data)
    c7_gsync_hdr_o   => link1_eci_packet_rx.c7_gsync_hdr_rx,
    c7_gsync_valid_o => link1_eci_packet_rx.c7_gsync_valid_rx,
    c7_gsync_ready_i => link1_eci_packet_rx.c7_gsync_ready_rx,

    c6_gsync_hdr_o   => link1_eci_packet_rx.c6_gsync_hdr_rx,
    c6_gsync_valid_o => link1_eci_packet_rx.c6_gsync_valid_rx,
    c6_gsync_ready_i => link1_eci_packet_rx.c6_gsync_ready_rx
);

link2_packet_deserialiser : vc_eci_pkt_router
port map (
    clk => clk,
    reset => reset,

    -- Incoming CPU to FPGA events (MIB VCs) c_ -> CPU initated events
    -- VC11 to VC2 (VC1, VC0) are IO VCs not handled here
    -- Incoming response without data VC 11,10
    c11_vc_pkt_i       => words_to_vector(link2_mib.vc.vc11.data),
    c11_vc_pkt_word_enable_i  => link2_mib.vc.vc11.word_enable,
    c11_vc_pkt_valid_i => link2_mib.vc.vc11.valid,
    c11_vc_pkt_ready_o => link2_mib.vc_ready(11),

    c10_vc_pkt_i       => words_to_vector(link2_mib.vc.vc10.data),
    c10_vc_pkt_word_enable_i  => link2_mib.vc.vc10.word_enable,
    c10_vc_pkt_valid_i => link2_mib.vc.vc10.valid,
    c10_vc_pkt_ready_o => link2_mib.vc_ready(10),

    -- Incoming Forwards VC 9,8
    -- Forwards are command only no data, currently not connected
    c9_vc_pkt_i       => words_to_vector(link2_mib.vc.vc9.data),
    c9_vc_pkt_word_enable_i  => link2_mib.vc.vc9.word_enable,
    c9_vc_pkt_valid_i => link2_mib.vc.vc9.valid,
    c9_vc_pkt_ready_o => link2_mib.vc_ready(9),

    c8_vc_pkt_i       => words_to_vector(link2_mib.vc.vc8.data),
    c8_vc_pkt_word_enable_i  => link2_mib.vc.vc8.word_enable,
    c8_vc_pkt_valid_i => link2_mib.vc.vc8.valid,
    c8_vc_pkt_ready_o => link2_mib.vc_ready(8),

   -- Incoming requests without data VC 7,6
    c7_vc_pkt_i       => words_to_vector(link2_mib.vc.vc7.data),
    c7_vc_pkt_word_enable_i  => link2_mib.vc.vc7.word_enable,
    c7_vc_pkt_valid_i => link2_mib.vc.vc7.valid,
    c7_vc_pkt_ready_o => link2_mib.vc_ready(7),

    c6_vc_pkt_i       => words_to_vector(link2_mib.vc.vc6.data),
    c6_vc_pkt_word_enable_i  => link2_mib.vc.vc6.word_enable,
    c6_vc_pkt_valid_i => link2_mib.vc.vc6.valid,
    c6_vc_pkt_ready_o => link2_mib.vc_ready(6),

    -- Incoming resoponse with data VC 5,4
    c5_vc_pkt_i       => words_to_vector(link2_mib.vc.vc5.data),
    c5_vc_pkt_word_enable_i  => link2_mib.vc.vc5.word_enable,
    c5_vc_pkt_valid_i => link2_mib.vc.vc5.valid,
    c5_vc_pkt_ready_o => link2_mib.vc_ready(5),

    c4_vc_pkt_i       => words_to_vector(link2_mib.vc.vc4.data),
    c4_vc_pkt_word_enable_i  => link2_mib.vc.vc4.word_enable,
    c4_vc_pkt_valid_i => link2_mib.vc.vc4.valid,
    c4_vc_pkt_ready_o => link2_mib.vc_ready(4),

    -- Incoming request with data VC 3,2
    c3_vc_pkt_i       => words_to_vector(link2_mib.vc.vc3.data),
    c3_vc_pkt_word_enable_i  => link2_mib.vc.vc3.word_enable,
    c3_vc_pkt_valid_i => link2_mib.vc.vc3.valid,
    c3_vc_pkt_ready_o => link2_mib.vc_ready(3),

    c2_vc_pkt_i       => words_to_vector(link2_mib.vc.vc2.data),
    c2_vc_pkt_word_enable_i  => link2_mib.vc.vc2.word_enable,
    c2_vc_pkt_valid_i => link2_mib.vc.vc2.valid,
    c2_vc_pkt_ready_o => link2_mib.vc_ready(2),

    -- ECI packets corresponding to the VCs holding CPU initiated events
    -- ECI packets for VC 11 - 2 (VC1,0 are IO VCs not handled here)

    -- All VCs other than request without data (6,7 i.e. reads) are just
    -- allowed to drain.

    -- ECI packet for CPU initiated response without data VC 11,10
    -- No payload only header
    c11_eci_hdr_o       => open,
    c11_eci_pkt_size_o  => open,
    c11_eci_pkt_vc_o    => open,
    c11_eci_pkt_valid_o => open,
    c11_eci_pkt_ready_i => '1',

    c10_eci_hdr_o       => open,
    c10_eci_pkt_size_o  => open,
    c10_eci_pkt_vc_o    => open,
    c10_eci_pkt_valid_o => open,
    c10_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated forwards VC 9,8
    -- No payload only header
    c9_eci_hdr_o       => open,
    c9_eci_pkt_size_o  => open,
    c9_eci_pkt_vc_o    => open,
    c9_eci_pkt_valid_o => open,
    c9_eci_pkt_ready_i => '1',

    c8_eci_hdr_o       => open,
    c8_eci_pkt_size_o  => open,
    c8_eci_pkt_vc_o    => open,
    c8_eci_pkt_valid_o => open,
    c8_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated request without data VC 7,6
    -- No payload only header
    c7_eci_hdr_o       => open,
    c7_eci_pkt_size_o  => open,
    c7_eci_pkt_vc_o    => open,
    c7_eci_pkt_valid_o => open,
    c7_eci_pkt_ready_i => '1',

    c6_eci_hdr_o       => open,
    c6_eci_pkt_size_o  => open,
    c6_eci_pkt_vc_o    => open,
    c6_eci_pkt_valid_o => open,
    c6_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated response with data VC 5,4
    -- Header + payload
    c5_eci_pkt_o       => open,
    c5_eci_pkt_size_o  => open,
    c5_eci_pkt_vc_o    => open,
    c5_eci_pkt_valid_o => open,
    c5_eci_pkt_ready_i => '1',

    c4_eci_pkt_o       => open,
    c4_eci_pkt_size_o  => open,
    c4_eci_pkt_vc_o    => open,
    c4_eci_pkt_valid_o => open,
    c4_eci_pkt_ready_i => '1',

    -- ECI packet for CPU initiated requests with data VC 3,2
    c3_eci_pkt_o       => open,
    c3_eci_pkt_size_o  => open,
    c3_eci_pkt_vc_o    => open,
    c3_eci_pkt_valid_o => open,
    c3_eci_pkt_ready_i => '1',

    c2_eci_pkt_o       => open,
    c2_eci_pkt_size_o  => open,
    c2_eci_pkt_vc_o    => open,
    c2_eci_pkt_valid_o => open,
    c2_eci_pkt_ready_i => '1',

    -- VC 1,0 are IO VCs, not handled here

    -- Special Handlers
    -- GSYNC from VC7,6 (req without data)
    c7_gsync_hdr_o   => link2_eci_packet_rx.c7_gsync_hdr_rx,
    c7_gsync_valid_o => link2_eci_packet_rx.c7_gsync_valid_rx,
    c7_gsync_ready_i => link2_eci_packet_rx.c7_gsync_ready_rx,

    c6_gsync_hdr_o   => link2_eci_packet_rx.c6_gsync_hdr_rx,
    c6_gsync_valid_o => link2_eci_packet_rx.c6_gsync_valid_rx,
    c6_gsync_ready_i => link2_eci_packet_rx.c6_gsync_ready_rx
);

i_link_eci_rx_vc7_gsync_mux : eci_packet_rx_mux
generic map (
    WIDTH           => 64
)
port map (
    clk             => clk,
    link1_data      => link1_eci_packet_rx.c7_gsync_hdr_rx,
    link1_valid     => link1_eci_packet_rx.c7_gsync_valid_rx,
    link1_ready     => link1_eci_packet_rx.c7_gsync_ready_rx,
    link2_data      => link2_eci_packet_rx.c7_gsync_hdr_rx,
    link2_valid     => link2_eci_packet_rx.c7_gsync_valid_rx,
    link2_ready     => link2_eci_packet_rx.c7_gsync_ready_rx,
    output_data     => link_eci_packet_rx.c7_gsync_hdr_rx,
    output_valid    => link_eci_packet_rx.c7_gsync_valid_rx,
    output_ready    => link_eci_packet_rx.c7_gsync_ready_rx 
);

i_link_eci_rx_vc6_gsync_mux : eci_packet_rx_mux
generic map (
    WIDTH           => 64
)
port map (
    clk             => clk,
    link1_data      => link1_eci_packet_rx.c6_gsync_hdr_rx,
    link1_valid     => link1_eci_packet_rx.c6_gsync_valid_rx,
    link1_ready     => link1_eci_packet_rx.c6_gsync_ready_rx,
    link2_data      => link2_eci_packet_rx.c6_gsync_hdr_rx,
    link2_valid     => link2_eci_packet_rx.c6_gsync_valid_rx,
    link2_ready     => link2_eci_packet_rx.c6_gsync_ready_rx,
    output_data     => link_eci_packet_rx.c6_gsync_hdr_rx,
    output_valid    => link_eci_packet_rx.c6_gsync_valid_rx,
    output_ready    => link_eci_packet_rx.c6_gsync_ready_rx 
);

link_vc7_vc11_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c7_gsync_hdr_rx,
    vc_req_valid_i => link_eci_packet_rx.c7_gsync_valid_rx,
    vc_req_ready_o => link_eci_packet_rx.c7_gsync_ready_rx,

    vc_resp_o       => link_eci_packet_tx.c11_gsync_hdr_tx,
    vc_resp_valid_o => link_eci_packet_tx.c11_gsync_valid_tx,
    vc_resp_ready_i => link_eci_packet_tx.c11_gsync_ready_tx
);

link_vc6_vc10_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c6_gsync_hdr_rx,
    vc_req_valid_i => link_eci_packet_rx.c6_gsync_valid_rx,
    vc_req_ready_o => link_eci_packet_rx.c6_gsync_ready_rx,

    vc_resp_o       => link_eci_packet_tx.c10_gsync_hdr_tx,
    vc_resp_valid_o => link_eci_packet_tx.c10_gsync_valid_tx,
    vc_resp_ready_i => link_eci_packet_tx.c10_gsync_ready_tx
);

end Behavioral;
